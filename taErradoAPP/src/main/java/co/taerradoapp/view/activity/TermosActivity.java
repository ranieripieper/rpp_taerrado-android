package co.taerradoapp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;

import co.taerradoapp.R;
import co.taerradoapp.util.Constants;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.view.activity.base.TaErradoBaseActivity;

public class TermosActivity extends TaErradoBaseActivity {

	private ImageView imgLogo;
	private TextView txtTermos;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.termos_activity);
		imgLogo = (ImageView)findViewById(R.id.img_logo);
		txtTermos = (TextView)findViewById(R.id.txt_termos);
		txtTermos.setText(Html.fromHtml(getString(R.string.termos_de_uso_texto)));
		// Scaling
    	Animation scale = new ScaleAnimation(1f, Constants.LOGO_SCALE_SIZE, 1f, Constants.LOGO_SCALE_SIZE, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
    	scale.setDuration(0);
    	scale.setFillEnabled(true);
    	scale.setFillAfter(true);
    	imgLogo.startAnimation(scale);
	}
	
	public void btAceitoClick(View v) {
		SharedPrefManager.getInstance().setTermosAceito(true);
		HomeActivity.showActivity(getContext(), HomeActivity.PAGE_MAP);
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
			finishAffinity();
		} else {
			finish();
		}
	}

	public void btNaoAceitoClick(View v) {
		LoginManager.getInstance().logOut();
		SharedPrefManager.getInstance().logout();
		
		startActivity(new Intent(getContext(), SplashLoginActivity.class));
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
			finishAffinity();
		} else {
			finish();
		}
	}
}
