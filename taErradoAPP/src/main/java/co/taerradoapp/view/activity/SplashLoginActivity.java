package co.taerradoapp.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.ButtonPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import co.taerradoapp.BuildConfig;
import co.taerradoapp.R;
import co.taerradoapp.model.BaseResult;
import co.taerradoapp.model.LoginResult;
import co.taerradoapp.service.RetrofitManager;
import co.taerradoapp.util.Constants;
import co.taerradoapp.util.Paths;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.util.UrlParameters;
import co.taerradoapp.view.activity.base.TaErradoBaseActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SplashLoginActivity extends Activity {

    private static int SPLASH_TIME_OUT = 1000;
    private ImageView imgLogo;
    private ImageView imgLoading1;
    private ImageView imgLoading2;
    private ImageView imgLoading3;
    private ImageView imgLoading4;
    private final int ANIMATION_LOADING_TIME = 500;
    private final int ANIMATION_TIME = 1000;
    private RelativeLayout layoutFacebook;
    private float startFacebookBtPostion = 0;
    private List<AnimationSet> lstAnimationSet = new ArrayList<AnimationSet>();
    private boolean stopLoadingAnimation = false;
    protected AsyncTaskService async;
    private boolean showMsg = false;
    private LoginButton loginButton;
    protected LoadingView loadingView;
    protected TouchBlackHoleView blackHole;

    private CallbackManager callbackManager;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    private EditTextPlus edtEmail;
    private EditTextPlus edtPwd;
    private View btSignUp;
    private View btResetPwd;
    private View btSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);

        SharedPrefManager.getInstance().setUploadFoto(false);
        setContentView(R.layout.splash_login_activity);

        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker(getString(R.string.google_analytics));
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);

        tracker.setScreenName("Splash e Login");
        callbackManager = CallbackManager.Factory.create();

        loadingView = (LoadingView) findViewById(R.id.progress_bar);
        blackHole = (TouchBlackHoleView) findViewById(R.id.black_hole);

        imgLogo = (ImageView) findViewById(R.id.img_logo);
        imgLoading1 = (ImageView) findViewById(R.id.img_loading_1);
        imgLoading2 = (ImageView) findViewById(R.id.img_loading_2);
        imgLoading3 = (ImageView) findViewById(R.id.img_loading_3);
        imgLoading4 = (ImageView) findViewById(R.id.img_loading_4);
        layoutFacebook = (RelativeLayout) findViewById(R.id.layout_facebook);
        loginButton = (LoginButton) findViewById(R.id.btn_conectar_facebook);
        loginButton.setReadPermissions(Arrays.asList("public_profile, email, publish_actions"));
        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TaErradoBaseActivity.trackerClick(tracker, "loginFacebook");
            }
        });
        edtEmail = (EditTextPlus) findViewById(R.id.edt_email);
        edtPwd = (EditTextPlus) findViewById(R.id.edt_password);
        btSignUp = findViewById(R.id.bt_fazer_cadastro);
        btResetPwd = findViewById(R.id.bt_reset_password);
        btSignIn = findViewById(R.id.layout_entrar);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                //panningView.stopPanning();
                if (SharedPrefManager.getInstance().userIsLogado()) {
                    startHomeActivity();
                } else {
                    showLogin();
                }
            }
        }, SPLASH_TIME_OUT);

        if (!SharedPrefManager.getInstance().userIsLogado()) {
            LoginManager.getInstance().logOut();
            // Callback registration

            FacebookCallback<com.facebook.login.LoginResult> fbCallback = new FacebookCallback<com.facebook.login.LoginResult>() {
                @Override
                public void onSuccess(final com.facebook.login.LoginResult loginResult) {
                    // App code
                    GraphRequest.newMeRequest(
                            loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject me, GraphResponse response) {
                                    if (response.getError() != null) {
                                        facebookError();
                                    } else {
                                        String email = me.optString("email");
                                        String fbId = me.optString("id");
                                        String name = me.optString("first_name");
                                        String surname = me.optString("last_name");
                                        String gender = me.optString("gender");
                                        facebookSuccess(name, surname, gender, email, loginResult.getAccessToken().getToken(), fbId);
                                    }
                                }
                            }).executeAsync();
                }

                @Override
                public void onCancel() {
                }

                @Override
                public void onError(FacebookException exception) {
                    facebookError();
                }
            };

            loginButton.registerCallback(callbackManager, fbCallback);

        }

        btSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignUpActivity.showActivity(SplashLoginActivity.this,
                        new Pair<View, String>(imgLogo, getString(R.string.transition_logo)));
            }
        });

        btResetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword();
            }
        });

        btSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (formIsValid()) {
                    callLogin();
                }
            }
        });

        if (BuildConfig.DEBUG) {
            edtEmail.setText("ranieripieper@gmail.com");
            edtPwd.setText("123456");
        }
        btSignIn.invalidate();
    }

    private void callLogin() {
        animationClickFacebook();
        RetrofitManager.getInstance().getUserService().login(edtEmail.getTextStr(), edtPwd.getTextStr(),
                Constants.PLATFORM, SharedPrefManager.getInstance().getDeviceToken(), new Callback<LoginResult>() {
                    @Override
                    public void success(LoginResult loginResult, Response response) {
                        dismissWaitDialog();
                        processResult(loginResult, null, null);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (error != null && error.getResponse() != null && error.getResponse().getStatus() == HttpStatus.UNAUTHORIZED.value()) {
                            facebookError(getString(R.string.email_password_not_valid));
                        } else {
                            facebookError();
                        }

                    }
                });
    }

    private boolean formIsValid() {
        edtEmail.hiddenKeyboard(this);
        edtPwd.hiddenKeyboard(this);
        if (edtEmail.isNotValid()) {
            Toast.makeText(this, R.string.email_not_valid, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (edtEmail.isNotValid()) {
            Toast.makeText(this, R.string.pwd_not_valid, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void resetPassword() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View v = LayoutInflater.from(this).inflate(R.layout.reset_password_dialog, null);
        builder.setView(v);

        final AlertDialog dialog = builder.create();

        final EditTextPlus edtEmailReset = (EditTextPlus) v.findViewById(R.id.edt_email);
        ButtonPlus btRight = (ButtonPlus) v.findViewById(R.id.bt_right);
        ButtonPlus btLeft = (ButtonPlus) v.findViewById(R.id.bt_left);

        btRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                edtEmailReset.hiddenKeyboard(SplashLoginActivity.this);
                if (edtEmailReset.isValid()) {
                    dialog.dismiss();
                    callResetPassword(edtEmailReset.getTextStr());
                } else {
                    Toast.makeText(SplashLoginActivity.this, R.string.email_not_valid, Toast.LENGTH_SHORT).show();
                }
            }
        });
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCanceledOnTouchOutside(true);

        dialog.show();
    }

    private void callResetPassword(String email) {
        showWaitDialog();
        RetrofitManager.getInstance().getUserService().reset_password(email, new Callback<BaseResult>() {
            @Override
            public void success(BaseResult baseResult, Response response) {
                Toast.makeText(SplashLoginActivity.this, R.string.email_enviado, Toast.LENGTH_SHORT).show();
                dismissWaitDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                TaErradoBaseActivity.showError(SplashLoginActivity.this, error, true);
                dismissWaitDialog();
            }
        });
    }

    protected void showMessage(int msg) {
        showMessage(getString(msg));
    }

    protected void showMessage(String msg) {
        if (showMsg) {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }
    }

    private void facebookSuccess(String name, String surname, String gender, String email, String accessToken, String fbId) {
        if (TextUtils.isEmpty(email)) {
            callService(name, surname, gender, email, accessToken, fbId);
        } else {
            callService(name, surname, gender, email, accessToken, fbId);
        }
    }

    private void facebookError() {
        facebookError(getString(R.string.msg_erro));
    }

    private void facebookError(String msg) {
        showMessage(msg);
        LoginManager.getInstance().logOut();
        SharedPrefManager.getInstance().logout();
        animationClickFacebookError();
        stopLoadingAnimation = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    //***********************************
    //******* LOGIN SERVICE**************
    //***********************************
    //curl -F "user[email]=teste@teste.com" -F "user[fb_id]=123" -F "user[fb_token]=1234567" -F "user[gender]=male" -F "user[surname]=xxx" -F "user[name]=xxx" http://localhost:3000/api/v1/users
    //{"success":true,"user_data":{"email":"teste@teste.com","fb_id":"123","auth_token":"12345"}}
    private void callService(final String name, final String surname, final String gender, final String email, final String accessToken, final String fbId) {
        animationClickFacebook();
        final Date dtInit = new Date();
        ObserverAsyncTask<LoginResult> observer = new ObserverAsyncTask<LoginResult>() {
            @Override
            public void onPreExecute() {
                //animationClickFacebook();
            }

            @Override
            public void onPostExecute(final LoginResult result) {
                if (DateUtil.getDifSeconds(dtInit, new Date()) > 5) {
                    processResult(result, accessToken, fbId);
                } else {
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            processResult(result, accessToken, fbId);
                        }
                    }, 2000);
                }
            }

            @Override
            public void onCancelled() {
                facebookError();
            }

            @Override
            public void onError(Exception e) {
                facebookError();
            }
        };

        MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
        params.add(UrlParameters.USER_EMAIL, email);
        params.add(UrlParameters.USER_FB_ID, fbId);
        params.add(UrlParameters.USER_FB_TOKEN, accessToken);
        params.add(UrlParameters.USER_GENDER, gender);
        params.add(UrlParameters.USER_NAME, name);
        params.add(UrlParameters.USER_SURNAME, surname);
        params.add(UrlParameters.USER_PLATFORM, Constants.PLATFORM);
        params.add(UrlParameters.USER_DEVICE_TOKEN, SharedPrefManager.getInstance().getDeviceToken());

        ServiceBuilder sb = new ServiceBuilder();
        sb.setUrl(Paths.LOGIN)
                .setObserverAsyncTask(observer)
                .setNeedConnection(true)
                .setHttpMethod(HttpMethod.POST)
                .setParams(params);

        async = sb.mappingInto(SplashLoginActivity.this, LoginResult.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            async.execute();
        }
    }

    private void processResult(final LoginResult result, final String accessToken, final String fbId) {
        if (result.isSuccess() && result.getUser() != null && !TextUtils.isEmpty(result.getUser().getAuthToken())) {
            SharedPrefManager.getInstance().setAuthToken(result.getUser().getAuthToken());
            SharedPrefManager.getInstance().setFacebookToken(accessToken);
            SharedPrefManager.getInstance().setFacebookId(fbId);
            SharedPrefManager.getInstance().setProfileImageUrl(result.getUser().getProfileImageUrl());
            startHomeActivity();
        } else {
            facebookError();
        }
    }

    private void startHomeActivity() {
        //panningView.stopPanning();
        if (SharedPrefManager.getInstance().isTermosAceito()) {
            HomeActivity.showActivity(SplashLoginActivity.this, HomeActivity.PAGE_MAP);
        } else {
            showTermos();
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            finish();
        }
    }

    private void showTermos() {
        startActivity(new Intent(this, TermosActivity.class));
    }

    //***********************************
    //******* ANIMAÇÕES *****************
    //***********************************
    private void animationClickFacebook() {
        btSignIn.invalidate();
        startFacebookBtPostion = loginButton.getLeft();
        // Texto
        Animation slideLeftTxt = new TranslateAnimation(startFacebookBtPostion, -(loginButton.getWidth() + ScreenUtil.getDisplayWidth(this)), 0, 0);
        slideLeftTxt.setDuration(ANIMATION_TIME);
        slideLeftTxt.setFillAfter(true);

        //Loadings
        Animation slideLeftLoading = new TranslateAnimation(ScreenUtil.getDisplayWidth(this) - imgLoading1.getLeft() + imgLoading1.getWidth(), 0, 0, 0);
        slideLeftLoading.setStartOffset(ANIMATION_TIME / 4);
        slideLeftLoading.setDuration(ANIMATION_TIME / 2);
        slideLeftLoading.setFillAfter(true);

        //loginButton.startAnimation(slideLeftTxt);
        lockScreen();

        startAnimationSetVisible(imgLoading1, slideLeftLoading);
        startAnimationSetVisible(imgLoading2, slideLeftLoading);
        startAnimationSetVisible(imgLoading3, slideLeftLoading);

        slideLeftLoading.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animationLoadingLogin();
                //panningView.stopPanning();
            }
        });
        imgLoading4.startAnimation(slideLeftLoading);
    }

    private void animationClickFacebookError() {

        //para dar tempo de cancelar a animação dos loadings
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                clearAnimation(imgLoading1);
                clearAnimation(imgLoading2);
                clearAnimation(imgLoading3);
                clearAnimation(imgLoading4);
                // Texto
                Animation slideLeftTxt = new TranslateAnimation(-ScreenUtil.getDisplayWidth(SplashLoginActivity.this), 0, 0, 0);
                slideLeftTxt.setDuration(ANIMATION_TIME);
                slideLeftTxt.setFillAfter(true);

                //Loadings
                Animation slideLeftLoading = new TranslateAnimation(0, ScreenUtil.getDisplayWidth(SplashLoginActivity.this), 0, 0);
                slideLeftLoading.setDuration(ANIMATION_TIME / 2);
                slideLeftLoading.setFillAfter(true);

                //loginButton.startAnimation(slideLeftTxt);
                unlockScreen();
                imgLoading1.startAnimation(slideLeftLoading);
                imgLoading2.startAnimation(slideLeftLoading);
                imgLoading3.startAnimation(slideLeftLoading);
                imgLoading4.startAnimation(slideLeftLoading);
            }
        }, ANIMATION_TIME);


    }

    private void clearAnimation(View v) {
        if (v.getAnimation() != null) {
            v.getAnimation().cancel();
        }

        v.clearAnimation();
        v.setAnimation(null);
    }

    private void startAnimationSetVisible(final View v, Animation anim) {
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                v.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }
        });
        v.startAnimation(anim);
    }

    private void showLogin() {
        btSignIn.invalidate();
        showMsg = true;
        AnimationSet animLogo = getAnimationMoveLogo();
        AnimationSet animFacebookLayout = getAnimatioFacebookLayout();

        animFacebookLayout.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                layoutFacebook.setAlpha(1.0f);
                btSignIn.invalidate();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //panningView.stopPanning();
                btSignIn.invalidate();
            }
        });
        imgLogo.startAnimation(animLogo);
        layoutFacebook.setVisibility(View.INVISIBLE);
        layoutFacebook.startAnimation(animFacebookLayout);

    }

    private AnimationSet createAnimationLoadingLogin(final ImageView img) {
        int move = img.getHeight();
        // Moving up
        Animation slideUp = new TranslateAnimation(0, 0, 0, -move);
        slideUp.setDuration(ANIMATION_LOADING_TIME);
        Animation slideDown1 = new TranslateAnimation(0, 0, 0, move);
        slideDown1.setDuration(ANIMATION_LOADING_TIME);
        slideDown1.setStartOffset(ANIMATION_LOADING_TIME);

        // Animation set to join both scaling and moving
        final AnimationSet animSet = new AnimationSet(true);
        animSet.addAnimation(slideUp);
        animSet.addAnimation(slideDown1);
        animSet.setFillAfter(true);

        animSet.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!stopLoadingAnimation) {
                    img.startAnimation(animSet);
                }

            }
        });

        return animSet;
    }

    private void animationLoadingLogin() {
        clearAnimation(imgLoading1);
        clearAnimation(imgLoading2);
        clearAnimation(imgLoading3);
        clearAnimation(imgLoading4);
        for (AnimationSet animSet : lstAnimationSet) {
            animSet.cancel();
        }
        lstAnimationSet = new ArrayList<AnimationSet>();
        final AnimationSet animLoading1 = createAnimationLoadingLogin(imgLoading1);
        final AnimationSet animLoading2 = createAnimationLoadingLogin(imgLoading2);
        final AnimationSet animLoading3 = createAnimationLoadingLogin(imgLoading3);
        final AnimationSet animLoading4 = createAnimationLoadingLogin(imgLoading4);

        imgLoading1.startAnimation(animLoading1);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                imgLoading2.startAnimation(animLoading2);
            }
        }, ANIMATION_LOADING_TIME / 4);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                imgLoading3.startAnimation(animLoading3);
            }
        }, ANIMATION_LOADING_TIME / 3);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                imgLoading4.startAnimation(animLoading4);
            }
        }, ANIMATION_LOADING_TIME / 2);

        lstAnimationSet.add(animLoading1);
        lstAnimationSet.add(animLoading2);
        lstAnimationSet.add(animLoading3);
        lstAnimationSet.add(animLoading4);
    }

    private AnimationSet getAnimatioFacebookLayout() {
        // Moving Left
        Animation slideUp = new TranslateAnimation(layoutFacebook.getWidth(), 0, 0, 0);
        slideUp.setDuration(ANIMATION_TIME);
        // Animation set to join both scaling and moving
        AnimationSet animSet = new AnimationSet(true);
        animSet.setFillEnabled(true);
        animSet.addAnimation(slideUp);
        animSet.setFillAfter(true);
        return animSet;
    }

    private AnimationSet getAnimationMoveLogo() {
        // Scaling
        Animation scale = new ScaleAnimation(1f, Constants.LOGO_SCALE_SIZE, 1f, Constants.LOGO_SCALE_SIZE, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scale.setDuration(ANIMATION_TIME);
        scale.setFillEnabled(true);
        scale.setFillAfter(true);

        layoutFacebook.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        imgLogo.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

        int height = ScreenUtil.getDisplayHeight(this);

        int translateY = 200;
        if (layoutFacebook.getMeasuredHeight() > height / 2) {
            translateY = (layoutFacebook.getMeasuredHeight() - height / 2) + imgLogo.getMeasuredHeight() / 2;
            if (translateY < 200) {
                translateY = 200;
            }
        }
        // Moving up
        Animation slideUp = new TranslateAnimation(0, 0, 0, -translateY);
        slideUp.setDuration(ANIMATION_TIME);
        slideUp.setFillEnabled(true);
        slideUp.setFillAfter(true);
        // Animation set to join both scaling and moving
        AnimationSet animSet = new AnimationSet(true);
        animSet.setFillEnabled(true);
        animSet.addAnimation(scale);
        animSet.addAnimation(slideUp);
        animSet.setFillAfter(true);
        return animSet;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            finishAffinity();
        } else {
            finish();
        }
    }

    public void showWaitDialog() {
        lockScreen();

        if (loadingView != null) {
            loadingView.setVisibility(View.VISIBLE);
            if (findViewById(R.id.layout_include_loading) != null) {
                findViewById(R.id.layout_include_loading).setVisibility(View.VISIBLE);
            }
        }
    }

    private void lockScreen() {
        if (findViewById(R.id.layout_include_loading) != null) {
            findViewById(R.id.layout_include_loading).setVisibility(View.VISIBLE);
        }
        if (blackHole != null) {
            blackHole.disable_touch(true);
            blackHole.setVisibility(View.VISIBLE);
        }
    }

    private void unlockScreen() {
        if (blackHole != null) {
            blackHole.disable_touch(false);
            blackHole.setVisibility(View.GONE);
        }
        if (findViewById(R.id.layout_include_loading) != null) {
            findViewById(R.id.layout_include_loading).setVisibility(View.GONE);
        }
    }

    public void dismissWaitDialog() {
        unlockScreen();

        if (loadingView != null) {
            loadingView.setVisibility(View.GONE);
        }
        if (findViewById(R.id.layout_include_loading) != null) {
            findViewById(R.id.layout_include_loading).setVisibility(View.GONE);
        }
    }

}