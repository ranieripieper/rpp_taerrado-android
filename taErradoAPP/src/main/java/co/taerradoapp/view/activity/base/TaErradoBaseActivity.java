package co.taerradoapp.view.activity.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.gps.GPSTracker;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.view.ActionBarUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.ButtonPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.springframework.http.HttpStatus;

import java.net.HttpURLConnection;

import co.taerradoapp.R;
import co.taerradoapp.service.exception.ServiceException;
import co.taerradoapp.util.Constants;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.view.activity.SplashLoginActivity;
import retrofit.RetrofitError;

public class TaErradoBaseActivity extends ActionBarActivity {

	static public boolean isAPI11 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	private ProgressDialog progress;
	protected LoadingView loadingView;
	protected TouchBlackHoleView blackHole;
	protected AsyncTaskService async;
	protected Toolbar toolbar;
	protected GPSTracker gpsTracker;
	public static final int REQUEST_CODE_GPS = 999;
	
	public static GoogleAnalytics analytics;
	public static Tracker tracker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		analytics = GoogleAnalytics.getInstance(this);
	    analytics.setLocalDispatchPeriod(1800);

	    tracker = analytics.newTracker(getString(R.string.google_analytics));
	    tracker.enableExceptionReporting(true);
	    tracker.enableAdvertisingIdCollection(true);
	    tracker.enableAutoActivityTracking(true);
	    
		gpsTracker = new GPSTracker(getContext());
	}
	
	public static void trackerClick(Tracker tracker, String label) {
		tracker.send(new HitBuilders.EventBuilder()
	       .setCategory("UX")
	       .setAction("click")
	       .setLabel(label)
	       .build());
	}
	
	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		loadingView = (LoadingView)findViewById(R.id.progress_bar);
		blackHole = (TouchBlackHoleView)findViewById(R.id.black_hole);
		configActionBar();
	}
	
	protected int getNavigationIcon() {
		return R.drawable.ic_back;
	}
	
    private void configActionBar() {
    	toolbar = (Toolbar)findViewById(R.id.taerrado_toolbar);
		if (toolbar != null) {
			toolbar.setNavigationIcon(getNavigationIcon());
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			//getSupportActionBar().setIcon(R.drawable.icn_menu);
			//getSupportActionBar().setLogo(R.drawable.icn_menu);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			ActionBarUtil.hideHomeAsUp(getWindow());
		}    	
    }
    
    protected void showMessageOnlyClose(int msg) {
    	showMessageOnlyClose(getString(msg));
    }
    
    public static void showMessageOnlyClose(Context context, int msg) {
    	showMessageOnlyClose(context, context.getString(msg));
    }
    
    public static void showMessageOnlyClose(Context context, String msg) {
    	showDefaultDialog(context, context.getString(R.string.fechar), context.getString(R.string.atencao), msg, new View.OnClickListener() {

			@Override
			public void onClick(View v) {
			}
		});
    }
    
    protected void showMessageOnlyClose(String msg) {
    	showMessageOnlyClose(getContext(), msg);
    }
    
    public static void showMessage(Context context, String msg, final RefreshListener refreshListener) {
		showDefaultDialog(context, context.getString(R.string.fechar), context.getString(R.string.tentar_novamente), context.getString(R.string.atencao), msg, null, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (refreshListener != null) {
                    refreshListener.refresh();
                }
            }
        }, false);
	}

    public static void showNewVersion(final Context context) {
        showDefaultDialog(context, context.getString(R.string.fechar), context.getString(R.string.atualizar), context.getString(R.string.atencao),
				context.getString(R.string.msg_nova_versao_disponivel), null, new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Util.openBrowser(context, Constants.URL_PALY_STORE);
					}
				}, false);
    }
    
    public interface RefreshListener {
    	public void refresh();
    }

	protected void showMessage(String msg) {
		
		showDefaultDialog(getContext(), getString(R.string.fechar), getString(R.string.tentar_novamente), getString(R.string.atencao), msg, null, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                refresh();
            }
        }, false);
	}
	
	protected void refresh() {}
	
	public int getToolbarHeight() {
		if (toolbar != null) {
			toolbar.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
			return toolbar.getMeasuredHeight();
		}
		return 0;
	}

	protected void dismissMessage() {
		//TODO
	}

	protected Context getContext() {
		return this;
	}

	public void showMessageAndFinish(int resMsgId) {
		showMessage(getString(resMsgId));
		blockScreen(true);
		new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                finish();
            }
        }, 1000);
	}

	protected void blockScreen(boolean blockScreen) {
		if (blackHole != null) {
			if (blockScreen) {
				blackHole.disable_touch(true);
				blackHole.setVisibility(View.VISIBLE);
			} else {
				blackHole.disable_touch(false);
				blackHole.setVisibility(View.GONE);
			}
		}
	}

	public void showMessage(int resMsgId) {
		showMessage(getString(resMsgId));
	}

	public void dismissWaitDialog() {
		if (blackHole != null) {
			blackHole.disable_touch(false);
			blackHole.setVisibility(View.GONE);
		}
		if (loadingView != null) {
			loadingView.setVisibility(View.GONE);
		}
		if (findViewById(R.id.layout_include_loading) != null) {
			findViewById(R.id.layout_include_loading).setVisibility(View.GONE);
		}
		if (progress != null) {
			progress.dismiss();
		}
	}

	public static void showError(Activity context, Exception e, boolean onlyClose) {
		String msgError = "";
		if (e != null) {
			if (e instanceof ConnectionNotFoundException) {
				msgError = context.getString(R.string.msg_sem_conexao);
			} else if (e instanceof DdsUtilIOException) {
				HttpStatus status = ((DdsUtilIOException) e).getHttpStatus();
				
				if (status != null && (HttpStatus.UNAUTHORIZED.equals(status))) {
					LoginManager.getInstance().logOut();
					SharedPrefManager.getInstance().logout();
					context.startActivity(new Intent(context, SplashLoginActivity.class));
					if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
						context.finishAffinity();
					} else {
						context.finish();
					}
				}
			}
		}
		
		if (TextUtils.isEmpty(msgError)) {
			msgError = context.getString(R.string.msg_ocorreu_erro);
		}
		
		if (onlyClose) {
			showMessageOnlyClose(context, msgError);
		} else {
			showMessage(context, msgError, null);
		}
	}
	
	public void showError(Exception e, boolean onlyClose) {
		dismissWaitDialog();
		String msgError = "";
		if (e != null) {
			if (e instanceof ConnectionNotFoundException) {
				msgError = getString(R.string.msg_sem_conexao);
			} else if (e instanceof DdsUtilIOException) {
				HttpStatus status = ((DdsUtilIOException) e).getHttpStatus();
				
				if (status != null && (HttpStatus.UNAUTHORIZED.equals(status))) {
					LoginManager.getInstance().logOut();
					SharedPrefManager.getInstance().logout();
					startActivity(new Intent(this, SplashLoginActivity.class));
					if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
						finishAffinity();
					} else {
						finish();
					}
				}
			}
		}
		
		if (TextUtils.isEmpty(msgError)) {
			msgError = getString(R.string.msg_ocorreu_erro);
		}
		
		if (onlyClose) {
			showMessageOnlyClose(msgError);
		} else {
			showMessage(msgError);
		}
	}
	
	public void showWaitDialog(boolean blockScreen) {
		blockScreen(blockScreen);
		
		if (loadingView != null) {
			loadingView.setVisibility(View.VISIBLE);
			if (findViewById(R.id.layout_include_loading) != null) {
				findViewById(R.id.layout_include_loading).setVisibility(View.VISIBLE);
			}
		} else {
			progress = ProgressDialog.show(this, getString(R.string.title_aguarde),
					getString(R.string.msg_aguarde), true);
			progress.show();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	//GPS
    protected boolean verifyGpsEnable() {
		if (gpsTracker == null) {
			gpsTracker = new GPSTracker(getContext());
		}
		if(gpsTracker.canGetLocation()){
			gpsTracker.stopUsingGPS();
			return true;
		}
		
		showSnackHabilitarGps(null);
		
		return false;
	}
    
    public void showSnackHabilitarGps() {
    	showSnackHabilitarGps(null);
    }
    
    public void showSnackHabilitarGps(final View.OnClickListener onclick) {
		showDefaultDialog(getContext(), getString(R.string.cancelar), getString(R.string.habilitar), getString(R.string.atencao), getString(R.string.msg_habilitar_gps), null, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (onclick == null) {
                    Intent callGPSSettingIntent = new Intent(
                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(callGPSSettingIntent, REQUEST_CODE_GPS);
                } else {
                    onclick.onClick(v);
                }

            }
        }, false);
	}
    
    public static void showDefaultDialog(Context context, final int strBtRight, int title, int msg, final OnClickListener onClickBtRight) {
    	showDefaultDialog(context, null, context.getString(strBtRight), context.getString(title), context.getString(msg), null, onClickBtRight);
    }
    
    public static void showDefaultDialog(Context context, final String strBtRight, String title, String msg, final OnClickListener onClickBtRight) {
    	showDefaultDialog(context, null, strBtRight, title, msg, null, onClickBtRight);
    }
    
    public static void showDefaultDialog(Context context, final String strBtLeft, final String strBtRight, String title, String msg, final OnClickListener onClickBtLeft, final OnClickListener onClickBtRight) {
    	showDefaultDialog(context, strBtLeft, strBtRight, title, msg, onClickBtLeft, onClickBtRight, true);
    }
    
    public static void showDefaultDialog(Context context, final String strBtLeft, final String strBtRight, String title, String msg, final OnClickListener onClickBtLeft, final OnClickListener onClickBtRight, boolean canceled) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View v = LayoutInflater.from(context).inflate(R.layout.default_2_buttons_dialog, null);
        builder.setView(v);

        final AlertDialog dialog = builder.create();

        TextViewPlus txtMsg = (TextViewPlus) v.findViewById(R.id.txt_msg);
        TextViewPlus txtTitle = (TextViewPlus) v.findViewById(R.id.txt_title);
        ButtonPlus btRight = (ButtonPlus) v.findViewById(R.id.bt_right);
        ButtonPlus btLeft = (ButtonPlus) v.findViewById(R.id.bt_left);

        txtMsg.setText(msg);
        txtTitle.setText(title);
        btRight.setText(strBtRight);
        if (TextUtils.isEmpty(strBtLeft)) {
            btLeft.setVisibility(View.GONE);
            ((LinearLayout.LayoutParams) btRight.getLayoutParams()).weight = 2;
        } else {
            btLeft.setText(strBtLeft);
        }
        btRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (onClickBtRight != null) {
                    onClickBtRight.onClick(v);
                }

            }
        });
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (onClickBtLeft != null) {
                    onClickBtLeft.onClick(v);
                }
            }
        });

        dialog.setCanceledOnTouchOutside(canceled);

        dialog.show();
	}
    
	@Override
	protected void onDestroy() {
		if (async != null) {
			async.cancel(true);

		}
		super.onDestroy();
	}
	
	@Override
	protected void onPause() {
		if (gpsTracker != null) {
			gpsTracker.stopUsingGPS();
		}
		super.onPause();
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		dismissMessage();
		final LocationManager mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        if(requestCode == REQUEST_CODE_GPS && resultCode == 0) {
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if(provider != null){
            	
            	Location loc = gpsTracker.getLocation();

            	if (loc == null) {
            		final LocationListener locationListener = new LocationListener() {
                    	Boolean search = false;
                        public void onLocationChanged(Location location) {
                        	synchronized (search) {
                        		if (!search) {
                        			search = true;
    	                    		mlocManager.removeUpdates(this); 
                        		}
    						}              	
                        }

                        public void onStatusChanged(String provider, int status, Bundle extras) {}

                        public void onProviderEnabled(String provider) {}

                        public void onProviderDisabled(String provider) {}
                      };

                      mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
                      mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2, 0, locationListener);
            	} else {
            	}
            } else {
            	showSnackHabilitarGps(null);
            }
        } else {
        	super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public static void showError(Activity context, RetrofitError error, final RefreshListener refreshListener) {
        String msg = context.getString(R.string.msg_ocorreu_erro);
        if (error != null) {

            boolean showUnauthorizedError = false;
            if (error.getCause() instanceof ServiceException) {
                ServiceException serviceException = (ServiceException)error.getCause();
                if (serviceException.getHttpStatus() != null &&
                        serviceException.getHttpStatus().intValue() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    showUnauthorizedError = true;
                    LoginManager.getInstance().logOut();
                    SharedPrefManager.getInstance().logout();
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        context.finishAffinity();
                    } else {
                        context.finish();
                    }
                    context.startActivity(new Intent(context, SplashLoginActivity.class));
                }
            }

            if (!showUnauthorizedError) {
                if (TextUtils.isEmpty(msg)) {
                    msg = context.getString(R.string.msg_ocorreu_erro);
                }

                showMessage(context, msg, refreshListener);
            }
        }
    }

    public void showError(RetrofitError error, final RefreshListener refreshListener) {
        dismissWaitDialog();
        showError(this, error, refreshListener);
    }

    public void showError(RetrofitError error) {
        showError(error, null);
    }

    public void showError(Exception e) {
        showError(e, false);
    }

}
