package co.taerradoapp.view.activity.post;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;
import com.doisdoissete.android.util.ddsutil.service.SaveFileService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;
import com.edmodo.cropper.CropImageView;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;

import co.taerradoapp.R;
import co.taerradoapp.model.PostPhoto;
import co.taerradoapp.service.TaErradoServiceBuilder;
import co.taerradoapp.util.Constants;
import co.taerradoapp.view.activity.base.TaErradoBaseActivity;

public class CropImageActivity extends TaErradoBaseActivity {

	private final static String PARAM_POST_PHOTO = "PARAM_POST_PHOTO";
	
    private PostPhoto postPhoto;
    
    // Static final constants
    private static final int DEFAULT_ASPECT_RATIO_VALUES = 10;
    private static final String ASPECT_RATIO_X = "ASPECT_RATIO_X";
    private static final String ASPECT_RATIO_Y = "ASPECT_RATIO_Y";
    private int mAspectRatioX = DEFAULT_ASPECT_RATIO_VALUES;
    private int mAspectRatioY = DEFAULT_ASPECT_RATIO_VALUES;
    
	private final static String PARAM_ROTATE = "PARAM_ROTATE";

	private CropImageView cropImageView;
    
	private int angleRotate = -1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		tracker.setScreenName("CropImage");
		
		setContentView(R.layout.post_crop_image_activity);
		postPhoto = getIntent().getExtras().getParcelable(PARAM_POST_PHOTO);
		angleRotate = getIntent().getExtras().getInt(PARAM_ROTATE);
		
		cropImageView = (CropImageView)findViewById(R.id.img_crop);
		
		findViewById(R.id.bt_ok).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				TaErradoBaseActivity.trackerClick(tracker, "btOk");
				cropImageView.setVisibility(View.INVISIBLE);
				callServiceSaveFile(cropImageView.getCroppedImage());
			}
		});
	}
	
	@Override
	protected void onResume() {
		cropImageView.setFixedAspectRatio(true);
		Bitmap bitmap;
		try {
			bitmap = BitmapUtil.media_getBitmapFromFile(new File(postPhoto.getFilePathPreCrop()), ScreenUtil.getDisplayWidth(getContext()));
			if (bitmap != null) {
				if (angleRotate != ExifInterface.ORIENTATION_NORMAL) {
					bitmap = BitmapUtil.rotateBitmap(bitmap, angleRotate);
				}
				cropImageView.setImageBitmap(bitmap);
			}				
		} catch (Exception e) {
		}
		cropImageView.setVisibility(View.VISIBLE);
		super.onResume();
	}
	
	private void callServiceSaveFile(Bitmap bitmap) {
		ObserverAsyncTask<File> observer = new ObserverAsyncTask<File>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(File file) {
				if (file != null) {
					postPhoto.setFilePath(file.getAbsolutePath());
					PostPhotoActivity.showActivity(getContext(), postPhoto);
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		
		params.add(SaveFileService.PARAM_BITMAP, bitmap);
		params.add(SaveFileService.PARAM_PREFIX, "cropped");
		
		TaErradoServiceBuilder sb = new TaErradoServiceBuilder(getContext());
		sb.setObserverAsyncTask(observer)
			.setNeedConnection(false)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), File.class, new SaveFileService(sb, getString(R.string.base_dir_save_images)));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
    // Saves the state upon rotating the screen/restarting the activity
    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt(ASPECT_RATIO_X, mAspectRatioX);
        bundle.putInt(ASPECT_RATIO_Y, mAspectRatioY);
    }

    // Restores the state upon rotating the screen/restarting the activity
    @Override
    protected void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        mAspectRatioX = bundle.getInt(ASPECT_RATIO_X);
        mAspectRatioY = bundle.getInt(ASPECT_RATIO_Y);
    }
    
	public static void showActivity(Context ctx, PostPhoto postPhoto, int angleRotate) {
		Intent it = new Intent(ctx, CropImageActivity.class);
		it.putExtra(PARAM_POST_PHOTO, postPhoto);
		it.putExtra(PARAM_ROTATE, angleRotate);
		ctx.startActivity(it);
	}
	
}
