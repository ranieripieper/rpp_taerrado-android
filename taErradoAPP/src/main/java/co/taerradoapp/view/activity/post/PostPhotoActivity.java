package co.taerradoapp.view.activity.post;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;

import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;

import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;

import java.io.File;

import co.taerradoapp.R;
import co.taerradoapp.model.PostPhoto;
import co.taerradoapp.model.PostResult;
import co.taerradoapp.service.PostPhotoService;
import co.taerradoapp.service.TaErradoServiceBuilder;
import co.taerradoapp.service.background.ShareFacebookBackgroundService;
import co.taerradoapp.util.BlurBuilder;
import co.taerradoapp.util.Paths;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.util.UrlParameters;
import co.taerradoapp.view.activity.HomeActivity;
import co.taerradoapp.view.activity.base.TaErradoBaseActivity;

public class PostPhotoActivity extends TaErradoBaseActivity {

	private final static String PARAM_POST_PHOTO = "PARAM_POST_PHOTO";
	
    private PostPhoto postPhoto;
    
    private ImageView img;
    private EditTextPlus edt;
    private Switch switchFacebook;
    private Bitmap bitmap;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.post_photo_activity);
		
		tracker.setScreenName("PostImage");
		
		postPhoto = getIntent().getExtras().getParcelable(PARAM_POST_PHOTO);
		
		img = (ImageView)findViewById(R.id.img);
		edt = (EditTextPlus)findViewById(R.id.edt_desc);
		switchFacebook = (Switch)findViewById(R.id.switch_share_facebook);
		
		try {
			bitmap = BitmapUtil.media_getBitmapFromFile(new File(postPhoto.getFilePath()), ScreenUtil.getDisplayWidth(getContext()));
			if (bitmap != null) {
				Bitmap image = BlurBuilder.blur(getContext(), bitmap);
				img.setImageBitmap(image);
			}				
		} catch (Exception e) {
		}
		
		findViewById(R.id.bt_ok).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TaErradoBaseActivity.trackerClick(tracker, "btOkPostPhoto");
                sendPhoto();
            }
        });

		if (TextUtils.isEmpty(SharedPrefManager.getInstance().getFacebookId())) {
			findViewById(R.id.layout_share_facebook).setVisibility(View.INVISIBLE);
		}
	}
	
	@Override
	protected void refresh() {
		super.refresh();
		TaErradoBaseActivity.trackerClick(tracker, "btRepetirPostPhoto");
		sendPhoto();
	}
	
	private void sendPhoto() {
		if (edt.isValid()) {			
			callService();			
		} else {
			showMessage(R.string.msg_preencha_descricao);
		}
	}
	
	private void publishFacebook() {
		Intent intentService = new Intent(this, ShareFacebookBackgroundService.class);
		intentService.putExtra(ShareFacebookBackgroundService.PARAM_DESCRIPTION, edt.getTextStr());
		intentService.putExtra(ShareFacebookBackgroundService.PARAM_FILE_PATH, postPhoto.getFilePath());
		intentService.putExtra(ShareFacebookBackgroundService.PARAM_ADDRESS, postPhoto.getAddress());
		startService(intentService);
	}
	
	private void callService() {
		ObserverAsyncTask<PostResult> observer = new ObserverAsyncTask<PostResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(PostResult result) {
				if (result.isSuccess() && result.getPost() != null) {
					if(switchFacebook.isChecked()) {
						publishFacebook();
					}
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        finish();
                    } else {
                        finishAffinity();
                    }
					HomeActivity.showActivity(getContext(), HomeActivity.PAGE_FEED);
				} else {
					showError(null);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				dismissWaitDialog();
			}
		};	
		
		TaErradoServiceBuilder sb = new TaErradoServiceBuilder(getContext());
		MultiValueMap<String, Object> params = sb.getParams();
		params.add(UrlParameters.PARAM_POST_DESCRIPTION, edt.getTextStr());
		params.add(UrlParameters.PARAM_POST_LAT, String.valueOf(postPhoto.getLatitude()));
		params.add(UrlParameters.PARAM_POST_LNG, String.valueOf(postPhoto.getLongitude()));
		params.add(UrlParameters.PARAM_POST_CITY, postPhoto.getCidade());
		params.add(UrlParameters.PARAM_POST_STREET, postPhoto.getRua());
		params.add(UrlParameters.PARAM_POST_STATE, postPhoto.getEstado());
		
		boolean uploadFile = false;
		if (!TextUtils.isEmpty(postPhoto.getFilePath())) {
			params.add(UrlParameters.PARAM_POST_IMAGE, postPhoto.getFilePath());
			uploadFile = true;
		} else {
			finish();
		}

		sb.setUrl(Paths.POST_PHOTO)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setUploadFile(uploadFile)
			//.addHeaders("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), PostResult.class, new PostPhotoService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	public static void showActivity(Context ctx, PostPhoto postPhoto) {
		Intent it = new Intent(ctx, PostPhotoActivity.class);
		it.putExtra(PARAM_POST_PHOTO, postPhoto);
		ctx.startActivity(it);
	}
}
