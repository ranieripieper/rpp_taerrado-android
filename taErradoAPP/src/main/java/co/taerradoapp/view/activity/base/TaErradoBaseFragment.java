package co.taerradoapp.view.activity.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.gps.GPSTracker;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.facebook.login.LoginManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.springframework.http.HttpStatus;

import java.net.HttpURLConnection;

import co.taerradoapp.R;
import co.taerradoapp.service.exception.ServiceException;
import co.taerradoapp.util.Constants;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.view.activity.SplashLoginActivity;
import retrofit.RetrofitError;

public abstract class TaErradoBaseFragment extends Fragment {

    public static final int REQUEST_CODE_GPS = 998;
    protected GPSTracker gpsTracker;
    protected AsyncTaskService async;
    private ProgressDialog progress;
    protected LoadingView loadingView;
    protected TouchBlackHoleView blackHole;
    private AdView mAdView;

    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    protected boolean isError;
    protected boolean isVisibleToUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        analytics = GoogleAnalytics.getInstance(getActivity());
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker(getString(R.string.google_analytics));
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);

        tracker.setScreenName(getScreenName());
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (isVisibleToUser) {
            if (isError) {
                refresh();
            } else {
                showFragment();
            }
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        isVisibleToUser = visible;
        if (visible && getActivity() != null) {
            if (isError) {
                refresh();
            } else {
                showFragment();
            }
        } else {
            hideFragment();
        }
    }

    public abstract void showFragment();

    public abstract void hideFragment();

    public abstract String getScreenName();

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
        // values/strings.xml.
        mAdView = (AdView) getView().findViewById(R.id.ad_view);

        loadingView = (LoadingView) getView().findViewById(R.id.progress_bar);
        blackHole = (TouchBlackHoleView) getView().findViewById(R.id.black_hole);

        if (mAdView != null) {
            AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
            if (Constants.DEBUG) {
                adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
                mAdView.setVisibility(View.GONE);
            }

            AdRequest adRequest = adRequestBuilder.build();

            // Start loading the ad in the background.
            mAdView.loadAd(adRequest);
        }
    }

    protected void showMessage(String msg) {

        if (isVisibleToUser) {
            showDefaultDialog(getContext(), getString(R.string.fechar), getString(R.string.tentar_novamente), getString(R.string.atencao), msg, null, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    refresh();
                }
            }, false);
        } else {
            isError = true;
        }
    }


    private void showDefaultDialog(Context context, final String strBtLeft, final String strBtRight, String title, String msg, final OnClickListener onClickBtLeft, final OnClickListener onClickBtRight, boolean canceled) {
        TaErradoBaseActivity.showDefaultDialog(context, strBtLeft, strBtRight, title, msg, onClickBtLeft, onClickBtRight, canceled);
    }

    public void showError(RetrofitError error) {
        dismissWaitDialog();
        String msg = getString(R.string.msg_ocorreu_erro);
        if (error != null) {

            boolean showUnauthorizedError = false;
            if (error.getCause() instanceof ServiceException) {
                ServiceException serviceException = (ServiceException)error.getCause();
                if (serviceException.getHttpStatus() != null &&
                        serviceException.getHttpStatus().intValue() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    showUnauthorizedError = true;
                    LoginManager.getInstance().logOut();
                    SharedPrefManager.getInstance().logout();
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        getActivity().finishAffinity();
                    } else {
                        getActivity().finish();
                    }
                    startActivity(new Intent(getActivity(), SplashLoginActivity.class));
                }
            }

            if (!showUnauthorizedError) {
                showMessage(msg);
            }
        }
    }

    public void showError(Exception e) {
        dismissWaitDialog();
        String msg = getString(R.string.msg_ocorreu_erro);
        if (e != null) {

            if (e instanceof ConnectionNotFoundException) {
                msg = getString(R.string.msg_sem_conexao);
            } else if (e instanceof DdsUtilIOException) {
                HttpStatus status = ((DdsUtilIOException) e).getHttpStatus();

                if (status != null && (HttpStatus.UNAUTHORIZED.equals(status))) {
                    LoginManager.getInstance().logOut();
                    SharedPrefManager.getInstance().logout();
                    startActivity(new Intent(getActivity(), SplashLoginActivity.class));
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        getActivity().finishAffinity();
                    } else {
                        getActivity().finish();
                    }
                }
            }

            showMessage(msg);
        }
    }

    protected void refresh() {
    }

    protected void showMessage(int msg) {
        showMessage(getString(msg));
    }

    protected void dismissMessage() {
        //TODO
    }

    protected Context getContext() {
        return getActivity();
    }

    protected boolean verifyGpsEnable() {
        if (gpsTracker == null) {
            gpsTracker = new GPSTracker(getContext());
        }
        if (gpsTracker.canGetLocation()) {
            gpsTracker.stopUsingGPS();
            return true;
        }

        showSnackHabilitarGps();

        return false;
    }

    protected void showSnackHabilitarGps() {
        ((TaErradoBaseActivity) getActivity()).showSnackHabilitarGps(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent callGPSSettingIntent = new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(callGPSSettingIntent, REQUEST_CODE_GPS);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        final LocationManager mlocManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (requestCode == REQUEST_CODE_GPS && resultCode == 0) {
            String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (provider != null) {

                Location loc = gpsTracker.getLocation();

                if (loc == null) {
                    final LocationListener locationListener = new LocationListener() {
                        Boolean search = false;

                        public void onLocationChanged(Location location) {
                            synchronized (search) {
                                if (!search) {
                                    search = true;
                                    mlocManager.removeUpdates(this);
                                    gpsEnable(location);
                                }
                            }
                        }

                        public void onStatusChanged(String provider, int status, Bundle extras) {
                        }

                        public void onProviderEnabled(String provider) {
                        }

                        public void onProviderDisabled(String provider) {
                        }
                    };

                    mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
                    mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2, 0, locationListener);
                } else {
                    gpsEnable(loc);
                }
                /*System.out.println("MainActivity.onActivityResult()");
            	// Define a listener that responds to location updates
                */

            } else {
                showSnackHabilitarGps();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void gpsEnable(Location location) {

    }


    protected void blockScreen(boolean blockScreen) {
        if (blackHole != null) {
            if (blockScreen) {
                blackHole.disable_touch(true);
                blackHole.setVisibility(View.VISIBLE);
            } else {
                blackHole.disable_touch(false);
                blackHole.setVisibility(View.GONE);
            }
        }
    }

    public void showWaitDialog(boolean blockScreen) {
        blockScreen(blockScreen);

        if (loadingView != null) {
            loadingView.setVisibility(View.VISIBLE);
            if (getView() != null && getView().findViewById(R.id.layout_include_loading) != null) {
                getView().findViewById(R.id.layout_include_loading).setVisibility(View.VISIBLE);
            }
        } else {
            progress = ProgressDialog.show(getContext(), getString(R.string.title_aguarde),
                    getString(R.string.msg_aguarde), true);
            progress.show();
        }
    }

    public void dismissWaitDialog() {
        if (blackHole != null) {
            blackHole.disable_touch(false);
            blackHole.setVisibility(View.GONE);
        }
        if (loadingView != null) {
            loadingView.setVisibility(View.GONE);
        }
        if (getView() != null && getView().findViewById(R.id.layout_include_loading) != null) {
            getView().findViewById(R.id.layout_include_loading).setVisibility(View.GONE);
        }
        if (progress != null) {
            progress.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        if (async != null) {
            async.cancel(true);
        }
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }
}
