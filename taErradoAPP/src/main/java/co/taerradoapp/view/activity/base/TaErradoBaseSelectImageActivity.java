package co.taerradoapp.view.activity.base;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;
import co.taerradoapp.R;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;
import com.doisdoissete.android.util.ddsutil.media.MediaScannerNotifier;

public abstract class TaErradoBaseSelectImageActivity extends TaErradoBaseActivity {

	private final int PICTURE_TAKEN_FROM_CAMERA = 9991;
	private final int PICTURE_TAKEN_FROM_GALLERY = 9992;
	protected File outFile = null;
	protected String filePath = null;
	private boolean storeImage = true;
	
	protected abstract void setImageSelect(Bitmap takenPictureData);
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    Bitmap takenPictureData = null;

	    switch(requestCode){

	        case PICTURE_TAKEN_FROM_CAMERA:             
	            if(resultCode==Activity.RESULT_OK) {
	                takenPictureData = handleResultFromCamera(data);
	            }               
	            break;
	        case PICTURE_TAKEN_FROM_GALLERY:                
	            if(resultCode==Activity.RESULT_OK) {
	                takenPictureData = handleResultFromChooser(data);                   
	            }
	            break;          
	    }

	    if(takenPictureData != null) {
	    	setImageSelect(takenPictureData);
	    	
	    }       
	}
	
	public void selectTakePicture(View v) {
		/*AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_escolher_foto)
               .setPositiveButton(R.string.dialog_txt_camera, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   getPictureFromCamera();
                   }
               })
               .setNegativeButton(R.string.dialog_txt_galeria, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   getPictureFromGallery();
                   }
               });
        builder.create().show()*/;
        getPictureFromGallery();
	}
	
	//AUXILIAR
	private Bitmap handleResultFromChooser(Intent data){
	    Bitmap takenPictureData = null;

	    Uri photoUri = data.getData();
	    if (photoUri != null){
	        try {
	            String[] filePathColumn = {MediaStore.Images.Media.DATA};
	            Cursor cursor = getContentResolver().query(photoUri, filePathColumn, null, null, null); 
	            cursor.moveToFirst();
	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            filePath = cursor.getString(columnIndex);
	            cursor.close();

	            takenPictureData = BitmapUtil.media_getBitmapFromFile(new File(filePath), 160);

	        } catch(Exception e) {
	            Toast.makeText(this, "Error getting selected image.", Toast.LENGTH_SHORT).show();
	        }
	    }

	    return takenPictureData;
	}

	private Bitmap handleResultFromCamera(Intent data) {
	    Bitmap takenPictureData = null;

	    if(data != null) {
	        Bundle extras = data.getExtras();
	        if (extras != null && extras.get("data") != null) {
	            takenPictureData = (Bitmap) extras.get("data");
	        }
	    } else {
	        try{
	            takenPictureData = BitmapUtil.media_getBitmapFromFile(outFile, 160);
	            takenPictureData = BitmapUtil.media_correctImageOrientation(outFile.getAbsolutePath());
	        } catch(Exception e) {
	            Toast.makeText(this, "Error getting saved taken picture.", Toast.LENGTH_SHORT).show();;
	        }
	        
	    }

	    if(storeImage) {
	        //We add the taken picture file to the gallery so user can see the image directly                   
	        new MediaScannerNotifier(this, outFile.getAbsolutePath(), "image/*", false);
	        filePath = outFile.getAbsolutePath();
	    }

	    return takenPictureData;
	}

	protected void getPictureFromCamera() {
	    boolean cameraAvailable = BitmapUtil.device_isHardwareFeatureAvailable(this, PackageManager.FEATURE_CAMERA);
	    if(cameraAvailable && 
	    	Util.system_isIntentAvailable(this, MediaStore.ACTION_IMAGE_CAPTURE)) {

	        Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

	        //We prepare the intent to store the taken picture
	        try {
	            File outputDir = BitmapUtil.storage_getExternalPublicFolder(Environment.DIRECTORY_PICTURES, getString(R.string.app_name), true);
	            outFile = BitmapUtil.storage_createUniqueFileName("cameraPic", ".jpg", outputDir);

	            takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outFile));             
	            storeImage = true;
	        } catch(Exception e) {
	            Toast.makeText(this, "Error setting output destination.", Toast.LENGTH_SHORT).show();;
	        }

	        startActivityForResult(takePicIntent, PICTURE_TAKEN_FROM_CAMERA);
	    } else {
	        if (cameraAvailable) {
	            Toast.makeText(this, "No application that can receive the intent camera.", Toast.LENGTH_SHORT).show();;
	        } else {
	            Toast.makeText(this, "No camera present!!", Toast.LENGTH_SHORT).show();;
	        }
	    }
	}

	protected void getPictureFromGallery() {
	    //This takes images directly from gallery
	    Intent gallerypickerIntent = new Intent(Intent.ACTION_PICK);
	    gallerypickerIntent.setType("image/*");
	    startActivityForResult(gallerypickerIntent, PICTURE_TAKEN_FROM_GALLERY); 
	}
}
