package co.taerradoapp.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.text.TextUtils;
import android.transition.Explode;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.doisdoissete.android.util.ddsutil.view.cameragallery.SelectImageActivity;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;

import java.io.File;

import co.taerradoapp.R;
import co.taerradoapp.model.LoginResult;
import co.taerradoapp.service.RetrofitManager;
import co.taerradoapp.util.Constants;
import co.taerradoapp.util.SharedPrefManager;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by ranipieper on 11/3/15.
 */
public class SignUpActivity extends SelectImageActivity {

    private EditTextPlus edtName;
    private EditTextPlus edtEmail;
    private EditTextPlus edtPwd;
    private EditTextPlus edtConfPwd;
    private ImageView imgUser;
    protected LoadingView loadingView;
    protected TouchBlackHoleView blackHole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // set an enter transition
            getWindow().setEnterTransition(new Explode());
            // set an exit transition
            getWindow().setExitTransition(new Explode());
        }

        setContentView(R.layout.signup_activity);

        loadingView = (LoadingView) findViewById(R.id.progress_bar);
        blackHole = (TouchBlackHoleView) findViewById(R.id.black_hole);

        imgUser = (ImageView) findViewById(R.id.img_user);
        edtName = (EditTextPlus) findViewById(R.id.edt_name);
        edtEmail = (EditTextPlus) findViewById(R.id.edt_email);
        edtPwd = (EditTextPlus) findViewById(R.id.edt_password);
        edtConfPwd = (EditTextPlus) findViewById(R.id.edt_conf_password);

        findViewById(R.id.bt_cadastrar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSignUp();
            }
        });

        imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTakePicture(v, getString(R.string.APP_NAME));
            }
        });
    }

    @Override
    protected void setImageSelect(Bitmap takenPictureData) {
        imgUser.setImageBitmap(takenPictureData);
    }

    private void callSignUp() {
        if (formIsValid()) {

            String[] nameArray = edtName.getTextStr().split(" ");
            String name = nameArray[0];
            String surname = "";
            if (nameArray.length > 1) {
                surname = edtName.getTextStr().substring(nameArray[0].length() + 1, edtName.getTextStr().length());
            }
            showWaitDialog();
            TypedFile image = null;
            if (!TextUtils.isEmpty(filePath)) {
                image = new TypedFile("image/jpeg", new File(filePath));
            }
            RetrofitManager.getInstance().getUserService().signup(edtEmail.getTextStr(),
                    name, surname, edtPwd.getTextStr(), edtConfPwd.getTextStr(),
                    Constants.PLATFORM, SharedPrefManager.getInstance().getDeviceToken(),
                    image,
                    new retrofit.Callback<LoginResult>() {
                        @Override
                        public void success(LoginResult loginResult, Response response) {
                            dismissWaitDialog();
                            Toast.makeText(SignUpActivity.this, R.string.cadastro_realizado_com_sucesso, Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            dismissWaitDialog();
                        }
                    });
        }
    }

    private boolean formIsValid() {
        edtName.hiddenKeyboard(this);
        edtEmail.hiddenKeyboard(this);
        edtPwd.hiddenKeyboard(this);
        edtConfPwd.hiddenKeyboard(this);

        if (edtName.isNotValid()) {
            Toast.makeText(this, R.string.name_not_valid, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (edtEmail.isNotValid()) {
            Toast.makeText(this, R.string.email_not_valid, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (edtPwd.isNotValid()) {
            Toast.makeText(this, R.string.pwd_not_valid, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (edtConfPwd.isNotValid()) {
            Toast.makeText(this, R.string.conf_pwd_not_valid, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!edtPwd.getTextStr().equals(edtConfPwd.getTextStr())) {
            Toast.makeText(this, R.string.conf_pwd_not_match, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public static void showActivity(Activity ctx, Pair<View, String>... sharedElements) {

        Intent intent = new Intent(ctx, SignUpActivity.class);


/*
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            Bundle bndlanimation =
                    ActivityOptions.makeCustomAnimation(ctx, R.anim.pull_up_from_bottom, R.anim.push_out_to_bottom).toBundle();
            ctx.startActivity(intent, bndlanimation);


            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    // the context of the activity
                    ctx,
                    sharedElements);

            options.makeCustomAnimation(ctx,
                    R.anim.pull_up_from_bottom, R.anim.push_out_to_bottom);

        } else {
            ctx.startActivity(intent);
        }
        */
        ctx.startActivity(intent);
    }


    public void showWaitDialog() {
        lockScreen();

        if (loadingView != null) {
            loadingView.setVisibility(View.VISIBLE);
            if (findViewById(R.id.layout_include_loading) != null) {
                findViewById(R.id.layout_include_loading).setVisibility(View.VISIBLE);
            }
        }
    }

    private void lockScreen() {
        if (findViewById(R.id.layout_include_loading) != null) {
            findViewById(R.id.layout_include_loading).setVisibility(View.VISIBLE);
        }
        if (blackHole != null) {
            blackHole.disable_touch(true);
            blackHole.setVisibility(View.VISIBLE);
        }
    }

    private void unlockScreen() {
        if (blackHole != null) {
            blackHole.disable_touch(false);
            blackHole.setVisibility(View.GONE);
        }
        if (findViewById(R.id.layout_include_loading) != null) {
            findViewById(R.id.layout_include_loading).setVisibility(View.GONE);
        }
    }

    public void dismissWaitDialog() {
        unlockScreen();

        if (loadingView != null) {
            loadingView.setVisibility(View.GONE);
        }
        if (findViewById(R.id.layout_include_loading) != null) {
            findViewById(R.id.layout_include_loading).setVisibility(View.GONE);
        }
    }

}
