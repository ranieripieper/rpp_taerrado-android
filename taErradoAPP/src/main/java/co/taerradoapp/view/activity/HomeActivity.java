package co.taerradoapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.facebook.login.LoginManager;

import co.taerradoapp.R;
import co.taerradoapp.TaErradoApplication;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.view.activity.base.TaErradoBaseActivity;
import co.taerradoapp.view.activity.post.TakePictureActivity;
import co.taerradoapp.view.fragment.FeedFragment;
import co.taerradoapp.view.fragment.MapFragment;
import co.taerradoapp.view.fragment.ScreenSlidePageFragment;
import co.taerradoapp.view.fragment.UserInfoFragment;

public class HomeActivity extends TaErradoBaseActivity {

	/**
	 * The number of pages (wizard steps) to show in this demo.
	 */
	private static final int NUM_PAGES = 3;
	
	public static final String PARAM_INTENT_PAGE = "PARAM_INTENT_PAGE";
	public static final int PAGE_MAP = 0;
	public static final int PAGE_FEED = 1;
	public static final int PAGE_INFO = 2;


	/**
	 * The pager widget, which handles animation and allows swiping horizontally
	 * to access previous and next wizard steps.
	 */
	private ViewPager mPager;

	/**
	 * The pager adapter, which provides the pages to the view pager widget.
	 */
	private PagerAdapter mPagerAdapter;

	//MENU
	private ImageView imgMenuMap;
	private ImageView imgMenuFeed;
	private ImageView imgMenuProfile;
	private ImageView imgMenuLogout;
	private View viewBlockScreen;
	private View layoutMenu;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        TaErradoApplication.getGpsTracker(this).getLocation();

		mPager = (ViewPager) findViewById(R.id.pager);
		imgMenuMap = (ImageView)findViewById(R.id.icn_menu_map);
		imgMenuFeed = (ImageView)findViewById(R.id.icn_menu_feed);
		imgMenuProfile = (ImageView)findViewById(R.id.icn_menu_profile);
		imgMenuLogout = (ImageView)findViewById(R.id.icn_menu_logout);
		viewBlockScreen = findViewById(R.id.black_hole);
		layoutMenu = findViewById(R.id.layout_menu);

		mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
		mPager.setAdapter(mPagerAdapter);

		configMenu();
		mPager.setOffscreenPageLimit(NUM_PAGES);
		int currentPage = PAGE_MAP;
		if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getInt(PARAM_INTENT_PAGE) >= 0) {
			currentPage = getIntent().getExtras().getInt(PARAM_INTENT_PAGE);
		}

		mPager.setCurrentItem(currentPage);
	}
	
	protected int getNavigationIcon() {
		return R.drawable.icn_menu;
	}
    
	private void configMenu() {
		viewBlockScreen.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showHideMenu();
			}
		});
		
		layoutMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showHideMenu();
			}
		});
		
		imgMenuMap.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mPager.setCurrentItem(PAGE_MAP);
				showHideMenu();
			}
		});
		imgMenuFeed.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mPager.setCurrentItem(PAGE_FEED);
				showHideMenu();
			}
		});
		imgMenuProfile.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mPager.setCurrentItem(PAGE_INFO);
				showHideMenu();
			}
		});
		imgMenuLogout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				hideMenu(new Animation.AnimationListener() {					
					@Override
					public void onAnimationStart(Animation animation) {
					}
					
					@Override
					public void onAnimationRepeat(Animation animation) {
					}
					
					@Override
					public void onAnimationEnd(Animation animation) {
						LoginManager.getInstance().logOut();
						SharedPrefManager.getInstance().logout();
						startActivity(new Intent(HomeActivity.this, SplashLoginActivity.class));
						finish();
					}
				});	

			}
		});
	}
	
	public void showFeed() {
		mPager.setCurrentItem(1);
	}
	
	private void showHideMenu() {
		if (!menuIsShowing) {
			showMenu();
			menuIsShowing = true;
		} else {
			hideMenu();
			menuIsShowing = false;
		}
	}
	
	private float ROTATE_MENU_DEGREE = 90;
	private boolean menuIsShowing = false;
	private final int ANIMATION_MENU_TIME = 500;
	
	private void showMenu() {
		AnimationSet animSet1 = createAnimationMenu(ROTATE_MENU_DEGREE, 0, -imgMenuMap.getHeight(), 10, 0, null);
		AnimationSet animSet2 = createAnimationMenu(ROTATE_MENU_DEGREE, 0, -imgMenuMap.getHeight(), 10, ANIMATION_MENU_TIME/4, null);
		AnimationSet animSet3 = createAnimationMenu(ROTATE_MENU_DEGREE, 0, -imgMenuMap.getHeight(), 10, ANIMATION_MENU_TIME/3, null);
		AnimationSet animSet4 = createAnimationMenu(ROTATE_MENU_DEGREE, 0, -imgMenuMap.getHeight(), 10, ANIMATION_MENU_TIME/2, null);
		
		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setDuration(ANIMATION_MENU_TIME);
		fadeIn.setAnimationListener(new Animation.AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				viewBlockScreen.setVisibility(View.VISIBLE);
				layoutMenu.setVisibility(View.VISIBLE);
				viewBlockScreen.bringToFront();
				layoutMenu.bringToFront();
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
			}
		});
		viewBlockScreen.setVisibility(View.VISIBLE);
		layoutMenu.setVisibility(View.VISIBLE);
		viewBlockScreen.startAnimation(fadeIn);
    	imgMenuMap.startAnimation(animSet1);
    	imgMenuFeed.startAnimation(animSet2);
    	imgMenuProfile.startAnimation(animSet3);
    	imgMenuLogout.startAnimation(animSet4);    	
	}
	
	private void hideMenu() {
		hideMenu(null);
	}
	
	private void hideMenu(AnimationListener animationListener) {
		AnimationSet animSet1 = createAnimationMenu(0, ROTATE_MENU_DEGREE, 10, -imgMenuMap.getHeight(), ANIMATION_MENU_TIME/2, null);
		AnimationSet animSet2 = createAnimationMenu(0, ROTATE_MENU_DEGREE, 10, -imgMenuMap.getHeight(), ANIMATION_MENU_TIME/3, null);
		AnimationSet animSet3 = createAnimationMenu(0, ROTATE_MENU_DEGREE, 10, -imgMenuMap.getHeight(), ANIMATION_MENU_TIME/4, null);
		AnimationSet animSet4 = createAnimationMenu(0, ROTATE_MENU_DEGREE, 10, -imgMenuMap.getHeight(), 0, null);
		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setDuration(ANIMATION_MENU_TIME);
		fadeOut.setAnimationListener(new Animation.AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				viewBlockScreen.setVisibility(View.GONE);
				layoutMenu.setVisibility(View.INVISIBLE);
			}
		});
		if (animationListener != null) {
			animSet4.setAnimationListener(animationListener);
		}
    	imgMenuMap.startAnimation(animSet1);
    	imgMenuFeed.startAnimation(animSet2);
    	imgMenuProfile.startAnimation(animSet3);
    	imgMenuLogout.startAnimation(animSet4);
    	viewBlockScreen.startAnimation(fadeOut);
	}
	
	private AnimationSet createAnimationMenu(float rotateDegreeFrom, float rotateDegreeTo, float fromY, float toY, long startOffset, AnimationListener listener) {
		Animation slideDown = new TranslateAnimation(0, 0, fromY, toY);
    	slideDown.setDuration(ANIMATION_MENU_TIME);
    	Animation rotate = new RotateAnimation(rotateDegreeFrom, rotateDegreeTo, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
    	rotate.setDuration(ANIMATION_MENU_TIME);
    	
    	final AnimationSet animSet = new AnimationSet(true);
    	animSet.addAnimation(rotate);
    	animSet.addAnimation(slideDown);
    	animSet.setInterpolator(new DecelerateInterpolator());
    	animSet.setStartOffset(startOffset);
    	animSet.setFillAfter(true);
    	animSet.setFillEnabled(true);
    	
    	if (listener != null) {
    		animSet.setAnimationListener(listener);
    	}
    	
    	return animSet;
	}
	
	

	@Override
	public void onBackPressed() {
		if (mPager.getCurrentItem() == 0) {
			// If the user is currently looking at the first step, allow the
			// system to handle the
			// Back button. This calls finish() on this activity and pops the
			// back stack.
			super.onBackPressed();
		} else {
			// Otherwise, select the previous step.
			mPager.setCurrentItem(mPager.getCurrentItem() - 1);
		}
	}

	/**
	 * A simple pager adapter that represents 5 ScreenSlidePageFragment objects,
	 * in sequence.
	 */
	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		public ScreenSlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			if (position == 0) {
				return new MapFragment();
			} else if (position == 1) {
				return new FeedFragment();
			} else if (position == 2) {
				return new UserInfoFragment();
			}/* else {
				return new FeedFragment();
			}*/
			return new ScreenSlidePageFragment();
		}

		@Override
		public int getCount() {
			return NUM_PAGES;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.home, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			showHideMenu();
			return true;
		case R.id.action_take_picture:
			TakePictureActivity.showActivity(getContext());
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public static void showActivity(Context ctx, int page) {
		Intent it = new Intent(ctx, HomeActivity.class);
		it.putExtra(PARAM_INTENT_PAGE, page);
		ctx.startActivity(it);
	}

    @Override
    protected void onDestroy() {
        TaErradoApplication.getGpsTracker(this).stopUsingGPS();
        super.onDestroy();
    }
}
