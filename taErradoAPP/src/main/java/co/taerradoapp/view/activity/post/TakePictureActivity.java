package co.taerradoapp.view.activity.post;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.gps.GPSTracker;
import com.doisdoissete.android.util.ddsutil.service.SaveFileService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.ActionBarUtil;
import com.doisdoissete.android.util.ddsutil.view.cameragallery.CameraActivity;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.io.IOException;

import co.taerradoapp.R;
import co.taerradoapp.TaErradoApplication;
import co.taerradoapp.model.GoogleGeocodingResult;
import co.taerradoapp.model.PostPhoto;
import co.taerradoapp.service.TaErradoServiceBuilder;
import co.taerradoapp.util.Constants;
import co.taerradoapp.util.Paths;
import co.taerradoapp.util.UrlParameters;
import co.taerradoapp.view.activity.base.TaErradoBaseActivity;

public class TakePictureActivity extends CameraActivity {
	
	protected AsyncTaskService async;
	protected LoadingView loadingView;
	protected TouchBlackHoleView blackHole;
	protected Toolbar toolbar;
	public static final int REQUEST_CODE_GPS = 9000;
	public static Tracker tracker;
	public static GoogleAnalytics analytics;
	private Location mLocation;

	private GPSTracker mGpsTracker;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.post_take_picture_activity);
		analytics = GoogleAnalytics.getInstance(this);
	    analytics.setLocalDispatchPeriod(1800);

		mGpsTracker = TaErradoApplication.getGpsTracker(this);
	    tracker = analytics.newTracker(getString(R.string.google_analytics));
	    tracker.enableExceptionReporting(true);
	    tracker.enableAdvertisingIdCollection(true);
	    tracker.enableAutoActivityTracking(true);

		configActionBar();
		loadingView = (LoadingView)findViewById(R.id.progress_bar);
		blackHole = (TouchBlackHoleView)findViewById(R.id.black_hole);
		
		if (mGpsTracker != null && !mGpsTracker.isGPSEnabled()) {
			showGpsDisabled();			
		} else {
			verifyGpsEnable();
		}
	}
	
	protected boolean canCapturePicture() {
		TaErradoBaseActivity.trackerClick(tracker, "btCapture");
		if (mGpsTracker != null && mGpsTracker.isGPSEnabled() && mGpsTracker.canGetLocation()) {
			mLocation = mGpsTracker.getLocation();
			if (mLocation != null) {
				return true;
			}
		}
		showGpsDisabled();
		
		return false;
	}
	
	//GPS
    protected boolean verifyGpsEnable() {
		if(mGpsTracker.isGPSEnabled() && mGpsTracker.canGetLocation()){
			return true;
		}
		
		showGpsDisabled(null);
		
		return false;
	}
    
    public void showGpsDisabled() {
    	showGpsDisabled(null);
    }
    
    public void showGpsDisabled(final View.OnClickListener onclick) {
		TaErradoBaseActivity.showDefaultDialog(getContext(), getString(R.string.cancelar), getString(R.string.habilitar), getString(R.string.atencao), getString(R.string.msg_habilitar_gps), null, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (onclick == null) {
                    Intent callGPSSettingIntent = new Intent(
                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(callGPSSettingIntent, REQUEST_CODE_GPS);
                } else {
                    onclick.onClick(v);
                }

            }
        }, false);
	}
	
	public void showWaitDialog(boolean blockScreen) {
		blockScreen(blockScreen);
		
		if (loadingView != null) {
			loadingView.setVisibility(View.VISIBLE);
			if (findViewById(R.id.layout_include_loading) != null) {
				findViewById(R.id.layout_include_loading).setVisibility(View.VISIBLE);
			}
		}
	}
	
	public void dismissWaitDialog() {
		if (blackHole != null) {
			blackHole.disable_touch(false);
			blackHole.setVisibility(View.GONE);
		}
		if (loadingView != null) {
			loadingView.setVisibility(View.GONE);
		}
		if (findViewById(R.id.layout_include_loading) != null) {
			findViewById(R.id.layout_include_loading).setVisibility(View.GONE);
		}
	}
	
	protected void blockScreen(boolean blockScreen) {
		if (blackHole != null) {
			if (blockScreen) {
				blackHole.disable_touch(true);
				blackHole.setVisibility(View.VISIBLE);
			} else {
				blackHole.disable_touch(false);
				blackHole.setVisibility(View.GONE);
			}
		}
	}
    private void configActionBar() {
    	toolbar = (Toolbar)findViewById(R.id.taerrado_toolbar);
		if (toolbar != null) {
			toolbar.setNavigationIcon(R.drawable.ic_back);
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			ActionBarUtil.hideHomeAsUp(getWindow());
		}    	
    }
    
	@Override
	protected Integer getButtonGalleryId() {
		return R.id.open_gallery;
	}
	
	@Override
	protected Integer getButtonCaptureId() {
		return R.id.button_capture;
	}
	@Override
	protected Integer getButtonSwitchCameraId() {
		return R.id.switch_camera;
	}
	
	@Override
	protected Integer getBottomCoverViewId() {
		return R.id.cover_top_view;
	}

	@Override
	protected Integer getButtonFlashId() {
		return null;
	}

	@Override
	protected Integer getSquareCameraPreviewId() {
		return R.id.camera_preview_view;
	}

	@Override
	protected Integer getTextAutoFlashId() {
		return null;
	}

	@Override
	protected Integer getTopCoverViewId() {
		return R.id.cover_bottom_view;
	}

	
	private void callServiceSaveFile(final byte[] data, final int rotation) {
		ObserverAsyncTask<File> observer = new ObserverAsyncTask<File>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(File file) {
				dismissWaitDialog();
				if (file != null) {
					startPostCropActivity(mLocation.getLatitude(), mLocation.getLongitude(), file.getAbsolutePath(), ExifInterface.ORIENTATION_NORMAL, false);
				} else {
					TaErradoBaseActivity.showMessageOnlyClose(getContext(), R.string.msg_generic_error);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				startCameraPreview();
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onError(Exception e) {
				TaErradoBaseActivity.showError(TakePictureActivity.this, e, true);
				startCameraPreview();
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		
		params.add(SaveFileService.PARAM_DATA, data);
		params.add(SaveFileService.PARAM_DATA_ROTATION, rotation);
		params.add(SaveFileService.PARAM_PREFIX, "taked");
		
		TaErradoServiceBuilder sb = new TaErradoServiceBuilder(getContext());
		sb.setObserverAsyncTask(observer)
			.setNeedConnection(false)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), File.class, new SaveFileService(sb, getString(R.string.base_dir_save_images)));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void startPostCropActivity(final double latitude, final double longitude, final String filePath, final int ongleRotate, final boolean cropImage) {
		ObserverAsyncTask<GoogleGeocodingResult> observer = new ObserverAsyncTask<GoogleGeocodingResult>() {
			@Override
			public void onPreExecute() {
				stopCameraPreview();
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(GoogleGeocodingResult result) {
				if (result != null && result.getResults() != null && result.getResults().length > 0) {
					String cidade = result.getResults()[0].getCity();
					String rua = result.getResults()[0].getStreet();
					String estado = result.getResults()[0].getState();
					if (TextUtils.isEmpty(rua) || TextUtils.isEmpty(cidade)) {
						TaErradoBaseActivity.showMessage(getContext(), getString(R.string.msg_erro_recuperar_endereco), null);
					} else {
						PostPhoto postPhoto = new PostPhoto();
						postPhoto.setLatitude(latitude);
						postPhoto.setLongitude(longitude);
						postPhoto.setFilePath(filePath);
						postPhoto.setFilePathPreCrop(filePath);
						postPhoto.setCidade(cidade);
						postPhoto.setEstado(estado);
						postPhoto.setRua(rua);
						if (cropImage) {
							CropImageActivity.showActivity(getContext(), postPhoto, ongleRotate);
						} else {
							PostPhotoActivity.showActivity(getContext(), postPhoto);
						}
					}
				} else {
					TaErradoBaseActivity.showMessageOnlyClose(getContext(), R.string.msg_erro_recuperar_endereco);
					startCameraPreview();
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				startCameraPreview();
			}

			@Override
			public void onError(Exception e) {
				dismissWaitDialog();
				TaErradoBaseActivity.showMessageOnlyClose(getContext(), R.string.msg_erro_recuperar_endereco);
				startCameraPreview();
			}
		};

		TaErradoServiceBuilder sb = new TaErradoServiceBuilder(this);

		MultiValueMap<String, String> params = sb.getParams();
		String url = Paths.GOOGLE_GEOCODING.replace(UrlParameters.LAT_URL, String.valueOf(latitude)).replace(UrlParameters.LNG_URL, String.valueOf(longitude));
		sb.setUrl(url)
				.setObserverAsyncTask(observer)
				.setHttpMethod(HttpMethod.GET)
				.setNeedConnection(true)
				.setParams(params);

		async = sb.mappingInto(this, GoogleGeocodingResult.class);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
		
	}
	
	private boolean photoWithLatLng(float latitude, float longitude) {
		if (latitude == -1 || longitude == -1) {
			TaErradoBaseActivity.showDefaultDialog(getContext(), getString(R.string.ok), getString(R.string.erro), getString(R.string.msg_foto_sem_latitude_longitude), null);
			return false;
		} else {
			return true;
		}
	}
	

	private Float convertToDegree(String stringDMS) {
		Float result = null;
		String[] DMS = stringDMS.split(",", 3);

		String[] stringD = DMS[0].split("/", 2);
		Double D0 = new Double(stringD[0]);
		Double D1 = new Double(stringD[1]);
		Double FloatD = D0 / D1;

		String[] stringM = DMS[1].split("/", 2);
		Double M0 = new Double(stringM[0]);
		Double M1 = new Double(stringM[1]);
		Double FloatM = M0 / M1;

		String[] stringS = DMS[2].split("/", 2);
		Double S0 = new Double(stringS[0]);
		Double S1 = new Double(stringS[1]);
		Double FloatS = S0 / S1;

		result = new Float(FloatD + (FloatM / 60) + (FloatS / 3600));

		return result;

	};
	
	/*
	 * (non-Javadoc)
	 * @see com.doisdoissete.android.util.ddsutil.view.cameragallery.CameraActivity#onPictureSelected(java.lang.String, android.graphics.Bitmap)
	 */
	@Override
	protected void onPictureSelected(String filePath, Bitmap bitmap) {
		ExifInterface exif;
		int orientation = ExifInterface.ORIENTATION_NORMAL;
		try {
			exif = new ExifInterface(filePath);
			//recupera coordenadas
			String LATITUDE = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
			 String LATITUDE_REF = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
			 String LONGITUDE = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
			 String LONGITUDE_REF = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);
			 // your Final lat Long Values
			 Float latitude = -1f;
			 Float longitude = -1f;

			if ((LATITUDE != null) && (LATITUDE_REF != null)
					&& (LONGITUDE != null) && (LONGITUDE_REF != null)) {

				if (LATITUDE_REF.equals("N")) {
					latitude = convertToDegree(LATITUDE);
				} else {
					latitude = 0 - convertToDegree(LATITUDE);
				}

				if (LONGITUDE_REF.equals("E")) {
					longitude = convertToDegree(LONGITUDE);
				} else {
					longitude = 0 - convertToDegree(LONGITUDE);
				}
			}
			
			if (photoWithLatLng(latitude, longitude)) {
				exif.getAttributeDouble(ExifInterface.TAG_GPS_LATITUDE, -1d);
				orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
				startPostCropActivity(latitude, longitude, filePath, orientation, true);
			}			
		} catch (IOException e) {
		}
	}
	
	 /**
     * A picture has been taken
     * @param data
     * @param rotation
     */
    @Override
    public void onPictureTaken(byte[] data, int rotation) {
    	callServiceSaveFile(data, rotation);
    }
	
    
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		dismissWaitDialog();
		final LocationManager mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        if(requestCode == REQUEST_CODE_GPS && resultCode == 0) {
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if(provider != null){
            	
            	Location loc = mGpsTracker.getLocation();

            	if (loc == null) {
            		final LocationListener locationListener = new LocationListener() {
                    	Boolean search = false;
                        public void onLocationChanged(Location location) {
                        	synchronized (search) {
                        		if (!search) {
                        			search = true;
    	                    		mlocManager.removeUpdates(this); 
                        		}
    						}              	
                        }

                        public void onStatusChanged(String provider, int status, Bundle extras) {}

                        public void onProviderEnabled(String provider) {}

                        public void onProviderDisabled(String provider) {}
                      };

                      mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
                      mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2, 0, locationListener);
            	}
            } else {
            	showGpsDisabled(null);
            }
        } else {
        	super.onActivityResult(requestCode, resultCode, data);
        }
    }
	
	public static void showActivity(Context ctx) {
		ctx.startActivity(new Intent(ctx, TakePictureActivity.class));
	}

}