package co.taerradoapp.view.fragment;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import co.taerradoapp.BuildConfig;
import co.taerradoapp.R;
import co.taerradoapp.model.GetPostsResult;
import co.taerradoapp.model.Post;
import co.taerradoapp.service.TaErradoServiceBuilder;
import co.taerradoapp.util.Paths;
import co.taerradoapp.util.UrlParameters;
import co.taerradoapp.view.activity.HomeActivity;
import co.taerradoapp.view.activity.base.TaErradoBaseFragment;
import co.taerradoapp.view.adapter.MarkerAdapter;

public class MapFragment extends TaErradoBaseFragment {

	private HashMap<Marker, Post> markers = new HashMap<Marker, Post>();
    private Location location;
    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;
    private TextView txtNrPosts;
    private TextView txtDenunciasHj;
    private ArrayList<LatLng> latLngMove = new ArrayList<LatLng>(); 
    private boolean firstCall = true;
	private List<Post> allPosts = new ArrayList<>();
    private boolean verifyVersion = true;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(
				R.layout.fragment_map, container, false);

		SupportMapFragment supportMapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);
		if (mMap != null) {
			mMap = supportMapFragment.getMap();
		}
			
		txtNrPosts = (TextView)rootView.findViewById(R.id.txt_nr_denuncias);
		txtDenunciasHj = (TextView)rootView.findViewById(R.id.txt_label_denuncias);
		
		return rootView;
	}
	
	@Override
	public String getScreenName() {
		return "Map";
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		if (mMap == null) {
			mMapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);
			if (mMapFragment != null) {
				mMap = mMapFragment.getMap();
			}    		
    	}
		initMap();
		if (mMap != null) {
			callService(null);
		}		
	}
	
	@Override
	public void showFragment() {
	}
	
	@Override
	protected void refresh() {
		super.refresh();
		initMap();
		if (mMap != null) {
			callService(null);
		}
	}
	
	@Override
	public void hideFragment() {
	}
	
	private void callService(Location pLocation) {
		if (pLocation != null || verifyGpsEnable()) {
		
			ObserverAsyncTask<GetPostsResult> observer = new ObserverAsyncTask<GetPostsResult>() {
				@Override
				public void onPreExecute() {
					if (firstCall) {
						firstCall = false;
						showWaitDialog(false);
					}
				}
	
				@Override
				public void onPostExecute(GetPostsResult result) {
					isError = false;
					if (result.isSuccess() && result.getPosts() != null) {
						populateMap(result.getPosts());

						if (result.getCountPosts() > 0) {
							getView().findViewById(R.id.layout_posts_dia).setVisibility(View.VISIBLE);
							getView().findViewById(R.id.layout_posts_dia).setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									((HomeActivity)getActivity()).showFeed();
								}
							});
							txtNrPosts.setText(result.getCountPostsStr());
							if (result.getCountPosts() > 1) {
								txtDenunciasHj.setText(R.string.denuncias_feitas_hoje);
							} else {
								txtDenunciasHj.setText(R.string.denuncia_feitas_hoje);
							}
							getView().findViewById(R.id.ad_view).setVisibility(View.GONE);
						} else {
							getView().findViewById(R.id.layout_posts_dia).setVisibility(View.GONE);
						}

                        if (verifyVersion && result.getAndroidVersion() > 0) {
                            verifyVersion = false;
                            if (result.getAndroidVersion() > BuildConfig.VERSION_CODE) {
                                HomeActivity.showNewVersion(getActivity());
                            }
                        }
					} else {
						showError(null);
					}
					dismissWaitDialog();
					executeNext();
					listenerMoveMap();
				}
	
				@Override
				public void onCancelled() {
					dismissWaitDialog();
				}
	
				@Override
				public void onError(Exception e) {
					dismissWaitDialog();
					showError(e);
					latLngMove = new ArrayList<LatLng>();
					listenerMoveMap();					
				}
			};
	
			if (pLocation == null) {
				location = gpsTracker.getLocation();
			} else {
				location = pLocation;
			}
			if (location == null) {
				showSnackHabilitarGps();
				return;
			}
			if (pLocation == null) {
				LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
				if (mMap != null) {
					mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
				}
			}
			
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			params.add(UrlParameters.DATE, DateUtil.dfDiaMesAno.get().format(new Date()));
			params.add(UrlParameters.LAT, String.valueOf(location.getLatitude()));
			params.add(UrlParameters.LNG, String.valueOf(location.getLongitude()));

			TaErradoServiceBuilder sb = new TaErradoServiceBuilder(getContext());
			sb.setUrl(Paths.GET_POSTS_MAP)
				.setObserverAsyncTask(observer)
				.setNeedConnection(true)
				.setHttpMethod(HttpMethod.GET)
				.setParams(params);
		
			async = sb.mappingInto(getContext(), GetPostsResult.class);
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			} else {
				async.execute();
			}	
			
		}
	}
	
	private void listenerMoveMap() {
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				mMap.setOnCameraChangeListener(null);
				mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
					@Override
					public void onCameraChange(CameraPosition position) {
						moveMap(mMap.getCameraPosition().target);
					}
				});
			}
		}, 2000);		
		
	}
	
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	if (mMap == null) {
    		SupportMapFragment supportMapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);
    		mMap = supportMapFragment.getMap();
    		initMap();
    	}
    }
    
    
	private void populateMap(List<Post> newPosts) {
		if (mMap == null) {
			mMapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);
			if (mMapFragment != null) {
				mMap = mMapFragment.getMap();
			}   
		}
		if (mMap == null) {
			return;
		}
		
		LatLng loc = null; 
		LayoutInflater layoutInflater = ((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE));

        Collections.sort(newPosts, new Comparator<Post>() {
            @Override
            public int compare(Post lhs, Post rhs) {
                return lhs.getId().compareTo(rhs.getId());
            }
        });

        Collections.sort(allPosts, new Comparator<Post>() {
            @Override
            public int compare(Post lhs, Post rhs) {
                return lhs.getId().compareTo(rhs.getId());
            }
        });

		for (Post post : newPosts) {

            //filtra os posts
            if (allPosts.contains(post)) {
                continue;
            }
			Marker m = mMap.addMarker(new MarkerOptions()
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_pin))
				.position(new LatLng(post.getLat(), post.getLng()))
				.title(post.getStreet())
				.draggable(true)
				.flat(true)
				.snippet(post.getCity()));
			
			View layoutMarker = layoutInflater.inflate(R.layout.post_marker, null);
			mMap.setInfoWindowAdapter(new MarkerAdapter(layoutInflater, getActivity(), markers, layoutMarker));
            allPosts.add(post);
			markers.put(m, post);
		}
	}
	
	public void gpsEnable(Location pLocation) {
		location = pLocation;
		if (mMap != null && location != null) {
			LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
		}
		
		callService(location);
	}
	
    private void initMap() {
    	if (mMap != null) {
	        mMap.getUiSettings().setZoomControlsEnabled(false);
	        mMap.setMyLocationEnabled(true);
	        
	        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
				
				@Override
				public void onInfoWindowClick(Marker marker) {
				}
			});
	    }		
    }
    
    private void moveMap(LatLng latLng) {
    	
    	if (async == null || !async.getStatus().equals(Status.RUNNING)) {
    		Location loc = new Location("");
    		loc.setLatitude(latLng.latitude);
    		loc.setLongitude(latLng.longitude);
    		callService(loc);
    	} else {
    		synchronized (latLngMove) {
    			latLngMove.add(latLng);
			}    		
    	}
    }
    
    private void executeNext() {
    	synchronized (latLngMove) {
	    	if (latLngMove != null && !latLngMove.isEmpty()) {
	    		LatLng latLng = latLngMove.remove(latLngMove.size()-1);
	    		Location loc = new Location("");
	    		loc.setLatitude(latLng.latitude);
	    		loc.setLongitude(latLng.longitude);
	    		callService(loc);
	    	}
    	}
    }

}
