package co.taerradoapp.view.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import co.taerradoapp.R;
import co.taerradoapp.TaErradoApplication;
import co.taerradoapp.model.Post;
import co.taerradoapp.model.User;
import co.taerradoapp.model.UserInfoResult;
import co.taerradoapp.service.RetrofitManager;
import co.taerradoapp.util.BlurBuilder;
import co.taerradoapp.util.Constants;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.view.activity.base.TaErradoBaseFragment;
import co.taerradoapp.view.adapter.PostAdapter;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class UserInfoFragment extends TaErradoBaseFragment {

	private PagingListView lstView;
	private PostAdapter adapter;
	private int page = 0;
	
	private ImageView imgBgUser;
	private ImageView imgUser;
	
	private TextViewPlus txtDias;
	private TextViewPlus txtNrFotos;
	private TextViewPlus txtXingamentos;
	private View headerView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_user_info, container, false);

		lstView = (PagingListView)rootView.findViewById(R.id.layout_content);
		createHeader();
		
		return rootView;
	}
	
	@Override
	public void showFragment() {
		if (async == null || (
				(async.isCancelled() || !async.isRunning()) && (adapter == null || adapter.getCount() <= 0)) || SharedPrefManager.getInstance().isUploadFoto()) {
			page = 1;
			if (adapter != null) {
				adapter.removeAllItems();
				adapter.notifyDataSetChanged();
			}
    		callService();
    	}
	}
	
	@Override
	protected void refresh() {
		super.refresh();
		showFragment();
	}
	
	@Override
	public void hideFragment() {
	}
	
	@Override
	public String getScreenName() {
		return "UserInfo";
	}
	
	private void createHeader() {
		LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		headerView = inflater.inflate(R.layout.include_header_user_info, null);
		imgBgUser = (ImageView)headerView.findViewById(R.id.img_bg_user);
		imgUser = (ImageView)headerView.findViewById(R.id.img_user);
		txtDias = (TextViewPlus)headerView.findViewById(R.id.txt_dias_app);
		txtNrFotos = (TextViewPlus)headerView.findViewById(R.id.txt_fotos_enviadas);
		txtXingamentos = (TextViewPlus)headerView.findViewById(R.id.txt_nr_xingamentos_user);
		
		TaErradoApplication.imageLoaderUser.loadImage(SharedPrefManager.getInstance().getProfileImageUrl(),
			new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String imageUri, View view) {
				}
				
				@Override
				public void onLoadingFailed(String imageUri, View view,
						FailReason failReason) {					
				}
				
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					if (loadedImage != null) {
						if (imgUser != null) {
							imgUser.setImageBitmap(loadedImage);
						}

						Bitmap workingBitmap = Bitmap.createBitmap(loadedImage);
						Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);
						Bitmap image = BlurBuilder.blur(getContext(), mutableBitmap);
						if (image != null) {
							Canvas canvas = new Canvas(image);
							Paint paint = new Paint();
							paint.setColor(getResources().getColor(R.color.bg_foto_user_info));
							canvas.drawRect(new Rect(0, 0, image.getWidth(), image.getHeight()), paint);
							if (imgBgUser != null) {
								imgBgUser.setImageBitmap(image);
							}
						}

					}

				}
				
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					
				}
			});
		
		lstView.addHeaderView(headerView);
	}
	
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		createAdapter();
		/*if (isVisibleToUser) {
			showFragment();
		}*/
	}
	 
	private void callService() {

        RetrofitManager.getInstance().getUserService().getUserInfo(page, new Callback<UserInfoResult>() {
            @Override
            public void success(UserInfoResult userInfoResult, Response response) {
                isError = false;
                if (userInfoResult.isSuccess() && userInfoResult.getPosts() != null) {
                    populateList(userInfoResult.getPosts());
                    refreshUser(userInfoResult.getUser());
                    SharedPrefManager.getInstance().setUploadFoto(false);
                } else if(userInfoResult.isSuccess() && userInfoResult.getPosts() == null) {
                    getView().findViewById(R.id.txt_nenhum_post).setVisibility(View.VISIBLE);
                } else {
                    showError(null);
                }
                dismissWaitDialog();

            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
                dismissWaitDialog();
                lstView.onFinishLoading(false, new ArrayList<Object>());
            }
        });
        /*
		ObserverAsyncTask<UserInfoResult> observer = new ObserverAsyncTask<UserInfoResult>() {
			@Override
			public void onPreExecute() {
				//if (lstView != null && adapter != null && adapter.getCount() <= 0) {
				//	lstView.onFinishLoading(false, new ArrayList<Object>());
				//}
			}

			@Override
			public void onPostExecute(UserInfoResult result) {
				isError = false;
				if (result.isSuccess() && result.getPosts() != null) {
					populateList(result.getPosts());
					refreshUser(result.getUser());
					SharedPrefManager.getInstance().setUploadFoto(false);
				} else if(result.isSuccess() && result.getPosts() == null) {
					getView().findViewById(R.id.txt_nenhum_post).setVisibility(View.VISIBLE);
				} else {
					showError(null);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				dismissWaitDialog();
				lstView.onFinishLoading(false, new ArrayList<Object>());
			}
		};	
		
		TaErradoServiceBuilder sb = new TaErradoServiceBuilder(getContext());
		
		MultiValueMap<String, String> params = sb.getParams();
		params.add(UrlParameters.PAGE, String.valueOf(page));
		
		sb.setUrl(Paths.USER_INFO)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), UserInfoResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
		*/
	}
	
	private void refreshUser(User user) {
		if (user != null) {
			txtNrFotos.setText(String.valueOf(user.getPostsCount()));
			txtXingamentos.setText(String.valueOf(user.getCursesCount()));
			txtDias.setText(user.getDiasApp());
			headerView.findViewById(R.id.layout_infos_internal).setVisibility(View.VISIBLE);
		}
	}
	
	private void populateList(List<Post> lstPosts) {
		createAdapter();
		
		if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > lstPosts.size()) {
			lstView.onFinishLoading(false, lstPosts);
    	} else {
    		lstView.onFinishLoading(true, lstPosts);
    	}    	
		lstView.setVisibility(View.VISIBLE);
    	adapter.notifyDataSetChanged();
    	lstView.requestLayout();
    	
    	getView().findViewById(R.id.txt_nenhum_post).setVisibility(View.GONE);
    	
    	if (page == 1 && (lstPosts == null || lstPosts.size() <= 0)) {
    		getView().findViewById(R.id.txt_nenhum_post).setVisibility(View.VISIBLE);
    		//showMessage(R.string.msg_nenhum_post_encontrado);
    	}
	}
	
	private void createAdapter() {
		if (adapter == null) {
			lstView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					page++;
					callService();
				}
			});
			adapter = new PostAdapter(getContext(), new ArrayList<Post>(), SharedPrefManager.getInstance().getProfileImageUrl());
	        lstView.setAdapter(adapter);
		}
	}

}
