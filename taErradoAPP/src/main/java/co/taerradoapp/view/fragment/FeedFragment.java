package co.taerradoapp.view.fragment;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;

import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;

import co.taerradoapp.R;
import co.taerradoapp.model.GetPostsResult;
import co.taerradoapp.model.Post;
import co.taerradoapp.service.RetrofitManager;
import co.taerradoapp.service.TaErradoServiceBuilder;
import co.taerradoapp.util.Constants;
import co.taerradoapp.util.Paths;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.util.UrlParameters;
import co.taerradoapp.view.activity.base.TaErradoBaseFragment;
import co.taerradoapp.view.adapter.PostAdapter;

public class FeedFragment extends TaErradoBaseFragment {

	private PagingListView lstView;
	private PostAdapter adapter;
	private int page = 1;
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private TextView txtNewPosts;
	private static final int FADE_IN = 1200;
	private static final int FADE_OUT = 3000;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_feed, container, false);

		lstView = (PagingListView)rootView.findViewById(R.id.layout_content);
		txtNewPosts = (TextView)rootView.findViewById(R.id.txt_new_posts);
		mSwipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.activity_main_swipe_refresh_layout);
		txtNewPosts.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				lstView.smoothScrollToPosition(0);
				txtNewPosts.setVisibility(View.INVISIBLE);
			}
		});
		mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.loading));
		mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				callServiceLoadNew();
			}
		});
		
		lstView.setOnTouchListener(new OnTouchListener() {
			GestureDetector gestureDetector = new GestureDetector(new SimpleOnGestureListener() {
            	@Override
            	public boolean onDoubleTap(MotionEvent e) {
            		int position = lstView.pointToPosition(Float.valueOf(e.getX()).intValue(), Float.valueOf(e.getY()).intValue());
            		adapter.callXingamento(adapter.getItem(position));
            		return super.onDoubleTap(e);
            	}
            	
            	@Override
            	public boolean onSingleTapConfirmed(MotionEvent e) {
            		return super.onSingleTapConfirmed(e);
            	}
            });
			@Override
	         public boolean onTouch(View v, MotionEvent event) {
	                return gestureDetector.onTouchEvent(event);
	         }
		});
		
		return rootView;
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if (isVisibleToUser) {
			showFragment();
		}
	}
	
	private void showNewPosts() {
		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
		fadeIn.setDuration(FADE_IN);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
		fadeOut.setStartOffset(FADE_IN);
		fadeOut.setDuration(FADE_OUT);

		AnimationSet animation = new AnimationSet(false); //change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);
		txtNewPosts.setAnimation(animation);
	}
	
	@Override
	public void showFragment() {
		if (adapter == null || adapter.getCount() <= 0) {
			callService();
		}		
	}
	
	@Override
	public void hideFragment() {
	}
	
	@Override
	protected void refresh() {
		super.refresh();
		callService();
	}
	
	@Override
	public String getScreenName() {
		return "Feed";
	}
	
    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);

    }
    
	private void callService() {

		ObserverAsyncTask<GetPostsResult> observer = new ObserverAsyncTask<GetPostsResult>() {
			@Override
			public void onPreExecute() {
				if (page == 1) {
					showWaitDialog(true);
				}
			}

			@Override
			public void onPostExecute(GetPostsResult result) {
				isError = false;
				if (result.isSuccess() && result.getPosts() != null) {
					populateList(result.getPosts());						
				} else {
					showError(null);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				dismissWaitDialog();
			}
		};	
		
		TaErradoServiceBuilder sb = new TaErradoServiceBuilder(getContext());
		
		MultiValueMap<String, String> params = sb.getParams();
		params.add(UrlParameters.PAGE, String.valueOf(page));
		params.add(UrlParameters.USER_PLATFORM, Constants.PLATFORM);
		params.add(UrlParameters.USER_DEVICE_TOKEN, SharedPrefManager.getInstance().getDeviceToken());
		
		sb.setUrl(Paths.GET_POSTS_FEED)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), GetPostsResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void callServiceLoadNew() {
		ObserverAsyncTask<GetPostsResult> observer = new ObserverAsyncTask<GetPostsResult>() {
			@Override
			public void onPreExecute() {
			}

			@Override
			public void onPostExecute(GetPostsResult result) {
				isError = false;
				if (result.isSuccess() && result.getPosts() != null) {
					populateNewItems(result.getPosts());						
				}
				mSwipeRefreshLayout.setRefreshing(false);
			}

			@Override
			public void onCancelled() {
				mSwipeRefreshLayout.setRefreshing(false);
			}

			@Override
			public void onError(Exception e) {
				mSwipeRefreshLayout.setRefreshing(false);
			}
		};	
		
		TaErradoServiceBuilder sb = new TaErradoServiceBuilder(getContext());
		
		MultiValueMap<String, String> params = sb.getParams();
		params.add(UrlParameters.LAST_ID, String.valueOf(adapter.getItem(0).getId()));
		
		sb.setUrl(Paths.GET_POSTS_FEED_UPDATE)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		AsyncTaskService asyncNow = sb.mappingInto(getContext(), GetPostsResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			asyncNow.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			asyncNow.execute();
		}
	}
	
	private void populateNewItems(List<Post> lstPosts) {
		int qtdeAdd = 0;
		if (lstPosts != null && !lstPosts.isEmpty()) {
			for (Post post : lstPosts) {
				adapter.addFirstPosition(post);
				qtdeAdd++;
			}
			
			if (qtdeAdd > 0) {
				int index = lstView.getFirstVisiblePosition();
				View v = lstView.getChildAt(0);
				int top = (v == null) ? 0 : v.getTop();

				// notify dataset changed or re-assign adapter here
				adapter.notifyDataSetChanged();
				// restore the position of listview
				lstView.setSelectionFromTop(index + qtdeAdd, top);
				showNewPosts();
			}			
		}		
		
	}
	
	private void populateList(List<Post> lstPosts) {
		if (adapter == null) {
			lstView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					page++;
					callService();
				}
			});
			adapter = new PostAdapter(getContext(), new ArrayList<Post>());
	        lstView.setAdapter(adapter);
		}
		
		if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > lstPosts.size()) {
			lstView.onFinishLoading(false, lstPosts);
    	} else {
    		lstView.onFinishLoading(true, lstPosts);
    	}    	
		lstView.setVisibility(View.VISIBLE);
		mSwipeRefreshLayout.setVisibility(View.VISIBLE);
    	adapter.notifyDataSetChanged();
    	lstView.requestLayout();
    	
    	if (page == 1 && (lstPosts == null || lstPosts.size() <= 0)) {
    		showMessage(R.string.msg_nenhum_post_encontrado);
    	}
	}
	

}
