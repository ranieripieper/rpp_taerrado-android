package co.taerradoapp.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.view.custom.imageview.CircleImageView;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.util.HashMap;

import co.taerradoapp.R;
import co.taerradoapp.TaErradoApplication;
import co.taerradoapp.model.Post;

public class MarkerAdapter implements InfoWindowAdapter{
	private LayoutInflater mInflater;
	private HashMap<Marker, Post> items;
	private Context mContext;
	private View layoutMarker;
	
	public MarkerAdapter(LayoutInflater i, Context context, HashMap<Marker, Post> posts, View layoutMarker){
	    mInflater = i;
	    this.items = posts;
	    this.mContext = context;
	    this.layoutMarker = layoutMarker;
	}

	@Override
	public View getInfoContents(Marker marker) {		
	    return layoutMarker;
	}

	@Override
	public View getInfoWindow(final Marker marker) {
		final TextView txtRua = (TextView)layoutMarker.findViewById(R.id.txt_street);
		final TextView txtData = (TextView)layoutMarker.findViewById(R.id.txt_data);
		
	    final CircleImageView circularImage = (CircleImageView)layoutMarker.findViewById(R.id.img_photo);
	    circularImage.setImageResource(R.drawable.img_place_holder);
	    
	    Post item = items.get(marker);
	    if (item != null) {
			if (!TextUtils.isEmpty(item.getImagerUrl())) {
				File f = TaErradoApplication.imageLoader.getDiscCache().get(item.getImagerUrl());
				if (f != null && f.exists()) {
					circularImage.setImageBitmap(BitmapFactory.decodeFile(f.getAbsolutePath()));
				} else {
					if (f != null && !f.exists()) {
						TaErradoApplication.imageLoader.getDiscCache().remove(item.getImagerUrl());
					}
					TaErradoApplication.imageLoader.displayImage(item.getImagerUrl(), circularImage, new ImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {

						}

						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

						}

						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							if (marker.isInfoWindowShown()) {
								marker.showInfoWindow();
							}
						}

						@Override
						public void onLoadingCancelled(String imageUri, View view) {

						}
					});

				}
			}
			txtRua.setText(item.getStreet());
			txtData.setText(item.getCreatedAtStr());
	    }
	    
		return layoutMarker;
	}
}
