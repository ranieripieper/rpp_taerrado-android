package co.taerradoapp.view.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewHolderPost {
	
	public TextView txtRua;
	public TextView txtData;
	public TextView txtNrXingamentos;
	public TextView txtLabelXingamentos;
	public TextView txtDesc;
	public TextView txtRetry;
	
	public ImageView imgUser;
	public ImageView imgPhoto;
	public ImageView imgXingamento;
	public ImageView imgMore;
	
	public View layoutMenu;
	public View loadingImageView;
}
