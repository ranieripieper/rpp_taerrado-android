package co.taerradoapp.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import co.taerradoapp.R;
import co.taerradoapp.TaErradoApplication;
import co.taerradoapp.model.Post;
import co.taerradoapp.service.background.ReportPhotoBackgroundService;
import co.taerradoapp.service.background.XingamentoBackgroundService;
import co.taerradoapp.view.activity.base.TaErradoBaseActivity;
import co.taerradoapp.view.adapter.holder.ViewHolderPost;

public class PostAdapter extends PagingBaseAdapter<Post> {

    private LayoutInflater inflater;
    //private final String TAG_CURSE = "TAG_CURSE";
    //private final String TAG_UNCURSE = "TAG_UNCURSE";
    private Context mContext;
    private String mUserImageUrl;

    public PostAdapter(Context context, List<Post> posts) {
        this.mContext = context;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (posts == null) {
            this.items = new ArrayList<Post>();
        } else {
            this.items = posts;
        }
    }

    public PostAdapter(Context context, List<Post> posts, String userImageUrl) {
        this.mContext = context;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (posts == null) {
            this.items = new ArrayList<Post>();
        } else {
            this.items = posts;
        }
        this.mUserImageUrl = userImageUrl;
    }

    @Override
    public int getCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public Post getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getViewRow(position, convertView, parent);
    }

    private View getViewRow(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        final ViewHolderPost holder;

        if (vi == null) {
            vi = inflater.inflate(R.layout.post_row, parent, false);
            holder = new ViewHolderPost();
            holder.txtRua = (TextView) vi.findViewById(R.id.txt_street);
            holder.txtData = (TextView) vi.findViewById(R.id.txt_data);
            holder.imgUser = (ImageView) vi.findViewById(R.id.img_user);
            holder.imgPhoto = (ImageView) vi.findViewById(R.id.img_photo);
            holder.txtRetry = (TextView) vi.findViewById(R.id.txt_retry);
            holder.imgXingamento = (ImageView) vi.findViewById(R.id.img_xingamentos);
            holder.txtNrXingamentos = (TextView) vi.findViewById(R.id.txt_xingamentos);
            holder.txtLabelXingamentos = (TextView) vi.findViewById(R.id.txt_label_xingamentos);
            holder.txtDesc = (TextView) vi.findViewById(R.id.txt_desc);
            holder.imgMore = (ImageView) vi.findViewById(R.id.img_more);
            holder.loadingImageView = vi.findViewById(R.id.img_loading);
            final Animation slideUp = new TranslateAnimation(0, 0, 0, -20);
            slideUp.setDuration(1);
            holder.layoutMenu = vi.findViewById(R.id.layout_menu_post);
            holder.imgXingamento.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
            ((RelativeLayout.LayoutParams) holder.txtNrXingamentos.getLayoutParams()).leftMargin = holder.imgXingamento.getMeasuredWidth();
            vi.setTag(holder);
        } else {
            holder = (ViewHolderPost) vi.getTag();
        }

        final Post post = getItem(position);

        holder.txtRua.setText(post.getAddress());
        holder.txtData.setText(post.getCreatedAtStr());
        holder.txtNrXingamentos.setText(String.valueOf(post.getCursesCount()));
        if (post.getCursesCount() == 0 || post.getCursesCount() > 1) {
            holder.txtLabelXingamentos.setText(R.string.xingamentos);
        } else {
            holder.txtLabelXingamentos.setText(R.string.xingamento);
        }
        holder.txtDesc.setText(post.getDescription());

        //verifica xingamento
        if (post.isUserCurse()) {
            holder.imgXingamento.setImageResource(R.drawable.icn_xingar_on);
            //holder.imgXingamento.setTag(TAG_UNCURSE);
        } else {
            holder.imgXingamento.setImageResource(R.drawable.icn_xingar_off);
            //holder.imgXingamento.setTag(TAG_CURSE);
        }
        holder.imgXingamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callXingamento(post);
            }
        });
        holder.txtRetry.setVisibility(View.GONE);
        holder.txtRetry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                loadPhoto(holder, post);
            }
        });
        loadPhoto(holder, post);

        String imgUserUrl = post.getImgUser();
        if (TextUtils.isEmpty(imgUserUrl) && !TextUtils.isEmpty(mUserImageUrl)) {
            imgUserUrl = mUserImageUrl;
        }

        TaErradoApplication.imageLoaderUser.displayImage(imgUserUrl, holder.imgUser, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view,
                                        FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.imgUser.bringToFront();
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });

        holder.imgMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TaErradoBaseActivity.showDefaultDialog(mContext, mContext.getString(R.string.nao), mContext.getString(R.string.sim), mContext.getString(R.string.impropria), mContext.getString(R.string.msg_pergunta_foto_impropria), null, new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (BaseService.internetConnection(mContext)) {
                            ReportPhotoBackgroundService.callService(mContext, post.getId());
                            TaErradoBaseActivity.showDefaultDialog(mContext, mContext.getString(R.string.ok), mContext.getString(R.string.impropria), mContext.getString(R.string.msg_obrigado_report), null);
                        } else {
                            TaErradoBaseActivity.showDefaultDialog(mContext, R.string.fechar, R.string.ops, R.string.msg_sem_conexao, null);
                        }
                    }
                });
            }
        });
        return vi;
    }

    private void loadPhoto(final ViewHolderPost holder, final Post post) {
        holder.txtRetry.setVisibility(View.GONE);
        //holder.imgPhoto.setImageResource(R.drawable.img_place_holder);
        TaErradoApplication.imageLoader.displayImage(post.getImagerUrl(), holder.imgPhoto, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.imgPhoto.setVisibility(View.INVISIBLE);
                holder.loadingImageView.setVisibility(View.VISIBLE);
                holder.loadingImageView.bringToFront();
            }

            @Override
            public void onLoadingFailed(String imageUri, View view,
                                        FailReason failReason) {
                holder.loadingImageView.setVisibility(View.GONE);
                holder.imgPhoto.setImageResource(R.drawable.img_place_holder);
                holder.imgPhoto.setVisibility(View.VISIBLE);
                holder.txtRetry.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.loadingImageView.setVisibility(View.GONE);
                holder.imgPhoto.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
    }

    public void callXingamento(Post post) {
        if (BaseService.internetConnection(mContext)) {
            if (!post.isUserCurse()) {
                post.setCursesCount(post.getCursesCount() + 1);
                post.setUserCurse(true);
            } else {
                post.setCursesCount(post.getCursesCount() - 1);
                post.setUserCurse(false);
            }
            XingamentoBackgroundService.callService(mContext, post.getId(), post.isUserCurse());
            notifyDataSetChanged();
        } else {
            TaErradoBaseActivity.showDefaultDialog(mContext, R.string.fechar, R.string.ops, R.string.msg_sem_conexao, null);
        }

    }

	/*public void callXingamento(Post post, ImageView imgXingamento) {
		if (BaseService.internetConnection(mContext)) {
			if (!post.isUserCurse()) {
				imgXingamento.setImageResource(R.drawable.icn_xingar_on);
				imgXingamento.setTag(TAG_UNCURSE);
				post.setCursesCount(post.getCursesCount()+1);
				post.setUserCurse(true);
			} else {
				imgXingamento.setImageResource(R.drawable.icn_xingar_off);
				imgXingamento.setTag(TAG_CURSE);
				post.setCursesCount(post.getCursesCount()-1);
				post.setUserCurse(false);
			}
			XingamentoBackgroundService.callService(mContext, post.getId(), post.isUserCurse());
			notifyDataSetChanged();
		} else {
			TaErradoBaseActivity.showDefaultDialog(mContext, R.string.fechar, R.string.ops, R.string.msg_sem_conexao, null);
		}
		
	}*/
}