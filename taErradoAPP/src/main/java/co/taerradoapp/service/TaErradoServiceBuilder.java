package co.taerradoapp.service;

import android.content.Context;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;

import co.taerradoapp.util.Constants;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.util.UrlParameters;

public class TaErradoServiceBuilder extends ServiceBuilder {

	public TaErradoServiceBuilder() {
		super();
		this.setCharset(Constants.CHARSET);
	}
	
	public TaErradoServiceBuilder(Context ctx) {
		this();
		this.addParams(UrlParameters.AUTH_TOKEN, SharedPrefManager.getInstance().getAuthToken());
		//this.addHeaders(UrlParameters.AUTH_TOKEN, SharedPrefManager.getAuthToken(ctx));
	}
	
}
