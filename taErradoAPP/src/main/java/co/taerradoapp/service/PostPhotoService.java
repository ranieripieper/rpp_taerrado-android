package co.taerradoapp.service;

import android.content.Context;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;

import org.springframework.http.HttpStatus;

import java.io.File;
import java.net.ConnectException;

import co.taerradoapp.model.PostResult;
import co.taerradoapp.service.interfaces.PostService;
import co.taerradoapp.util.Constants;
import co.taerradoapp.util.Paths;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.util.UrlParameters;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedFile;

public class PostPhotoService extends BaseService {

	public PostPhotoService(ServiceBuilder builder) {
		super(builder);
	}
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException, DdsUtilIOException {
		
		String desc = params.get(UrlParameters.PARAM_POST_DESCRIPTION).get(0).toString();
		String lat = params.get(UrlParameters.PARAM_POST_LAT).get(0).toString();
		String lng = params.get(UrlParameters.PARAM_POST_LNG).get(0).toString();
		String street = params.get(UrlParameters.PARAM_POST_STREET).get(0).toString();
		String city = params.get(UrlParameters.PARAM_POST_CITY).get(0).toString();
		String state = params.get(UrlParameters.PARAM_POST_STATE).get(0).toString();
		String filePath = params.get(UrlParameters.PARAM_POST_IMAGE).get(0).toString();

		RequestInterceptor requestInterceptor = new RequestInterceptor() {
			@Override
			public void intercept(RequestFacade request) {
				request.addHeader(UrlParameters.AUTH_TOKEN, SharedPrefManager.getInstance().getAuthToken());
			}
		};

		RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint(Paths.HOST)
				.setErrorHandler(new MyErrorHandler())
				.setConverter(new GsonConverter(Util.getGson()))
				.setLogLevel(Constants.DEBUG ? RestAdapter.LogLevel.FULL :  RestAdapter.LogLevel.NONE)
				.setRequestInterceptor(requestInterceptor).build();

		PostResult result = null;
		
		PostService service = restAdapter.create(PostService.class);

		result = service.postPhoto(desc, lat, lng, street, city, state, new TypedFile("image/jpeg", new File(filePath)));
		
		return result;
	}

	class MyErrorHandler implements ErrorHandler {
		@Override
		public Throwable handleError(RetrofitError cause) {
			Response r = cause.getResponse();
			if (r != null && r.getStatus() == 401) {
				return new DdsUtilIOException(cause.getMessage(),
						HttpStatus.valueOf(r.getStatus()));
			}
			return cause;
		}
	}
}
