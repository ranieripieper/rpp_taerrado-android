package co.taerradoapp.service.exception;

/**
 * Created by ranipieper on 8/19/15.
 */
public class ServiceException extends java.io.IOException {

    private static final long serialVersionUID = 1L;
    private String msgErro;
    private Integer httpStatus;

    public ServiceException() {

    }

    public ServiceException(String m) {
        super();
        this.msgErro = m;
    }

    public ServiceException(Exception e) {
        super(e);
    }

    public ServiceException(Exception e, String msg, Integer httpStatus) {
        super(e);
        this.msgErro = msg;
        this.httpStatus = httpStatus;
    }

    public ServiceException(String msg, Integer httpStatus) {
        this.msgErro = msg;
        this.httpStatus = httpStatus;
    }

    public String getMsgErro() {
        return msgErro;
    }

    public void setMsgErro(String msgErro) {
        this.msgErro = msgErro;
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }


}