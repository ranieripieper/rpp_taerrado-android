package co.taerradoapp.service.interfaces;

import co.taerradoapp.model.BaseResult;
import co.taerradoapp.model.LoginResult;
import co.taerradoapp.model.UserInfoResult;
import co.taerradoapp.util.UrlParameters;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by ranipieper on 11/3/15.
 */
public interface UserService {


    @POST("/users")
    @Multipart
    void signup(
            @Part("user[email]") String email,
            @Part("user[name]") String name,
            @Part("user[surname]") String surname,
            @Part("user[password]") String password,
            @Part("user[password_confirmation]") String passwordConf,
            @Part("user[platform]") String platform,
            @Part("user[device_token]") String deviceToken,
            @Part("user[profile_image]") TypedFile photo,
            retrofit.Callback<LoginResult> callback);

    @POST("/users/password_reset")
    @FormUrlEncoded
    void reset_password(
            @Field("user[email]") String email,
            retrofit.Callback<BaseResult> callback);

    @POST("/users/auth")
    @FormUrlEncoded
    void login(
            @Field("user[email]") String email,
            @Field("user[password]") String password,
            @Field("user[platform]") String platform,
            @Field("user[device_token]") String deviceToken,
            retrofit.Callback<LoginResult> callback);

    @GET("/users/posts")
    void getUserInfo(@Query(UrlParameters.PAGE) Integer page, Callback<UserInfoResult> callback);
}
