package co.taerradoapp.service.interfaces;

import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;

import co.taerradoapp.model.BaseResult;
import co.taerradoapp.model.PostResult;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;

public interface PostService {
	

	@POST("/posts")
	@Multipart
	PostResult postPhoto(
			@Part("post[description]") String desc,
			@Part("post[lat]") String lat,
			@Part("post[lng]") String lng,
			@Part("post[street]") String street,
			@Part("post[city]") String city,
			@Part("post[state]") String state,
			@Part("post[image]") TypedFile photo) throws DdsUtilIOException;

    @POST("/posts/{post_id}/report")
    BaseResult reportPost(@Path("post_id") Integer postId);

    @POST("/posts/{post_id}/curse")
    BaseResult cursePost(@Path("post_id") Integer postId);

    @POST("/posts/{post_id}/uncurse")
    BaseResult uncursePost(@Path("post_id") Integer postId);


}
