package co.taerradoapp.service.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import co.taerradoapp.model.BaseResult;
import co.taerradoapp.service.RetrofitManager;

public class XingamentoBackgroundService extends IntentService {
	
	public static String TAG = "XingamentoBackgroundService";
	public static String PARAM_POST_ID = "PARAM_POST_ID";
	public static String PARAM_CURSE = "PARAM_CURSE";
	
	public XingamentoBackgroundService() {
		super("LikeBackgroundService");
	}

	@Override
	protected void onHandleIntent(final Intent intent) {

		synchronized (TAG) {
			final Integer postId = intent.getExtras().getInt(PARAM_POST_ID);
			final boolean curse = intent.getExtras().getBoolean(PARAM_CURSE);

			new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						BaseResult result = null;
						if (curse) {
                            result = RetrofitManager.getInstance().getPostService().cursePost(postId);
						} else {
                            result = RetrofitManager.getInstance().getPostService().uncursePost(postId);
						}
                        Log.e(TAG, "Post " + postId + " xingado? " + curse);
					} catch (Exception e) {
                        Log.e(TAG, "Erro ao reportar post " + e.getMessage());
                        e.printStackTrace();
					}
				}
			}).start();
		}
	}
	
	public static void callService(Context context, Integer postId, boolean curse) {
		Intent intentService = new Intent(context, XingamentoBackgroundService.class);
		intentService.putExtra(PARAM_POST_ID, postId);
		intentService.putExtra(PARAM_CURSE, curse);
		context.startService(intentService);
	}

}