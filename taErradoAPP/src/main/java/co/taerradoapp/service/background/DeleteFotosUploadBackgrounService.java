package co.taerradoapp.service.background;

import java.io.File;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

public class DeleteFotosUploadBackgrounService extends IntentService {
	
	public static String TAG = "DeleteFotosUploadBackgrounService";
	
	public DeleteFotosUploadBackgrounService() {
		super("DeleteFotosUploadBackgrounService");
	}

	@Override
	protected void onHandleIntent(final Intent intent) {

		synchronized (TAG) {
			File mediaStorageDir = new File(getFilesDir().getAbsolutePath(), "taerrado");
			if (mediaStorageDir.exists() && mediaStorageDir.isDirectory()){
				for (File f : mediaStorageDir.listFiles()) {
					f.delete();
				}
			}
		}
	}
	
	public static void callService(Context context) {
		Intent intentService = new Intent(context, DeleteFotosUploadBackgrounService.class);
		context.startService(intentService);
	}

}