package co.taerradoapp.service.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import co.taerradoapp.model.BaseResult;
import co.taerradoapp.service.RetrofitManager;

public class ReportPhotoBackgroundService extends IntentService {
	
	public static String TAG = "ReportPhotoBackgroundService";
	public static String PARAM_POST_ID = "PARAM_POST_ID";
	
	public ReportPhotoBackgroundService() {
		super("ReportPhotoBackgroundService");
	}

	@Override
	protected void onHandleIntent(final Intent intent) {

		synchronized (TAG) {
			final Integer postId = intent.getExtras().getInt(PARAM_POST_ID);

			try {
                BaseResult result = RetrofitManager.getInstance().getPostService().reportPost(postId);
                Log.d(TAG, "Post reportado: " + postId);
            } catch (Exception e) {
                Log.e(TAG, "Erro ao reportar post " + e.getMessage());
                e.printStackTrace();
			}
		}
	}
	
	public static void callService(Context context, Integer postId) {
		Intent intentService = new Intent(context, ReportPhotoBackgroundService.class);
		intentService.putExtra(PARAM_POST_ID, postId);
		context.startService(intentService);
	}

}