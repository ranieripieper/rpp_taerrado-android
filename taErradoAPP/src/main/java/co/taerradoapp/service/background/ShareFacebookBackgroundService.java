package co.taerradoapp.service.background;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;

import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;

import java.io.File;

public class ShareFacebookBackgroundService extends IntentService {
	
	public static String TAG = "ShareFacebookBackgroundService";
	public static String PARAM_DESCRIPTION = "PARAM_DESCRIPTION";
    public static String PARAM_ADDRESS = "PARAM_ADDRESS";
	public static String PARAM_FILE_PATH = "PARAM_FILE_PATH";
	
	private Handler mMainThreadHandler = null;
	
	public ShareFacebookBackgroundService() {
		super("ShareFacebookBackgroundService");
		mMainThreadHandler = new Handler();
	}

	@Override
	protected void onHandleIntent(final Intent intent) {

		synchronized (TAG) {
			final String filePath = intent.getExtras().getString(PARAM_FILE_PATH);
			final String description = intent.getExtras().getString(PARAM_DESCRIPTION);
            final String address = intent.getExtras().getString(PARAM_ADDRESS);

			try {
				Bitmap image = BitmapUtil.media_getBitmapFromFile(new File(filePath), ScreenUtil.getDisplayWidth(this));
                Bitmap imageToShare = BitmapUtil.drawTextToBitmap(this, image, address);

				if (image != null) {
					SharePhoto photo = new SharePhoto.Builder()
							.setBitmap(imageToShare).setCaption(description + "\r\n Via http://taerradoapp.co").build();
					final SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(
							photo).build();
					mMainThreadHandler.post(new Runnable() {
			            @Override
			            public void run() {
			            	ShareApi.share(content, new FacebookCallback() {
								@Override
								public void onCancel() {
								}

								@Override
								public void onError(FacebookException error) {
								}

								@Override
								public void onSuccess(Object result) {
								}
							});
			            }
			        });
			    }				
			} catch (Exception e) {
			}
			
		}
	}
	
}
