package co.taerradoapp.util;

public class UrlParameters {

	public static final String AUTH_TOKEN = "X-Token";
	
	public static final String PAGE = "page";
	public static final String LAST_ID = "last_id";
	public static final String LAT = "lat";
	public static final String LNG = "lng";
	public static final String DATE = "date";
	public static final String USER_EMAIL = "user[email]";
	public static final String USER_FB_ID = "user[fb_id]";
	public static final String USER_FB_TOKEN = "user[fb_token]";
	public static final String USER_GENDER = "user[gender]";
	public static final String USER_NAME = "user[name]";
	public static final String USER_SURNAME = "user[surname]";
	public static final String USER_PLATFORM = "user[platform]";
	public static final String USER_DEVICE_TOKEN = "user[device_token]";


	
	public static final String PARAM_POST_ID_URL = "{post_id}";	
	public static final String PARAM_POST_DESCRIPTION = "post[description]";
	public static final String PARAM_POST_STREET = "post[street]";
	public static final String PARAM_POST_STATE = "post[state]";
	public static final String PARAM_POST_CITY = "post[city]";
	public static final String PARAM_POST_LAT = "post[lat]";
	public static final String PARAM_POST_LNG = "post[lng]";
	public static final String PARAM_POST_IMAGE = "post[image]";
	public static final String PARAM_POST_IMAGE_CONTENT_TYPE = "image_content_type";
	
	
	public static final String LAT_URL = "{lat}";
	public static final String LNG_URL = "{lng}";
	public static final String GEOCODING_KEY = "lng";
}
