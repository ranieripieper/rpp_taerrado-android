package co.taerradoapp.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.Date;

public class SharedPrefManager {

    private static SharedPrefManager sharedPrefManager;
    private Context mContext;

	private static final String PREFERENCES = SharedPrefManager.class + "";
	private static final String PREF_LAST_CLEAR_CACHE = "PREF_LAST_CLEAR_CACHE";
	private static final String PREF_FACEBOOK_TOKEN = "PREF_FACEBOOK_TOKEN";
	private static final String PREF_AUTH_TOKEN = "PREF_AUTH_TOKEN";
	private static final String PREF_FACEBOOK_ID = "PREF_FACEBOOK_ID";
	private static final String PREF_PROFILE_IMAGE_URL = "PREF_PROFILE_IMAGE_URL";
	private static final String PREF_TERMOS_ACEITO = "PREF_TERMOS_ACEITO";
	private static final String PREF_UPLOAD_FOTO = "PREF_UPLOAD_FOTO";
	private static final String PREF_DEVICE_TOKEN = "PREF_DEVICE_TOKEN";

    private SharedPrefManager(Context context) {
        this.mContext = context;
    }

    public static SharedPrefManager getInstance() {
        return sharedPrefManager;
    }

    public static void init(Application application) {
        sharedPrefManager = new SharedPrefManager(application);
    }

	public Date getLastClearCache() {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		long time = settings.getLong(PREF_LAST_CLEAR_CACHE, -1);
		if (time > 0) {
			return new Date(time);
		}
		return null;
	}
	
	public void setLastClearCache(Date value) {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(PREF_LAST_CLEAR_CACHE, value.getTime());
		editor.commit();
	}
	
	public boolean isUploadFoto() {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		return settings.getBoolean(PREF_UPLOAD_FOTO, false);
	}
	
	public void setUploadFoto(boolean value) {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PREF_UPLOAD_FOTO, value);
		editor.commit();
	}
	
	public boolean isTermosAceito() {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		return settings.getBoolean(PREF_TERMOS_ACEITO, false);
	}
	
	public void setTermosAceito(boolean value) {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PREF_TERMOS_ACEITO, value);
		editor.commit();
	}
	
	public String getProfileImageUrl() {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		String v = settings.getString(PREF_PROFILE_IMAGE_URL, "");
		return v;
	}
	
	public void setProfileImageUrl(String value) {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_PROFILE_IMAGE_URL, value);
		editor.commit();
	}
	
	public String getAuthToken() {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		String v = settings.getString(PREF_AUTH_TOKEN, "");
		return v;
	}
	
	public void setAuthToken(String value) {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_AUTH_TOKEN, value);
		editor.commit();
	}
	
	public String getFacebookToken() {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		String v = settings.getString(PREF_FACEBOOK_TOKEN, "");
		return v;
	}
	
	public void setFacebookToken(String value) {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_FACEBOOK_TOKEN, value);
		editor.commit();
	}
	
	public String getFacebookId() {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		String v = settings.getString(PREF_FACEBOOK_ID, "");
		return v;
	}
	
	public void setFacebookId(String value) {
		SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_FACEBOOK_ID, value);
		editor.commit();
	}

    public String getDeviceToken() {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
        String v = settings.getString(PREF_DEVICE_TOKEN, "");
        return v;
    }

    public void setDeviceToken(String value) {
        SharedPreferences settings = mContext.getSharedPreferences(PREFERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREF_DEVICE_TOKEN, value == null ? "" : value);
        editor.commit();
    }

	public boolean userIsLogado() {
		if (TextUtils.isEmpty(getAuthToken())) {
			return false;
		}
		return true;
	}
	
	public void logout() {
		setAuthToken(null);
		setFacebookId(null);
		setFacebookToken(null);
		setProfileImageUrl(null);
		setTermosAceito(false);
	}

}
