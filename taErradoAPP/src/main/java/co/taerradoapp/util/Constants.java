package co.taerradoapp.util;

import co.taerradoapp.BuildConfig;

public class Constants {

    public static final boolean DEBUG = BuildConfig.DEBUG;
    public static final String CHARSET = "UTF-8";

    public static final String PLATFORM = "android";
    public static final String FACEBOOK_PROFILE_URL = "http://graph.facebook.com/%s/picture?type=large";
    public static final int TOTAL_ITENS_PAGE = 30;
    public static final float LOGO_SCALE_SIZE = 0.7f;

    public static final String URL_PALY_STORE = "https://play.google.com/store/apps/details?id=co.taerradoapp";
}
