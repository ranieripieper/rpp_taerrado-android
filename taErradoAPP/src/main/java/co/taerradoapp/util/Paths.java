package co.taerradoapp.util;

import co.taerradoapp.BuildConfig;

public class Paths {

    public static final String HOST = BuildConfig.BASE_API_PATH;
    //public static final String HOST = "http://192.168.0.11:3000/api/v1";

    public static final String LOGIN = String.format("%s/users", HOST);

    public static final String GET_POSTS_MAP = String.format(
            "%s/posts?%s={%s}&%s={%s}&%s={%s}", HOST,
            UrlParameters.DATE, UrlParameters.DATE,
            UrlParameters.LAT, UrlParameters.LAT,
            UrlParameters.LNG, UrlParameters.LNG);

    public static final String GET_POSTS_FEED = String.format(
            "%s/posts?%s={%s}&%s={%s}&%s={%s}&%s={%s}", HOST,
            UrlParameters.PAGE, UrlParameters.PAGE,
            UrlParameters.AUTH_TOKEN, UrlParameters.AUTH_TOKEN,
            UrlParameters.USER_DEVICE_TOKEN, UrlParameters.USER_DEVICE_TOKEN,
            UrlParameters.USER_PLATFORM, UrlParameters.USER_PLATFORM);

    public static final String GET_POSTS_FEED_UPDATE = String.format(
            "%s/posts/new?%s={%s}&%s={%s}", HOST,
            UrlParameters.LAST_ID, UrlParameters.LAST_ID, UrlParameters.AUTH_TOKEN, UrlParameters.AUTH_TOKEN);

    //public static final String USER_INFO = String.format(
    //        "%s/users/posts?%s={%s}&%s={%s}", HOST,
     //       UrlParameters.PAGE, UrlParameters.PAGE, UrlParameters.AUTH_TOKEN, UrlParameters.AUTH_TOKEN);

    public static final String POST_PHOTO = String.format("%s/posts", HOST);

    public static final String CURSE = String.format("%s/posts/%s/curse", HOST, UrlParameters.PARAM_POST_ID_URL);
    public static final String UNCURSE = String.format("%s/posts/%s/uncurse", HOST, UrlParameters.PARAM_POST_ID_URL);

    public static final String REPORT = String.format("%s/posts/%s/report", HOST, UrlParameters.PARAM_POST_ID_URL);

    public static final String GOOGLE_GEOCODING = String.format("https://maps.googleapis.com/maps/api/geocode/json?result_type=street_address&latlng=%s,%s&key=%s", UrlParameters.LAT_URL, UrlParameters.LNG_URL, BuildConfig.GOOGLE_API_KEY);

}
