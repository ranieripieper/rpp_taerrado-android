package co.taerradoapp.model;

public class GoogleGeocodingResult {

	private GoogleGeocodingResults[] results;

	/**
	 * @return the results
	 */
	public GoogleGeocodingResults[] getResults() {
		return results;
	}

	/**
	 * @param results the results to set
	 */
	public void setResults(GoogleGeocodingResults[] results) {
		this.results = results;
	}
	
	
}
