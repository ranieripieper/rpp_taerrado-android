package co.taerradoapp.model;

import com.google.gson.annotations.SerializedName;

public class LoginResult extends BaseResult {

	@SerializedName("user_data")
	private User user;

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
