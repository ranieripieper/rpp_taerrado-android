package co.taerradoapp.model;

import com.google.gson.annotations.SerializedName;


public class GoogleGeocodingResults {

	@SerializedName("address_components")
	private AddressComponents[] addressComponents;

	/**
	 * @return the addressComponents
	 */
	public AddressComponents[] getAddressComponents() {
		return addressComponents;
	}

	/**
	 * @param addressComponents the addressComponents to set
	 */
	public void setAddressComponents(AddressComponents[] addressComponents) {
		this.addressComponents = addressComponents;
	}
	
	
	public String getCity() {
		return getShortName(AddressComponents.CITY);
	}
	
	public String getState() {
		return getShortName(AddressComponents.STATE);
	}
	
	public String getStreet() {
		return getShortName(AddressComponents.STREET);
	}
	
	public String getLongName(String field) {
		if (this.addressComponents != null) {
			for (AddressComponents address : this.addressComponents) {
				if (address.getTypes().contains(field)) {
					return address.getLongName();
				}
			}
		}
		return null;
	}
	
	public String getShortName(String field) {
		if (this.addressComponents != null) {
			for (AddressComponents address : this.addressComponents) {
				if (address.getTypes().contains(field)) {
					return address.getShortName();
				}
			}
		}
		return null;
	}
	
}
