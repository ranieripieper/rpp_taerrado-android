package co.taerradoapp.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class AddressComponents {

	public static final String STREET = "route";
	public static final String CITY = "locality";
	public static final String STATE = "administrative_area_level_1";
	
	@SerializedName("long_name")
	private String longName;
	@SerializedName("short_name")
	private String shortName;
	private List<String> types;
	
	/**
	 * @return the longName
	 */
	public String getLongName() {
		return longName;
	}
	/**
	 * @param longName the longName to set
	 */
	public void setLongName(String longName) {
		this.longName = longName;
	}
	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}
	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	/**
	 * @return the types
	 */
	public List<String> getTypes() {
		return types;
	}
	/**
	 * @param types the types to set
	 */
	public void setTypes(List<String> types) {
		this.types = types;
	}
}
