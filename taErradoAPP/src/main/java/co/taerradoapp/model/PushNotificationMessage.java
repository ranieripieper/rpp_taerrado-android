package co.taerradoapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PushNotificationMessage implements Parcelable {

	@SerializedName("alert")
	private String message;

	@SerializedName("post_id")
	private String postId;

	@SerializedName("post_image")
	private String postImage;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getPostImage() {
		return postImage;
	}

	public void setPostImage(String postImage) {
		this.postImage = postImage;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.message);
		dest.writeString(this.postId);
		dest.writeString(this.postImage);
	}

	public PushNotificationMessage() {
	}

	private PushNotificationMessage(Parcel in) {
		this.message = in.readString();
		this.postId = in.readString();
		this.postImage = in.readString();
	}

	public static final Creator<PushNotificationMessage> CREATOR = new Creator<PushNotificationMessage>() {
		public PushNotificationMessage createFromParcel(Parcel source) {
			return new PushNotificationMessage(source);
		}

		public PushNotificationMessage[] newArray(int size) {
			return new PushNotificationMessage[size];
		}
	};
}
