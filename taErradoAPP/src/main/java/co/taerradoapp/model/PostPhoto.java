package co.taerradoapp.model;

import android.os.Parcelable;
import android.os.Parcel;

public class PostPhoto implements Parcelable {

	private double latitude;
	private double longitude;
	private String rua;
	private String estado;
	private String cidade;
	private String filePath;
	private String filePathPreCrop;
	
	public static Parcelable.Creator<PostPhoto> CREATOR = new Parcelable.Creator<PostPhoto>(){
		public PostPhoto createFromParcel(Parcel source) {
			return new PostPhoto(source);
		}
		public PostPhoto[] newArray(int size) {
			return new PostPhoto[size];
		}
	};
	
	public PostPhoto() {
		
	}
	
	public PostPhoto(double latitude, double longitude, String rua,
			String estado, String cidade, String filePath, String filePathPreCrop) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.rua = rua;
		this.estado = estado;
		this.cidade = cidade;
		this.filePath = filePath;
		this.filePathPreCrop = filePathPreCrop;
	}

	public String getAddress() {
		return String.format("%s - %s/%s", this.rua, this.cidade, this.estado);
	}
	
	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	/**
	 * @return the rua
	 */
	public String getRua() {
		return rua;
	}
	/**
	 * @param rua the rua to set
	 */
	public void setRua(String rua) {
		this.rua = rua;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}
	/**
	 * @param cidade the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}
	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
	/**
	 * @return the filePathPreCrop
	 */
	public String getFilePathPreCrop() {
		return filePathPreCrop;
	}

	/**
	 * @param filePathPreCrop the filePathPreCrop to set
	 */
	public void setFilePathPreCrop(String filePathPreCrop) {
		this.filePathPreCrop = filePathPreCrop;
	}

	private PostPhoto(Parcel in) {
		this.latitude = in.readDouble();
		this.longitude = in.readDouble();
		this.rua = in.readString();
		this.estado = in.readString();
		this.cidade = in.readString();
		this.filePath = in.readString();
		this.filePathPreCrop = in.readString();
	}
	public int describeContents() { 
		return 0; 
	}
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeDouble(this.latitude);
		dest.writeDouble(this.longitude);
		dest.writeString(this.rua);
		dest.writeString(this.estado);
		dest.writeString(this.cidade);
		dest.writeString(this.filePath);
		dest.writeString(this.filePathPreCrop);
	}
	
	
	
}