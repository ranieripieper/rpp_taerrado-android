package co.taerradoapp.model;

import android.text.TextUtils;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Post {

    private Integer id;
    private String description;
    private double lat;
    private double lng;
    @SerializedName("created_at")
    private Date createdAt;
    @SerializedName("updateda_t")
    private Date updatedAt;
    @SerializedName("curses_count")
    private long cursesCount;
    private String state;
    private String city;
    private String street;
    @SerializedName("user_curse")
    private boolean userCurse;
    private User user;
    @SerializedName("image_url")
    private String imagerUrl;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * @return the lng
     */
    public double getLng() {
        return lng;
    }

    /**
     * @param lng the lng to set
     */
    public void setLng(double lng) {
        this.lng = lng;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    public String getCreatedAtStr() {
        return DateUtil.dfDiaMesAnoHoraMinuto.get().format(this.createdAt);
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the cursesCount
     */
    public long getCursesCount() {
        return cursesCount;
    }

    /**
     * @param cursesCount the cursesCount to set
     */
    public void setCursesCount(long cursesCount) {
        this.cursesCount = cursesCount;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        if (TextUtils.isEmpty(street)) {
            return "";
        }
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the userCurse
     */
    public boolean isUserCurse() {
        return userCurse;
    }

    /**
     * @param userCurse the userCurse to set
     */
    public void setUserCurse(boolean userCurse) {
        this.userCurse = userCurse;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the imagerUrl
     */
    public String getImagerUrl() {
        return imagerUrl;
    }

    /**
     * @param imagerUrl the imagerUrl to set
     */
    public void setImagerUrl(String imagerUrl) {
        this.imagerUrl = imagerUrl;
    }

    public String getAddress() {
        String result = "";
        if (!TextUtils.isEmpty(street)) {
            result = street;
        }
        if (!TextUtils.isEmpty(city)) {
            if (!TextUtils.isEmpty(result)) {
                result += ", ";
            }
            result += city;
        }
        if (!TextUtils.isEmpty(state)) {
            if (!TextUtils.isEmpty(result)) {
                result += " - ";
            }
            result += state;
        }
        return result;
    }


    public String getImgUser() {
        if (this.getUser() != null) {
            return this.getUser().getProfileImageUrl();
        }
        return "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Post post = (Post) o;

        if (id != null ? !id.equals(post.id) : post.id != null) {
            return false;
        }
        return !(description != null ? !description.equals(post.description) : post.description != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
