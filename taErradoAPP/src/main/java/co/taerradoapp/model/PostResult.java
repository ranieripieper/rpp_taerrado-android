package co.taerradoapp.model;

public class PostResult extends BaseResult {

	private Post post;

	/**
	 * @return the post
	 */
	public Post getPost() {
		return post;
	}

	/**
	 * @param post the post to set
	 */
	public void setPost(Post post) {
		this.post = post;
	}
	
	
}
