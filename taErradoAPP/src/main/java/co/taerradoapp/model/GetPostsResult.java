package co.taerradoapp.model;

import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class GetPostsResult extends BaseResult {

    @SerializedName("post")
    private List<Post> posts;
    @SerializedName("count_posts")
    private long countPosts;
    @SerializedName("android_version")
    private int androidVersion;

    /**
     * @return the posts
     */
    public List<Post> getPosts() {
        return posts;
    }

    /**
     * @param posts the posts to set
     */
    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    /**
     * @return the countPosts
     */
    public long getCountPosts() {
        return countPosts;
    }

    public String getCountPostsStr() {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);

        return (formatter.format(this.countPosts)).replace(",", ".");
    }

    /**
     * @param countPosts the countPosts to set
     */
    public void setCountPosts(long countPosts) {
        this.countPosts = countPosts;
    }

    public int getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(int androidVersion) {
        this.androidVersion = androidVersion;
    }
}
