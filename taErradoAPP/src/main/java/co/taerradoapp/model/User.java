package co.taerradoapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class User {

	private Integer id;
	private String email;
	@SerializedName("fb_id")
	private String fbId;
	@SerializedName("auth_token")
	private String authToken;
	@SerializedName("curses_count")
	private Long cursesCount;
	@SerializedName("posts_count")
	private Long postsCount;
	@SerializedName("created_at")
	private Date createdAt;
	@SerializedName("profile_image_url")
	private String profileImageUrl;
	
	public String getDiasApp() {
		if (this.createdAt != null) {
			return String.valueOf(getDateDiff(createdAt, new Date(), TimeUnit.DAYS));
		}
		return "";
	}
	
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the fbId
	 */
	public String getFbId() {
		return fbId;
	}
	/**
	 * @param fbId the fbId to set
	 */
	public void setFbId(String fbId) {
		this.fbId = fbId;
	}
	/**
	 * @return the authToken
	 */
	public String getAuthToken() {
		return authToken;
	}
	/**
	 * @param authToken the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	/**
	 * @return the cursesCount
	 */
	public Long getCursesCount() {
		return cursesCount;
	}
	/**
	 * @param cursesCount the cursesCount to set
	 */
	public void setCursesCount(Long cursesCount) {
		this.cursesCount = cursesCount;
	}
	/**
	 * @return the postsCount
	 */
	public Long getPostsCount() {
		return postsCount;
	}
	/**
	 * @param postsCount the postsCount to set
	 */
	public void setPostsCount(Long postsCount) {
		this.postsCount = postsCount;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the profileImageUrl
	 */
	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	/**
	 * @param profileImageUrl the profileImageUrl to set
	 */
	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}
	
	
}
