package co.taerradoapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.multidex.MultiDexApplication;

import com.doisdoissete.android.util.ddsutil.gps.GPSTracker;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;

import java.util.Date;

import co.taerradoapp.service.background.DeleteFotosUploadBackgrounService;
import co.taerradoapp.util.SharedPrefManager;
import co.taerradoapp.util.UserImageLoader;

public class TaErradoApplication extends MultiDexApplication {

    public static ImageLoader imageLoader;
    public static ImageLoader imageLoaderUser;
    private static GPSTracker gpsTracker;

    @Override
    public void onCreate() {
        super.onCreate();

        //init SharedPrefManager
        SharedPrefManager.init(this);

        gpsTracker = new GPSTracker(this);
        Parse.initialize(this, BuildConfig.PARSE_APP_ID, BuildConfig.PARSE_CLIENT_KEY);

        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                String deviceToken = (String) ParseInstallation.getCurrentInstallation().get("deviceToken");
                SharedPrefManager.getInstance().setDeviceToken(deviceToken);
            }
        });

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_place_holder)
                .showImageForEmptyUri(R.drawable.img_place_holder)
                .showImageOnFail(R.drawable.img_place_holder)
                .resetViewBeforeLoading(false)
                .delayBeforeLoading(100)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .resetViewBeforeLoading(true)
                .considerExifParams(false)
                .cacheInMemory(true)
                .cacheOnDisc(true).build();

        DisplayImageOptions optionsUser = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_placeholder_user)
                .showImageForEmptyUri(R.drawable.img_placeholder_user)
                .showImageOnFail(R.drawable.img_placeholder_user)
                .resetViewBeforeLoading(false)
                .delayBeforeLoading(100)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .resetViewBeforeLoading(true)
                .considerExifParams(false)
                .cacheInMemory(true)
                .cacheOnDisc(true).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(options)
                .memoryCache(new WeakMemoryCache())
                .build();

        ImageLoaderConfiguration configUser = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(optionsUser)
                .memoryCache(new WeakMemoryCache())
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoaderUser = UserImageLoader.getInstance();
        imageLoader.init(config);
        imageLoaderUser.init(configUser);
        Date dt = SharedPrefManager.getInstance().getLastClearCache();

        if (dt == null || (int) ((new Date().getTime() - dt.getTime()) / (24 * 60 * 60 * 1000)) > BuildConfig.DIAS_CACHE) {
            TaErradoApplication.imageLoader.clearDiskCache();
            TaErradoApplication.imageLoader.clearMemoryCache();
            TaErradoApplication.imageLoaderUser.clearDiskCache();
            TaErradoApplication.imageLoaderUser.clearMemoryCache();
            SharedPrefManager.getInstance().setLastClearCache(new Date());
            DeleteFotosUploadBackgrounService.callService(this);
        }
    }

    public static GPSTracker getGpsTracker(Context context) {
        if (gpsTracker == null) {
            gpsTracker = new GPSTracker(context);
        }
        return gpsTracker;
    }
}   
