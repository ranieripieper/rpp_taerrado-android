package com.doisdoissete.android.util.ddsutil.view;

import android.app.Activity;
import android.content.Context;

public class ResourceUtil {

	public static int getLayoutId(String layoutName, Activity activity) {
		return activity.getResources().getIdentifier(layoutName, "layout",
				activity.getPackageName());
	}

	public static int getId(String id, Activity activity) {
		return activity.getResources().getIdentifier(id, "id",
				activity.getPackageName());
	}

	public static int getDrawableId(String name, Activity activity) {
		return activity.getResources().getIdentifier(name, "drawable",
				activity.getPackageName());
	}

	public static int getStringId(String name, Activity activity) {
		return activity.getResources().getIdentifier(name, "string",
				activity.getPackageName());
	}
	
	public static int getColorId(String name, Context mContext) {
		int resColor = mContext.getResources().getIdentifier(name, "color", mContext.getPackageName());
		return mContext.getResources().getColor(resColor);
	}
}
