package com.doisdoissete.android.util.ddsutil.gps;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

public class GpsUtil {

	public static boolean gpsIsEnabled(Context ctx) {
		final LocationManager manager = (LocationManager)ctx.getSystemService( Context.LOCATION_SERVICE );
		 if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
			 return false;
		 }
		 return true;
	}
   
    
	public static android.location.Location getLocation(Context ctx) {
	    LocationManager mLocationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

	    Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	    Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

	    if (locationGPS != null) {
	    	return locationGPS;
	    }
	    return locationNet;
	}
	
	public static String distance(Context mContext, double lat1, double lon1) {
		Location loc1 = new Location("");
		loc1.setLatitude(lat1);
		loc1.setLongitude(lon1);

		Location loc2 = GpsUtil.getLocation(mContext);
		if (loc2 != null) {
			float distanceInMeters = loc1.distanceTo(loc2);
			float distanceInKm = distanceInMeters / 1000;
			
			if (distanceInKm < 1 ) {
				return String.format("%.0fm", distanceInMeters);
			} else {
				return String.format("%.2fKm", distanceInKm);
			}
		}
		
		return "";
		
	}
	
	public static float distanceFloat(Context mContext, double lat1, double lon1) {
		Location loc1 = new Location("");
		loc1.setLatitude(lat1);
		loc1.setLongitude(lon1);

		Location loc2 = GpsUtil.getLocation(mContext);
		if (loc2 != null) {
			float distanceInMeters = loc1.distanceTo(loc2);
			
			return distanceInMeters;
		}
		
		return -1;
		
	}
}
