package com.doisdoissete.android.util.ddsutil.view.cameragallery;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Size;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;


public abstract class CameraActivity extends AppCompatActivity implements SurfaceHolder.Callback, Camera.PictureCallback {

    public static final String TAG = CameraActivity.class.getSimpleName();
    public static final String CAMERA_ID_KEY = "camera_id";
    public static final String CAMERA_FLASH_KEY = "flash_mode";
    public static final String PREVIEW_HEIGHT_KEY = "preview_height";

    private static final int PICTURE_SIZE_MAX_WIDTH = 1280;
    private static final int PREVIEW_SIZE_MAX_WIDTH = 640;

    private int mCameraID;
    private String mFlashMode;
    private Camera mCamera;
    private SquareCameraPreview mPreviewView;
    private SurfaceHolder mSurfaceHolder;

    private int mDisplayOrientation;
    private int mLayoutOrientation;

    private int mCoverHeight;
    private int mPreviewHeight;

    private CameraOrientationListener mOrientationListener;
    
    //usado para galeria
	protected String filePath = null;
	
    protected abstract Integer getButtonFlashId();
    protected abstract Integer getTextAutoFlashId();
    protected abstract Integer getSquareCameraPreviewId();
    protected abstract Integer getTopCoverViewId();
    protected abstract Integer getBottomCoverViewId();
    protected abstract void onPictureTaken(byte[] data, int rotation);
    protected abstract void onPictureSelected(String filePath, Bitmap bitmap);
    protected abstract Integer getButtonSwitchCameraId();
    protected abstract Integer getButtonCaptureId();
    protected abstract Integer getButtonGalleryId();
    
    protected boolean canCapturePicture() {
    	return true;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        mOrientationListener = new CameraOrientationListener(this);
        mOrientationListener.enable();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	mPreviewView.getHolder().removeCallback(this);
    	if (mSurfaceHolder == null) {
    		mPreviewView.getHolder().addCallback(this);
    	} else {
    		getCamera(mCameraID);
            startCameraPreview();
    	}
    }
    
    protected Context getContext() {
    	return this;
    }
    
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    BitmapFilePath bitmapFilePath = null;

	    if (requestCode == NativeCameraGalleyUtil.PICTURE_TAKEN_FROM_GALLERY) {
	    	bitmapFilePath = NativeCameraGalleyUtil.processActivityResult(CameraActivity.this, resultCode, requestCode, data, null);
	    	if(bitmapFilePath != null) {
		    	stopCameraPreview();
		        mCamera.release();
		    	onPictureSelected(bitmapFilePath.getFilePath(), bitmapFilePath.getBitmap());
		    } else {
		    }
	    }	          
	}
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	if (savedInstanceState == null) {
            mCameraID = getBackCameraID();
            mFlashMode = Camera.Parameters.FLASH_MODE_AUTO;
        } else {
            mCameraID = savedInstanceState.getInt(CAMERA_ID_KEY);
            mFlashMode = savedInstanceState.getString(CAMERA_FLASH_KEY);
            mPreviewHeight = savedInstanceState.getInt(PREVIEW_HEIGHT_KEY);
        }
    	super.onRestoreInstanceState(savedInstanceState);
        
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	outState.putInt(CAMERA_ID_KEY, mCameraID);
        outState.putString(CAMERA_FLASH_KEY, mFlashMode);
        outState.putInt(PREVIEW_HEIGHT_KEY, mPreviewHeight);
    	super.onSaveInstanceState(outState);
    }
    
    @Override
    public void setContentView(int layoutResID) {
    	super.setContentView(layoutResID);
    	mPreviewView = (SquareCameraPreview) findViewById(getSquareCameraPreviewId());
        
        
        final View topCoverView = findViewById(getTopCoverViewId());
        final View btnCoverView = findViewById(getBottomCoverViewId());
        
    	if (mCoverHeight == 0) {
            ViewTreeObserver observer = mPreviewView.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int width = mPreviewView.getWidth();
                    mPreviewHeight = mPreviewView.getHeight();
                    mCoverHeight = (mPreviewHeight - width) / 2;

                    Log.d(TAG, "preview width " + width + " height " + mPreviewHeight);
                    topCoverView.getLayoutParams().height = mCoverHeight;
                    btnCoverView.getLayoutParams().height = mCoverHeight;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        mPreviewView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        mPreviewView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                }
            });
        } else {
            topCoverView.getLayoutParams().height = mCoverHeight;
            btnCoverView.getLayoutParams().height = mCoverHeight;
        }
    	
    	if (getButtonSwitchCameraId() != null) {
        	final ImageView swapCameraBtn = (ImageView) findViewById(getButtonSwitchCameraId());
            swapCameraBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCameraID == CameraInfo.CAMERA_FACING_FRONT) {
                        mCameraID = getBackCameraID();
                    } else {
                        mCameraID = getFrontCameraID();
                    }
                    restartPreview();
                }
            });
    	}

        Integer flashButtonId = getButtonFlashId();
        Integer textAutoFlashId = getTextAutoFlashId();
        if (flashButtonId != null && textAutoFlashId != null) {
        	final View changeCameraFlashModeBtn = findViewById(flashButtonId);
            final TextView autoFlashIcon = (TextView) findViewById(textAutoFlashId);
            changeCameraFlashModeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_AUTO)) {
                        mFlashMode = Camera.Parameters.FLASH_MODE_ON;
                        autoFlashIcon.setText("On");
                    } else if (mFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_ON)) {
                        mFlashMode = Camera.Parameters.FLASH_MODE_OFF;
                        autoFlashIcon.setText("Off");
                    } else if (mFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_OFF)) {
                        mFlashMode = Camera.Parameters.FLASH_MODE_AUTO;
                        autoFlashIcon.setText("Auto");
                    }

                    setupCamera();
                }
            });
        }

        if (getButtonCaptureId() != null) {
        	final Button takePhotoBtn = (Button) findViewById(getButtonCaptureId());
            takePhotoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    takePicture();
                }
            });
        }       
        
        if (getButtonGalleryId() != null) {
        	findViewById(getButtonGalleryId()).setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                    NativeCameraGalleyUtil.getPictureFromGallery(CameraActivity.this);
                 }
             });
        }
    }
    
    private void getCamera(int cameraID) {
        Log.d(TAG, "get camera with id " + cameraID);
        try {
            mCamera = Camera.open(cameraID);
            mPreviewView.setCamera(mCamera);
        } catch (Exception e) {
            Log.d(TAG, "Can't open camera with id " + cameraID);
        }
    }

    /**
     * Start the camera preview
     */
    protected void startCameraPreview() {
        determineDisplayOrientation();
        setupCamera();

        try {
            mCamera.setPreviewDisplay(mSurfaceHolder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Can't start camera preview due to IOException " + e);
        }
    }

    /**
     * Stop the camera preview
     */
    protected void stopCameraPreview() {
        // Nulls out callbacks, stops face detection
    	if (mCamera != null) {
    		try {
           	 mCamera.stopPreview();
           } catch (Exception e) {
           }
    	}
        if (mPreviewView != null) {
        	mPreviewView.setCamera(null);
        }
       
       
    }

    /**
     * Determine the current display orientation and rotate the camera preview
     * accordingly
     */
    private void determineDisplayOrientation() {
        CameraInfo cameraInfo = new CameraInfo();
        Camera.getCameraInfo(mCameraID, cameraInfo);

        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: {
                degrees = 0;
                break;
            }
            case Surface.ROTATION_90: {
                degrees = 90;
                break;
            }
            case Surface.ROTATION_180: {
                degrees = 180;
                break;
            }
            case Surface.ROTATION_270: {
                degrees = 270;
                break;
            }
        }

        int displayOrientation;

        // Camera direction
        if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
            // Orientation is angle of rotation when facing the camera for
            // the camera image to match the natural orientation of the device
            displayOrientation = (cameraInfo.orientation + degrees) % 360;
            displayOrientation = (360 - displayOrientation) % 360;
        } else {
            displayOrientation = (cameraInfo.orientation - degrees + 360) % 360;
        }

        mDisplayOrientation = (cameraInfo.orientation - degrees + 360) % 360;
        mLayoutOrientation = degrees;

        mCamera.setDisplayOrientation(displayOrientation);
    }

    
    
    /**
     * Setup the camera parameters
     */
    private void setupCamera() {
        // Never keep a global parameters
        Camera.Parameters parameters = mCamera.getParameters();

        Size bestPreviewSize = determineBestPreviewSize(parameters);
        Size bestPictureSize = determineBestPictureSize(parameters);

        parameters.setPreviewSize(bestPreviewSize.width, bestPreviewSize.height);
        parameters.setPictureSize(bestPictureSize.width, bestPictureSize.height);

        // Set continuous picture focus, if it's supported
        if (parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        Integer buttonFlashId = getButtonFlashId();
        if (buttonFlashId != null) {
        	 final View changeCameraFlashModeBtn = findViewById(buttonFlashId);
             List<String> flashModes = parameters.getSupportedFlashModes();
             if (flashModes != null && flashModes.contains(mFlashMode)) {
                 parameters.setFlashMode(mFlashMode);
                 changeCameraFlashModeBtn.setVisibility(View.VISIBLE);
             } else {
                 changeCameraFlashModeBtn.setVisibility(View.INVISIBLE);
             }
        }
       
        // Lock in the changes
        mCamera.setParameters(parameters);
    }

    private Size determineBestPreviewSize(Camera.Parameters parameters) {
        return determineBestSize(parameters.getSupportedPreviewSizes(), PREVIEW_SIZE_MAX_WIDTH);
    }

    private Size determineBestPictureSize(Camera.Parameters parameters) {
        return determineBestSize(parameters.getSupportedPictureSizes(), PICTURE_SIZE_MAX_WIDTH);
    }

    private Size determineBestSize(List<Size> sizes, int widthThreshold) {
        Size bestSize = null;
        Size size;
        int numOfSizes = sizes.size();
        for (int i = 0; i < numOfSizes; i++) {
            size = sizes.get(i);
            boolean isDesireRatio = (size.width / 4) == (size.height / 3);
            boolean isBetterSize = (bestSize == null) || size.width > bestSize.width;

            if (isDesireRatio && isBetterSize) {
                bestSize = size;
            }
        }

        if (bestSize == null) {
            Log.d(TAG, "cannot find the best camera size");
            return sizes.get(sizes.size() - 1);
        }

        return bestSize;
    }

    private void restartPreview() {
        stopCameraPreview();
        mCamera.release();

        getCamera(mCameraID);
        startCameraPreview();
    }

    private int getFrontCameraID() {
        PackageManager pm = getPackageManager();
        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            return CameraInfo.CAMERA_FACING_FRONT;
        }

        return getBackCameraID();
    }

    private int getBackCameraID() {
        return CameraInfo.CAMERA_FACING_BACK;
    }

    /**
     * Take a picture
     */
    private void takePicture() {
    	if (canCapturePicture()) {
    		mOrientationListener.rememberOrientation();
            // Shutter callback occurs after the image is captured. This can
            // be used to trigger a sound to let the user know that image is taken
            Camera.ShutterCallback shutterCallback = null;

            // Raw callback occurs when the raw image data is available
            Camera.PictureCallback raw = null;

            // postView callback occurs when a scaled, fully processed
            // postView image is available.
            Camera.PictureCallback postView = null;

            // jpeg callback occurs when the compressed image is available
            mCamera.takePicture(shutterCallback, raw, postView, this);
    	}
        
    }

    @Override
    protected void onRestart() {
    	super.onRestart();
    	//restartPreview();
    }
    
    @Override
    public void onPause() {
        mOrientationListener.disable();

        // stop the preview
        stopCameraPreview();
        if (mCamera != null) {
            mCamera.release();
        }
        super.onPause();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mSurfaceHolder = holder;
        stopCameraPreview();
        getCamera(mCameraID);
        startCameraPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    	stopCameraPreview();
    	mSurfaceHolder = null;
    }

    /**
     * A picture has been taken
     * @param data
     * @param camera
     */
    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
    	stopCameraPreview();
        mCamera.release();
    	
        int rotation = (
                mDisplayOrientation
                        + mOrientationListener.getRememberedNormalOrientation()
                        + mLayoutOrientation
        ) % 360;

        onPictureTaken(data, rotation);
    }
    
    /**
     * When orientation changes, onOrientationChanged(int) of the listener will be called
     */
    private static class CameraOrientationListener extends OrientationEventListener {

        private int mCurrentNormalizedOrientation;
        private int mRememberedNormalOrientation;

        public CameraOrientationListener(Context context) {
            super(context, SensorManager.SENSOR_DELAY_NORMAL);
        }

        @Override
        public void onOrientationChanged(int orientation) {
            if (orientation != ORIENTATION_UNKNOWN) {
                mCurrentNormalizedOrientation = normalize(orientation);
            }
        }

        private int normalize(int degrees) {
            if (degrees > 315 || degrees <= 45) {
                return 0;
            }

            if (degrees > 45 && degrees <= 135) {
                return 90;
            }

            if (degrees > 135 && degrees <= 225) {
                return 180;
            }

            if (degrees > 225 && degrees <= 315) {
                return 270;
            }

            throw new RuntimeException("The physics as we know them are no more. Watch out for anomalies.");
        }

        public void rememberOrientation() {
            mRememberedNormalOrientation = mCurrentNormalizedOrientation;
        }

        public int getRememberedNormalOrientation() {
            return mRememberedNormalOrientation;
        }
    }
}
