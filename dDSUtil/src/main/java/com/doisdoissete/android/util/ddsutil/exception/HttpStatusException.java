package com.doisdoissete.android.util.ddsutil.exception;

public class HttpStatusException extends Exception {

	private static final long serialVersionUID = 1L;

	public HttpStatusException() {}
	
	private int status;
	
	public HttpStatusException(Throwable e, int status) {
		super(e);
		this.status = status;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
