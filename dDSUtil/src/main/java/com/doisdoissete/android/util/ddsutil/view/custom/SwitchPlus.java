package com.doisdoissete.android.util.ddsutil.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Switch;

import com.doisdoissete.android.util.ddsutil.R;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;

public class SwitchPlus extends Switch {
	
	private static final String TAG = "SwitchPlus";

	public SwitchPlus(Context context) {
		super(context);
	}

	public SwitchPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
		setCustomFont(context, attrs);
	}

	public SwitchPlus(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setCustomFont(context, attrs);
	}

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewPlus);
        TypedArray defaultAttr = ctx.obtainStyledAttributes(attrs, R.styleable.DDSDefaultAttributes);
        String customFont = defaultAttr.getString(R.styleable.DDSDefaultAttributes_text_font);
        defaultAttr.recycle();
        a.recycle();
        
        setTypeFace(ctx, customFont);
    }
    
    private void setTypeFace(Context ctx, String asset) {
		if (asset == null) {
			return;
		}
		Typeface tf = null;
		try {
			tf = FontUtilCache.get(asset, ctx);
		} catch (Exception e) {
			return;
		}

		setSwitchTypeface(tf);
    }

}
