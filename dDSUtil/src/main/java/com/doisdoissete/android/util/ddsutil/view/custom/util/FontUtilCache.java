package com.doisdoissete.android.util.ddsutil.view.custom.util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.widget.TextView;

import java.util.Hashtable;

public class FontUtilCache {
	
	private static final String TAG = "FontUtilCache";
	
	private static Hashtable<String, Typeface> fontCache = new Hashtable<String, Typeface>();

	public static boolean setCustomFont(Context ctx, TextView txtView, String asset) {
		if (asset == null) {
			return false;
		}
		Typeface tf = null;
		try {
			tf = get(asset, ctx);
		} catch (Exception e) {
			Log.e(TAG, "Could not get typeface: " + e.getMessage());
			return false;
		}

		txtView.setTypeface(tf);
		return true;
	}
	
    public static Typeface get(String asset, Context context) {
    	if (asset == null) {
			return null;
		}
        Typeface tf = fontCache.get(asset);
        if(tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), asset);
            }
            catch (Exception e) {
                return null;
            }
            fontCache.put(asset, tf);
        }
        return tf;
    }
}
