package com.doisdoissete.android.util.ddsutil.service.asynctask;

import android.content.Context;
import android.os.AsyncTask;

import com.doisdoissete.android.util.ddsutil.exception.ParseException;

import org.springframework.web.client.ResourceAccessException;

public class AsyncTaskService extends AsyncTask<Void, Void, Object>{

	private BaseService service;
	protected Context ctx;
	protected ObserverAsyncTask observer;
	protected Exception mException;
	protected boolean error = false;
	
	public AsyncTaskService(Context ctx) {
		this.ctx = ctx;
	}
	
	public AsyncTaskService(Context ctx, BaseService service, ObserverAsyncTask obs) {
		this.service = service;
		this.ctx = ctx;
		this.observer = obs;
	}
	
	public boolean isRunning() {
		if (Status.RUNNING.equals(this.getStatus())) {
			return true;
		}
		return false;
	}
	
	public boolean isPending() {
		if (Status.PENDING.equals(this.getStatus())) {
			return true;
		}
		return false;
	}
	
	protected Object doInBackground(Void... paramsV) {
		try {
			return service.execute(ctx);
		} catch (ResourceAccessException e) {
			this.error = true;
			this.mException = e;
		} catch (ParseException e) {
			this.error = true;
			this.mException = e;
		} catch (RuntimeException e) {
			this.error = true;
			this.mException = e;
		} catch (Exception e) {
			this.error = true;
			this.mException = e;
		}
		return null;
	};
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (observer != null) {
			observer.onPreExecute();
		}
	}
	
	protected void onPostExecute(Object result) {
		if (error) {
			onError();
			return;
		}
		if (observer != null) {
			observer.onPostExecute(result);
		}
		
	};
	
	@Override
	protected void onCancelled() {
		super.onCancelled();
		if (observer != null) {
			observer.onCancelled();
		}
		
	}
	
	protected void onError() {
		if (observer != null) {
			observer.onError(mException);
		}
		
	};
}
