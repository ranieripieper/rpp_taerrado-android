package com.doisdoissete.android.util.ddsutil.service.asynctask;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.apache.http.client.methods.HttpUriRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.ConnectException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class BaseService {
	
	private boolean needConnection = true;
	protected String url = null;
	protected TypeToken<?> targetType;
	protected MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
	protected JsonDeserializer jsonDeserializer;
	protected HttpMethod httpMethod = HttpMethod.POST;
	protected HttpHeaders httpHeaders = new HttpHeaders();
	protected String charset = null;
	protected boolean uploadFile = false;
	protected ServiceBuilder mServiceBuilder;
	
	public BaseService(ServiceBuilder builder) {
		setParams(builder);
	}
	
	
	public void refreshServiceBuilder(ServiceBuilder builder) {
		setParams(builder);
	}
	
	private void setParams(ServiceBuilder builder) {
		this.mServiceBuilder = builder;
		this.url = builder.getUrl();
		this.targetType = builder.getTargetType();
		this.params = builder.getParams();
		this.jsonDeserializer = builder.getJsonDeserializer();
		this.httpMethod = builder.getHttpMethod();
		this.httpHeaders = builder.getHttpHeaders();
		this.uploadFile = builder.isUploadFile();
		this.charset = builder.getCharset();
		this.needConnection = builder.isNeedConnection();
	}
	
	public static MultiValueMap<String, Object> createParams(ParamServiceInterface... views) {
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		
		if (views != null) {
			for (ParamServiceInterface p : views) {
				if (!TextUtils.isEmpty(p.getParamService())) {
					params.add(p.getParamService(), p.getValue());
				}
			}
		}
		
		return params;
	}

	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException, DdsUtilIOException {
		
		//if (this.uploadFile) {
		//	return callServiceUploadFile(ctx);
		//} else {
			RestTemplate restTemplate = new RestTemplate();
			FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
			Gson gson = Util.getGson(null, jsonDeserializer, targetType);
			
			if (charset != null) {
				restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName(charset)));
			} else {
				restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
			}

			HttpHeaders headers = null;
			if (this.httpHeaders != null) {
				headers = this.httpHeaders;
			} else {
				headers = new HttpHeaders();
			}

			String result = "";
	
			if (HttpMethod.POST == httpMethod) {
				headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
				HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(params, headers);
	
				restTemplate.getMessageConverters().add(formHttpMessageConverter);
				
				setHanlderError(restTemplate);
				result = restTemplate.postForObject(url, request, String.class);
			} else if (HttpMethod.PUT == httpMethod) {
				headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
				restTemplate.getMessageConverters().add(formHttpMessageConverter);
				HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(params, headers);
				
				restTemplate.getMessageConverters().add(formHttpMessageConverter);
				
				setHanlderError(restTemplate);
				HttpEntity<String> response = restTemplate.exchange(url, httpMethod, request, String.class);
				
				result = response.getBody();
			} else if (HttpMethod.DELETE == httpMethod) {
				HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(params, headers);
				new HttpEntity<String>("some sample body sent along the DELETE request");
				
				 restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory() {
					 @Override
					protected HttpUriRequest createHttpRequest(
							HttpMethod httpMethod, URI uri) {
						if (HttpMethod.DELETE == httpMethod) {
			                return new HttpEntityEnclosingDeleteRequest(uri);
			            }
						return super.createHttpRequest(httpMethod, uri);
					}
				    });
				restTemplate.getMessageConverters().add(formHttpMessageConverter);
				
				setHanlderError(restTemplate);
				
				ResponseEntity<String> response = restTemplate.exchange(
				            url,
				            HttpMethod.DELETE,
				            request,
				            String.class);
				
				
				result = response.getBody();
			} else if (HttpMethod.GET == httpMethod) {
				Map<String, String> paramsGet = new HashMap<String, String>();
				if (params != null && params.size() > 0) {
					for (String key : params.keySet()) {
						List<Object> lst = params.get(key);
						if (lst != null && lst.size() > 0) {
							paramsGet.put(key, lst.get(0).toString());
						}
					}
				}
				
				HttpEntity entity = new HttpEntity(headers);
				
				setHanlderError(restTemplate);
				
				HttpEntity<String> response = restTemplate.exchange(url, httpMethod, entity, String.class, paramsGet);
				
				result = response.getBody();
			} else {
				HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(params, headers);
				
				restTemplate.getMessageConverters().add(formHttpMessageConverter);
				HttpEntity<String> response = restTemplate.exchange(url, httpMethod, request, String.class);
				
				setHanlderError(restTemplate);
				
				result = response.getBody();
			}
			
			if (result != null) {
				if (String.class.equals(targetType.getType())) {
					return result;
				} else {
					return gson.fromJson(result, targetType.getType());
				}
			}
	
			return null;
		//}
	}
	
	private void setHanlderError(RestTemplate restTemplate) {
		restTemplate.setErrorHandler( new ResponseErrorHandler() {
			
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				try {
					if (clientHttpResponse != null && clientHttpResponse.getStatusCode() != null) {
						return isError(clientHttpResponse.getStatusCode());
					} else {
						return true;
					}
					
				} catch(IOException e) {
					return true;
				} catch(NullPointerException e) {
					return true;
				}
				
			}
			
			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
				if (!HttpStatus.OK.equals(clientHttpResponse.getStatusCode()) && !HttpStatus.CREATED.equals(clientHttpResponse.getStatusCode())
						&& !HttpStatus.ACCEPTED.equals(clientHttpResponse.getStatusCode())) {
					String body = "";
					try {
						body = getStringFromInputStream(clientHttpResponse.getBody());
					} catch(IOException e) {
						e.printStackTrace();
					}
					DdsUtilIOException exception = new DdsUtilIOException(body, clientHttpResponse.getStatusCode());
			        throw exception;
				} else {
					//return false;
				}
			}
		});
	}
	
	public static String getStringFromInputStream(InputStream stream) throws IOException {
	    int n = 0;
	    char[] buffer = new char[1024 * 4];
	    InputStreamReader reader = new InputStreamReader(stream, "UTF8");
	    StringWriter writer = new StringWriter();
	    while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
	    return writer.toString();
	}
	
	public Object callServiceUploadFile(Context ctx) {
		FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        formHttpMessageConverter.setCharset(Charset.forName("UTF8"));

        RestTemplate restTemplate = new RestTemplate();


        restTemplate.getMessageConverters().add( formHttpMessageConverter );
        if (charset != null) {
			restTemplate.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName(charset)));
		} else {
			restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		}


        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());

        HttpHeaders imageHeaders = null;
		if (this.httpHeaders != null) {
			imageHeaders = this.httpHeaders;
		} else {
			imageHeaders = new HttpHeaders();
		}
		
        imageHeaders.setContentType(new MediaType("multipart", "form-data", Charset.forName(charset)));
        HttpEntity<MultiValueMap<String, Object>> imageEntity = new HttpEntity<MultiValueMap<String, Object>>(params, imageHeaders);

        setHanlderError(restTemplate);
        
        ResponseEntity<String> response = restTemplate.exchange(url, httpMethod, imageEntity, String.class);
        
        if (response != null && response.getBody() != null) {
        	String result = response.getBody();
        	
        	if (result != null) {
				if (String.class.equals(targetType.getType())) {
					return result;
				} else {
					Gson gson = Util.getGson(null, jsonDeserializer, targetType);
					return gson.fromJson(result, targetType.getType());
				}
			}
        }
        return null;
	}
	
	public Object execute(Context ctx) throws ConnectionNotFoundException, ParseException, ConnectionException, DdsUtilIOException {
		if (!needConnection || internetConnection(ctx)) {
			try {
				return callService(ctx);
			} catch(JsonIOException e) {
				throw new ParseException(e);
			} catch(JsonSyntaxException e) {
				throw new ParseException(e);
			} catch(JsonParseException e) {
				throw new ParseException(e);
			} catch (ConnectException e) {
				throw new ConnectionException(e);
			} catch (DdsUtilIOException e) {
				throw e;
			} catch (ResourceAccessException e) {
				if (e.getCause() instanceof DdsUtilIOException) {
					throw (DdsUtilIOException)e.getCause();
				}
				HttpStatus status = HttpStatus.UNAUTHORIZED;
				throw new ConnectionException(e, e.getMessage(), status);
			} catch (RestClientException e) {
				String errorResponse = null;
				HttpStatus status = null;
				if(e instanceof HttpStatusCodeException){
					errorResponse = ((HttpStatusCodeException)e).getResponseBodyAsString();
					status = ((HttpStatusCodeException)e).getStatusCode();
				}
				throw new ConnectionException(e, errorResponse, status);
			}
		} else {
			throw new ConnectionNotFoundException();
		}
		
	}
	
	public void addParam(String key, String value) {
		params.add(key, value);
	}
	
	public static boolean internetConnection(Context context) {
		ConnectivityManager connMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connMgr.getActiveNetworkInfo();
		return (info != null && info.isConnected() && info.isAvailable());
	}

	public boolean isNeedConnection() {
		return needConnection;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
    public static boolean isError(HttpStatus status) {
        HttpStatus.Series series = status.series();
        return (HttpStatus.Series.CLIENT_ERROR.equals(series)
                || HttpStatus.Series.SERVER_ERROR.equals(series));
    }
	
}
