package com.doisdoissete.android.util.ddsutil.view.custom.listview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.R;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;

import java.util.List;

public class PagingListView extends ListView {

	public interface Pagingable {
		void onLoadMoreItems();
	}

	private boolean isLoading;
	private boolean hasMoreItems;
	private Pagingable pagingableListener;
	private View loadinView;

	private OnScrollListener onScrollListener;

	public PagingListView(Context context) {
		super(context);
		init(null);
	}

	public PagingListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public PagingListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public boolean isLoading() {
		return this.isLoading;
	}

	public void setIsLoading(boolean isLoading) {
		this.isLoading = isLoading;
	}

	public void setPagingableListener(Pagingable pagingableListener) {
		this.pagingableListener = pagingableListener;
	}

	public void setHasMoreItems(boolean hasMoreItems) {
		this.hasMoreItems = hasMoreItems;
		if (!this.hasMoreItems) {
			removeFooterView(loadinView);
		} else {
			if (this.getFooterViewsCount() <= 0) {
				addFooterView(loadinView);
			}
		}
	}

	public boolean hasMoreItems() {
		return this.hasMoreItems;
	}

	public void onFinishLoading(boolean hasMoreItems,
			List<? extends Object> newItems) {
		setHasMoreItems(hasMoreItems);
		setIsLoading(false);
		if (newItems != null && newItems.size() > 0 && getAdapter() != null) {
			ListAdapter adapter = ((HeaderViewListAdapter) getAdapter())
					.getWrappedAdapter();
			if (adapter instanceof PagingBaseAdapter) {
				((PagingBaseAdapter) adapter).addMoreItems(newItems);
			}
		}
	}
	
	public void hideTxtCarregando() {
		TextView txtCarregando = (TextView) loadinView.findViewById(R.id.txt_carregando);
		if (txtCarregando != null) {
			txtCarregando.setVisibility(View.GONE);
		}
	}

	private void init(AttributeSet attrs) {
		isLoading = false;
		loadinView = ((LayoutInflater) getContext().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE)).inflate(
				R.layout.loading_view, null);

		LoadingView loadView = (LoadingView)loadinView.findViewById(R.id.load_view);
		loadView.parseAttributes(getContext().obtainStyledAttributes(attrs, R.styleable.LoadingView));
		
		// fonte
		TypedArray defaultAttr = getContext().obtainStyledAttributes(attrs, R.styleable.DDSDefaultAttributes);
		String customFont = defaultAttr.getString(R.styleable.DDSDefaultAttributes_text_font);
		defaultAttr.recycle();

		//texto visivel
		TypedArray loadingAttr = getContext().obtainStyledAttributes(attrs, R.styleable.ListViewAttributes);
		boolean visible = loadingAttr.getBoolean(R.styleable.ListViewAttributes_load_txt_carregando_visible, true);
		defaultAttr.recycle();
		
		TextView txtCarregando = (TextView) loadinView.findViewById(R.id.txt_carregando);
		if (txtCarregando != null) {
			FontUtilCache.setCustomFont(getContext(), txtCarregando, customFont);
			if (!visible) {
				txtCarregando.setVisibility(View.GONE);
			}
		}

		addFooterView(loadinView);
		super.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// Dispatch to child OnScrollListener
				if (onScrollListener != null) {
					onScrollListener.onScrollStateChanged(view, scrollState);
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {

				// Dispatch to child OnScrollListener
				if (onScrollListener != null) {
					onScrollListener.onScroll(view, firstVisibleItem,
							visibleItemCount, totalItemCount);
				}

				if (totalItemCount > 0) {
					int lastVisibleItem = firstVisibleItem + visibleItemCount;
					if (!isLoading && hasMoreItems
							&& (lastVisibleItem == totalItemCount)) {
						if (pagingableListener != null) {
							isLoading = true;
							pagingableListener.onLoadMoreItems();
						}

					}
				}
			}
		});
	}

	@Override
	public void setOnScrollListener(OnScrollListener listener) {
		onScrollListener = listener;
	}
}
