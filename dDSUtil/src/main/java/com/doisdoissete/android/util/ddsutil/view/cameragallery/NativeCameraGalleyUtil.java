package com.doisdoissete.android.util.ddsutil.view.cameragallery;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Toast;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;

import java.io.File;

public class NativeCameraGalleyUtil {

	public static final int PICTURE_TAKEN_FROM_CAMERA = 7991;
	public static final int PICTURE_TAKEN_FROM_GALLERY = 7992;
	
	public static void getPictureFromGallery(Activity activity) {
	    Intent gallerypickerIntent = new Intent(Intent.ACTION_PICK);
	    gallerypickerIntent.setType("image/*");
	    activity.startActivityForResult(gallerypickerIntent, PICTURE_TAKEN_FROM_GALLERY); 
	}
	
	public static void getPictureFromCamera(Activity activity, File outFile) {
	    boolean cameraAvailable = BitmapUtil.device_isHardwareFeatureAvailable(activity, PackageManager.FEATURE_CAMERA);
	    if(cameraAvailable && Util.system_isIntentAvailable(activity, MediaStore.ACTION_IMAGE_CAPTURE)) {
	        Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	        takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outFile));    
	        activity.startActivityForResult(takePicIntent, NativeCameraGalleyUtil.PICTURE_TAKEN_FROM_CAMERA);
	    } else {
	        if (cameraAvailable) {
	            Toast.makeText(activity, "No application that can receive the intent camera.", Toast.LENGTH_SHORT).show();;
	        } else {
	            Toast.makeText(activity, "No camera present!!", Toast.LENGTH_SHORT).show();;
	        }
	    }
	}
	
	public static BitmapFilePath processActivityResult(Activity activity, int resultCode, int requestCode, Intent data, File outFile) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == NativeCameraGalleyUtil.PICTURE_TAKEN_FROM_CAMERA) {
				return handleResultFromCamera(activity, data, outFile);
			} else if (requestCode == NativeCameraGalleyUtil.PICTURE_TAKEN_FROM_GALLERY) {
				return handleResultFromChooser(activity, data);
			}
		}

		return null;
		  
	}
	
	//AUXILIAR
	public static BitmapFilePath handleResultFromChooser(Activity activity, Intent data){
	    Bitmap takenPictureData = null;
	    String filePath = null;
	    
	    Uri photoUri = data.getData();
	    if (photoUri != null){
	        try {
	            String[] filePathColumn = {MediaStore.Images.Media.DATA};
	            Cursor cursor = activity.getContentResolver().query(photoUri, filePathColumn, null, null, null); 
	            cursor.moveToFirst();
	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            filePath = cursor.getString(columnIndex);
	            cursor.close();

	            takenPictureData = BitmapUtil.media_getBitmapFromFile(new File(filePath), 160);

	        } catch(Exception e) {
	            Toast.makeText(activity, "Error getting selected image.", Toast.LENGTH_SHORT).show();
	        }
	    }

	    if (takenPictureData != null) {
	    	return new BitmapFilePath(takenPictureData, filePath);
	    }
	    return null;
	}

	public static BitmapFilePath handleResultFromCamera(Activity activity, Intent data, File outFile) {
	    Bitmap takenPictureData = null;
	    String filePath = null;
	    
	    if(data != null) {
	        Bundle extras = data.getExtras();
	        if (extras != null && extras.get("data") != null) {
	            takenPictureData = (Bitmap) extras.get("data");
	        }
	    } else {
	        try{
	            takenPictureData = BitmapUtil.media_getBitmapFromFile(outFile, 160);
	            takenPictureData = BitmapUtil.media_correctImageOrientation(outFile.getAbsolutePath());
	            filePath = outFile.getAbsolutePath();
	        } catch(Exception e) {
	            Toast.makeText(activity, "Error getting saved taken picture.", Toast.LENGTH_SHORT).show();;
	        }	        
	    }
	    if (takenPictureData != null) {
	    	return new BitmapFilePath(takenPictureData, filePath);
	    }
	    return null;
	}
}
