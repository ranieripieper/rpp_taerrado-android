package com.doisdoissete.android.util.ddsutil.service.asynctask;

public interface ParamServiceInterface {

	String getParamService();
	String getValue();
}
