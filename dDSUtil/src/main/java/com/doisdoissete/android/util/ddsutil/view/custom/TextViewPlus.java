package com.doisdoissete.android.util.ddsutil.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Layout.Alignment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.R;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;

import java.util.ArrayList;
import java.util.List;

public class TextViewPlus extends TextView {
    private static final String TAG = "TextViewPlus";

	
    // Minimum text size for this text view
    public static final float MIN_TEXT_SIZE = 12;

    private List<TextViewPlus> sameSize = null;

    // Interface for resize notifications
    public interface OnTextResizeListener {
        public void onTextResize(TextView textView, float oldSize, float newSize);
    }

    // Our ellipse string
    private static final String mEllipsis = "...";

    // Registered resize listener
    private OnTextResizeListener mTextResizeListener;

    // Flag for text and/or size changes to force a resize
    private boolean mNeedsResize = false;
    
    private boolean mResizeText = false;
    
    //Disable Touch
    private boolean mDisableTouch = false;

    // Text size that is set from code. This acts as a starting point for resizing
    private float mTextSize;

    // Temporary upper bounds on the starting text size
    private float mMaxTextSize = 0;

    // Lower bounds for text size
    private float mMinTextSize = MIN_TEXT_SIZE;

    // Text view line spacing multiplier
    private float mSpacingMult = 1.0f;

    // Text view additional line spacing
    private float mSpacingAdd = 0.0f;

    // Add ellipsis to text that overflows at the smallest text size
    private boolean mAddEllipsis = true;
    
    private boolean underline = false;
    
    public TextViewPlus(Context context) {
        super(context);
        mTextSize = getTextSize();
        if (underline) {
       	 setPaintFlags(getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
       }
    }

    public TextViewPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        loadAttributes(context, attrs);
        mTextSize = getTextSize();
        if (underline) {
       	 setPaintFlags(getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
       }
    }

    public TextViewPlus(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        loadAttributes(context, attrs);
        mTextSize = getTextSize();
        if (underline) {
        	 setPaintFlags(getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
       
    }

    private void loadAttributes(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewPlus);
        
        //underline
        underline = a.getBoolean(R.styleable.TextViewPlus_underline, underline);
        
        //fonte
        TypedArray defaultAttr = ctx.obtainStyledAttributes(attrs, R.styleable.DDSDefaultAttributes);
        String customFont = defaultAttr.getString(R.styleable.DDSDefaultAttributes_text_font);
        mDisableTouch  = defaultAttr.getBoolean(R.styleable.DDSDefaultAttributes_disable_touch, false);
        defaultAttr.recycle();
        
        FontUtilCache.setCustomFont(ctx, this, customFont);       
        a.recycle();
    }
    
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (mDisableTouch) {
			return false;
		}
		return super.onTouchEvent(event);
	}
	
    /**
     * When text changes, set the force resize flag to true and reset the text size.
     */
    @Override
    protected void onTextChanged(final CharSequence text, final int start, final int before, final int after) {
    	super.onTextChanged(text, start, before, after);
        mNeedsResize = true;
        // Since this view may be reused, it is good to reset the text size
        resetTextSize();
    }

    /**
     * If the text view size changed, set the force resize flag to true
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (w != oldw || h != oldh) {
            mNeedsResize = true;
        }
    }

    /**
     * Register listener to receive resize notifications
     * @param listener
     */
    public void setOnResizeListener(OnTextResizeListener listener) {
        mTextResizeListener = listener;
    }

    /**
     * Override the set text size to update our internal reference values
     */
    @Override
    public void setTextSize(float size) {
        super.setTextSize(size);
        mTextSize = getTextSize();
    }

    /**
     * Override the set text size to update our internal reference values
     */
    @Override
    public void setTextSize(int unit, float size) {
        super.setTextSize(unit, size);
        mTextSize = getTextSize();
    }
    
    

    /**
     * Override the set line spacing to update our internal reference values
     */
    @Override
    public void setLineSpacing(float add, float mult) {
        super.setLineSpacing(add, mult);
        mSpacingMult = mult;
        mSpacingAdd = add;
    }

    /**
     * Set the upper text size limit and invalidate the view
     * @param maxTextSize
     */
    public void setMaxTextSize(float maxTextSize) {
        mMaxTextSize = maxTextSize;
        requestLayout();
        invalidate();
    }

    /**
     * Return upper text size limit
     * @return
     */
    public float getMaxTextSize() {
        return mMaxTextSize;
    }

    /**
     * Set the lower text size limit and invalidate the view
     * @param minTextSize
     */
    public void setMinTextSize(float minTextSize) {
        mMinTextSize = minTextSize;
        requestLayout();
        invalidate();
    }

    /**
     * Return lower text size limit
     * @return
     */
    public float getMinTextSize() {
        return mMinTextSize;
    }

    /**
     * Set flag to add ellipsis to text that overflows at the smallest text size
     * @param addEllipsis
     */
    public void setAddEllipsis(boolean addEllipsis) {
        mAddEllipsis = addEllipsis;
    }

    /**
     * Return flag to add ellipsis to text that overflows at the smallest text size
     * @return
     */
    public boolean getAddEllipsis() {
        return mAddEllipsis;
    }

    /**
     * Reset the text to the original size
     */
    public void resetTextSize() {
        if(mTextSize > 0) {
            super.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
            mMaxTextSize = mTextSize;
        }
    }

    /**
     * Resize text after measuring
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if(mResizeText && (changed || mNeedsResize)) {
            int widthLimit = (right - left) - getCompoundPaddingLeft() - getCompoundPaddingRight();
            int heightLimit = (bottom - top) - getCompoundPaddingBottom() - getCompoundPaddingTop();
            resizeText(widthLimit, heightLimit);
        }
        super.onLayout(changed, left, top, right, bottom);
    }


    /**
     * Resize the text size with default width and height
     */
    public void resizeText() {
        int heightLimit = getHeight() - getPaddingBottom() - getPaddingTop();
        int widthLimit = getWidth() - getPaddingLeft() - getPaddingRight();
        resizeText(widthLimit, heightLimit);
    }

    /**
     * Resize the text size with specified width and height
     * @param width
     * @param height
     */
    public void resizeText(int width, int height) {
        CharSequence text = getText();
        // Do not resize if the view does not have dimensions or there is no text
        if(text == null || text.length() == 0 || height <= 0 || width <= 0 || mTextSize == 0) {
            return;
        }

        // Get the text view's paint object
        TextPaint textPaint = getPaint();

        // Store the current text size
        float oldTextSize = textPaint.getTextSize();
        // If there is a max text size set, use the lesser of that and the default text size
        float targetTextSize = mMaxTextSize > 0 ? Math.min(mTextSize, mMaxTextSize) : mTextSize;

        // Get the required text height
        int textHeight = getTextHeight(text, textPaint, width, targetTextSize);

        // Until we either fit within our text view or we had reached our min text size, incrementally try smaller sizes
        while(textHeight > height && targetTextSize > mMinTextSize) {
            targetTextSize = Math.max(targetTextSize - 2, mMinTextSize);
            textHeight = getTextHeight(text, textPaint, width, targetTextSize);
        }

        // If we had reached our minimum text size and still don't fit, append an ellipsis
        if(mAddEllipsis && targetTextSize == mMinTextSize && textHeight > height) {
            // Draw using a static layout
            StaticLayout layout = new StaticLayout(text, textPaint, width, Alignment.ALIGN_NORMAL, mSpacingMult, mSpacingAdd, false);
            // Check that we have a least one line of rendered text
            if(layout.getLineCount() > 0) {
                // Since the line at the specific vertical position would be cut off,
                // we must trim up to the previous line
                int lastLine = layout.getLineForVertical(height) - 1;
                // If the text would not even fit on a single line, clear it
                if(lastLine < 0) {
                    setText("");
                }
                // Otherwise, trim to the previous line and add an ellipsis
                else {
                    int start = layout.getLineStart(lastLine);
                    int end = layout.getLineEnd(lastLine);
                    float lineWidth = layout.getLineWidth(lastLine);
                    float ellipseWidth = textPaint.measureText(mEllipsis);

                    // Trim characters off until we have enough room to draw the ellipsis
                    while(width < lineWidth + ellipseWidth) {
                        lineWidth = textPaint.measureText(text.subSequence(start, --end + 1).toString());
                    }
                    setText(text.subSequence(0, end) + mEllipsis);
                }
            }
        }

        // Some devices try to auto adjust line spacing, so force default line spacing
        // and invalidate the layout as a side effect
        textPaint.setTextSize(targetTextSize);
        if (sameSize != null) {
        	for (TextViewPlus txt : sameSize) {
            	txt.setTextSize(TypedValue.COMPLEX_UNIT_PX, targetTextSize);
            }
        }
        
        setLineSpacing(mSpacingAdd, mSpacingMult);

        // Notify the listener if registered
        if(mTextResizeListener != null) {
            mTextResizeListener.onTextResize(this, oldTextSize, targetTextSize);
        }

        // Reset force resize flag
        mNeedsResize = false;
    }

    // Set the text size of the text paint object and use a static layout to render text off screen before measuring
    private int getTextHeight(CharSequence source, TextPaint paint, int width, float textSize) {
        // Update the text paint object
        paint.setTextSize(textSize);
        // Measure using a static layout
        StaticLayout layout = new StaticLayout(source, paint, width, Alignment.ALIGN_NORMAL, mSpacingMult, mSpacingAdd, true);
        return layout.getHeight();
    }

    public void addSameSize(TextViewPlus txt) {
    	if (sameSize == null) {
    		sameSize = new ArrayList<TextViewPlus>();
    	}
    	sameSize.add(txt);
    }
    
    public void setText(String str, boolean hashtag, TagClick tagClick) {
    	setMovementMethod(LinkMovementMethod.getInstance());
    	setText(addClickablePart(str, tagClick), BufferType.SPANNABLE);
    }
    
	public void setText(String str) {
		if (TextUtils.isEmpty(str)) {
			super.setText("");
		} else {
			super.setText(str);
		}
	}
	
	public interface TagClick {
		public void clickedTag(String tag);
	}
	
	 //hashtag

		private final static String mHashTagColor = "#5BCFF2";
		private final static boolean mHypeLinkEnabled = false;
		public static SpannableStringBuilder addClickablePart(String nTagString, TagClick TagClick) {
			return addClickablePart(nTagString, TagClick, mHypeLinkEnabled, mHashTagColor);
		}
		
		public static SpannableStringBuilder addClickablePart(final String nTagString,
				final TagClick tagClick, final boolean hypeLinkEnabled, final String hashTagColor) {

			SpannableStringBuilder string = new SpannableStringBuilder(nTagString);

			int start = -1;
			for (int i = 0; i < nTagString.length(); i++) {
				if (nTagString.charAt(i) == '#') {
					start = i;
				} else if (nTagString.charAt(i) == ' ' || nTagString.charAt(i) == ','
	 					|| (i == nTagString.length() - 1 && start != -1)) {
					if (start != -1) {
						if (i == nTagString.length() - 1) {
							i++; // case for if hash is last word and there is no
									// space after word
						}

						final String tag = nTagString.substring(start, i);
						string.setSpan(new ClickableSpan() {

							@Override
							public void onClick(View widget) {
								
								// Click on each tag will get here
								
								Log.d("TAg--HAsh", String.format("Clicked", tag));
								tagClick.clickedTag(tag);
							}

							@Override
							public void updateDrawState(TextPaint ds) {
								
								// color for the hash tag
								ds.setColor(Color.parseColor(hashTagColor));
								ds.setUnderlineText(hypeLinkEnabled);
							}
						}, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						start = -1;
					}
				}
			}

			return string;
		}
}