package com.doisdoissete.android.util.ddsutil;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Util {

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
	public static boolean checkPlayServices(Context ctx) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(ctx);
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }
    
	/**
	 * This function allows to know if there is an application that responds to
	 * the specified action.
	 * 
	 * @param context
	 * @param action	Action that requires an application.
	 * @return
	 */
	public static boolean system_isIntentAvailable(Context context, String action) {
	    final PackageManager packageManager = context.getPackageManager();
	    final Intent intent = new Intent(action);
	    List<ResolveInfo> list =
	            packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
	    return list.size() > 0;
	}
	
	
    private static final ThreadLocal<DateFormat> df1 = new ThreadLocal<DateFormat>() {
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ") {
		        public StringBuffer format(Date date, StringBuffer toAppendTo, java.text.FieldPosition pos) {
		            StringBuffer toFix = super.format(date, toAppendTo, pos);
		            return toFix.insert(toFix.length()-2, ':');
		        };
		        
		        public Date parse(String text, ParsePosition pos) {
		        	int indexOf = text.indexOf(':', text.length() - 4);
		        	if (indexOf > 0) {
		        		text = text.substring(0, indexOf) + text.substring(indexOf+1, text.length());
		            	return super.parse(text, pos);
		        	} else {
		        		return null;
		        	}
		        	
		        }
		        
		    };
		}
	};
	
	private static final ThreadLocal<DateFormat> df2 = new ThreadLocal<DateFormat>() {
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		}
	};
    
	public static Gson getGson() {
		return getGson(null, null, null);
	}
	
	
	
	public static Gson getGson(String dtFormat, JsonDeserializer jsonDeserializer, TypeToken targetType) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder
				.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
		gsonBuilder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
		gsonBuilder.serializeNulls();
		gsonBuilder.registerTypeAdapter(Date.class, deser);
		gsonBuilder.registerTypeAdapter(Date.class, ser);
		if (jsonDeserializer != null) {
			gsonBuilder.registerTypeAdapter(targetType.getType(), jsonDeserializer);
		}
		

		Gson gson = gsonBuilder.create();
		return gson;
	}
	
	static JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		  @Override
		  public Date deserialize(JsonElement json, Type typeOfT,
		       JsonDeserializationContext context) throws JsonParseException {
			  if (json != null) {
				  try {
					Date dt = df1.get().parse(json.getAsString());
					if (dt == null) {
						return tryOtherFormat(json);
					}
					return dt;
				} catch (ParseException e) {
					return tryOtherFormat(json);
				}
			  }
		    
			  return null;
		  }
		  
		  private Date tryOtherFormat(JsonElement json) {
			  try {
					return df2.get().parse(json.getAsString());
				} catch (ParseException e1) {
				}
			  return null;
		  }
		};
		
		
	static JsonSerializer<Date> ser = new JsonSerializer<Date>() {
		  @Override
		  public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext 
		             context) {
		    return src == null ? null : new JsonPrimitive(src.getTime());
		  }
		};

	public static void openBrowser(Context ctx, String site) {
		
		String newUrl = site;
    	if (newUrl.indexOf("http") < 0) {
    		newUrl = "http://" + newUrl;
    	}
    	
		Uri uri = Uri.parse(newUrl);
		Intent it = new Intent(Intent.ACTION_VIEW, uri);
		ctx.startActivity(it);
	}
	
	public static void sendMail(Context ctx, String from, String subject, String body) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL  , new String[]{from});
		i.putExtra(Intent.EXTRA_SUBJECT, subject);
		i.putExtra(Intent.EXTRA_TEXT   , body);
		try {
		    ctx.startActivity(Intent.createChooser(i, "Send mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
		    
		}
	}
	
	public static void navigateTo(Context ctx, double lng, double lat, String name) {
		
		String uri = String.format("geo:%f,%f?q=%f,%f(%s)", lat, lng, lat, lng, name);
		
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
		ctx.startActivity(intent);
	}

	public static void viewOnMap(Context ctx, String address) {
		Intent intent = new Intent(Intent.ACTION_VIEW,
	                      Uri.parse(String.format("geo:0,0?q=%s",
	                                              URLEncoder.encode(address))));
	    ctx.startActivity(intent);
	}

	public static void viewOnMap(Context ctx, String lat, String lng) {
		Intent intent = new Intent(Intent.ACTION_VIEW,
	                      Uri.parse(String.format("geo:%s,%s", lat, lng)));
	    ctx.startActivity(intent);
	}
	
	public static void callTo(Context ctx, String number) {
		Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
		ctx.startActivity(intent);
	}
	
	public static void openMap(Context ctx, double latitude, double longitude) {
		String uri = String.format("geo:%f,%f", latitude, longitude);
		Intent intent = new Intent(Intent.ACTION_VIEW);
	    intent.setData(Uri.parse(uri));
	    ctx.startActivity(intent);
	    
		
		/*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
		ctx.startActivity(intent);*/
	}
	
	public static String getString(String str) {
		if (str != null) {
			return str.trim();
		}
		return "";
	}
}
