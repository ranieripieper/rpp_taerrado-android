package com.doisdoissete.android.util.ddsutil.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SaveFileService extends BaseService {

	public static String PARAM_DATA = "PARAM_DATA";
	public static String PARAM_DATA_ROTATION = "PARAM_DATA_ROTATION";
	public static String PARAM_PREFIX = "PARAM_PREFIX";
	
	public static String PARAM_BITMAP = "PARAM_BITMAP";
	private int MAX_HEIGHT = 700;
	private int MAX_WIDTH = 700;
	
	private String mPrefix = "";
	private String mBaseDir = "";
	
	public SaveFileService(ServiceBuilder builder, String baseDir) {
		super(builder);
		this.mBaseDir = baseDir;
	}	
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException {
		
		byte[] data= null;
		Bitmap bitmap = null;
		if (params.get(PARAM_PREFIX) != null) {
			mPrefix = params.get(PARAM_PREFIX).get(0).toString();
		}
		
		if (params.get(PARAM_DATA) != null) {
			data = (byte[]) params.get(PARAM_DATA).get(0);
			int rotation = Integer.valueOf(params.get(PARAM_DATA_ROTATION).get(0).toString());
			return saveData(data, rotation);
		} else if (params.get(PARAM_BITMAP) != null) {
			bitmap = (Bitmap)params.get(PARAM_BITMAP).get(0);
			return saveBitmap(bitmap);
		}
		
		return null;

	}
	
	private File saveBitmap(Bitmap bitmap) {

		File file = getOutputMediaFile(mPrefix, mServiceBuilder.getmContext());
		BitmapUtil.copyJpg(getResizedBitmap(bitmap, MAX_HEIGHT, MAX_WIDTH, ExifInterface.ORIENTATION_NORMAL), file);
		return file;
	}
	
	private File saveData(byte[] data, int rotation) {
		File result = null;
		try {
			//make a new picture file
			File pictureFile = getOutputMediaFile(mPrefix, mServiceBuilder.getmContext());
			
			if (pictureFile == null) {
				return result;
			}
			
			//write the file
			FileOutputStream fos = new FileOutputStream(pictureFile);
			fos.write(data);
			fos.close();
			
			Bitmap bitmap = BitmapUtil.decodeSampledBitmapFromByte(mServiceBuilder.getmContext(), data, MAX_WIDTH, MAX_HEIGHT);
					//BitmapFactory.decodeByteArray(data, 0, data.length);//BitmapFactory.decodeFile(pictureFile.getAbsolutePath(),bmOptions);
			File newFile = getOutputMediaFile("", mServiceBuilder.getmContext());

			 Matrix matrix = new Matrix();
			 
			if (rotation != 0) {
				matrix.postRotate(rotation);
			}
			
            Bitmap oldBitmap = bitmap;
            int initW = 0;
            int initH = 0;
            int endW = oldBitmap.getWidth();
            int endH = oldBitmap.getHeight();
            if (endW > endH) {
            	int tmp = endW - endH;
            	initW = tmp / 2;
            	endW = endH;
            } else if (endW < endH) {
            	int tmp = endH - endW;
            	initH = tmp / 2;
            	endH = endW;
            }
            bitmap = Bitmap.createBitmap(oldBitmap, initW, initH, endW, endH, matrix, true);

            oldBitmap.recycle();
	        
	        
			BitmapUtil.copyJpg(getResizedBitmap(bitmap, MAX_HEIGHT, MAX_WIDTH, ExifInterface.ORIENTATION_NORMAL), newFile);
			result = newFile;
			bitmap.recycle();

		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} catch (Exception e) {
		}
		return result;
	}
	
	//make picture and save to a folder
	private File getOutputMediaFile(String prefix, Context ctx) {
		return getOutputMediaFile(ctx, mBaseDir, prefix);
	}
		
	//make picture and save to a folder
	public static File getOutputMediaFile(Context ctx, String baseDir, String prefix) {
		//make a new file directory inside the "sdcard" folder
		File mediaStorageDir = new File(ctx.getFilesDir().getAbsolutePath(), baseDir);
		
		//if this "JCGCamera folder does not exist
		if (!mediaStorageDir.exists()) {
			//if you cannot make this folder return
			if (!mediaStorageDir.mkdirs()) {
				return null;
			}
		}
		
		//take the current timeStamp
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		File mediaFile;
		//and make a media file:
		mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + prefix + timeStamp + ".jpg");
		
		return mediaFile;
	}
	
	private Bitmap getResizedBitmap(Bitmap bm, int h, int w, int orientation) {
	    int width = bm.getWidth();
	    int height = bm.getHeight();
	    float scaleWidth = ((float) h) / width;
	    float scaleHeight = ((float) w) / height;
	    // CREATE A MATRIX FOR THE MANIPULATION
	    Matrix matrix = new Matrix();
	    Matrix matrixRotate = BitmapUtil.getMatrixRotate(matrix, orientation);
	    if (matrixRotate != null) {
	    	matrix = matrixRotate;
	    }
	    // RESIZE THE BIT MAP
	    if (width <= w) {
	    	return bm;
	    }
	    
	    float scale = Math.max(scaleWidth, scaleHeight);
	    
	    matrix.postScale(scale, scale);

	    // "RECREATE" THE NEW BITMAP
	    Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
	    
	    return resizedBitmap;
	}
}
