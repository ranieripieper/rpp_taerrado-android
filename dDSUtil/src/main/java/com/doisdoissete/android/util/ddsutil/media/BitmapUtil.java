package com.doisdoissete.android.util.ddsutil.media;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.ViewConfiguration;
import android.view.WindowManager;

import com.doisdoissete.android.util.ddsutil.R;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class BitmapUtil {

    /**
     * Enables or disables log
     */
    public static boolean LOG_ENABLE = false;
    private static final String TAG = BitmapUtil.class.toString();
    // Media Related -----------------------------------------------------------------------------------------------------------------------------

    public static void copyJpg(Bitmap bmp, File dst) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(dst);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap drawTextToBitmap(Context gContext,
                                   Bitmap bitmap,
                                   String gText) {
        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if (bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.WHITE);
        // text size in pixels
        paint.setTextSize((int) (7 * scale));
        // text shadow

        // draw text to the Canvas center
        Rect bounds = new Rect();
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        paint.setTypeface(FontUtilCache.get("fonts/HelveticaNeue-Light", gContext));
        Paint paintLineText = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintLineText.setColor(gContext.getResources().getColor(R.color.transparent_dark_mask));

        //canvas.drawText(gText, bounds.height() / 2, bounds.height() + bounds.height() / 2, paint);

        TextPaint tp = new TextPaint();
        tp.setColor(Color.WHITE);
        tp.setTextSize((int) (7 * scale));
        tp.setAntiAlias(true);

        StaticLayout sl = new StaticLayout(gText, tp,
                bitmap.getWidth(), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        canvas.drawRect(0, 0, bitmap.getWidth(), sl.getHeight(), paintLineText);
        sl.draw(canvas);
        return bitmap;
    }


    public static void copy(Bitmap bmp, File dst) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(dst);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Decode and sample down a bitmap from a byte stream
     */
    public static Bitmap decodeSampledBitmapFromByte(Context context, byte[] bitmapBytes, int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inMutable = true;
        options.inBitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false; // If set to true, the decoder will return null (no bitmap), but the out... fields will still be set, allowing the caller to query the bitmap without having to allocate the memory for its pixels.
        options.inPurgeable = true;         // Tell to gc that whether it needs free memory, the Bitmap can be cleared
        options.inInputShareable = true;    // Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future

        return BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length, options);
    }

    /**
     * Calculate an inSampleSize for use in a {@link android.graphics.BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link android.graphics.BitmapFactory}. This implementation calculates
     * the closest inSampleSize that is a power of 2 and will result in the final decoded bitmap
     * having a width and height equal to or larger than the requested width and height
     * <p/>
     * The function rounds up the sample size to a power of 2 or multiple
     * of 8 because BitmapFactory only honors sample size this way.
     * For example, BitmapFactory downsamples an image by 2 even though the
     * request is 3. So we round up the sample size to avoid OOM.
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int initialInSampleSize = 1;
        if (options.outHeight > reqHeight || options.outWidth > reqWidth) {
            if (options.outWidth > options.outHeight) {
                initialInSampleSize = Math.round((float) options.outHeight / (float) reqHeight) - 1;
            } else {
                initialInSampleSize = Math.round((float) options.outWidth / (float) reqWidth) - 1;
            }
        }

        //int initialInSampleSize = computeInitialSampleSize(options, reqWidth, reqHeight);

        int roundedInSampleSize;
        if (initialInSampleSize <= 8) {
            roundedInSampleSize = 1;
            while (roundedInSampleSize < initialInSampleSize) {
                // Shift one bit to left
                roundedInSampleSize <<= 1;
            }
        } else {
            roundedInSampleSize = (initialInSampleSize + 7) / 8 * 8;
        }


        return roundedInSampleSize;
    }

    private static int computeInitialSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final double height = options.outHeight;
        final double width = options.outWidth;

        final long maxNumOfPixels = reqWidth * reqHeight;
        final int minSideLength = Math.min(reqHeight, reqWidth);

        int lowerBound = (maxNumOfPixels < 0) ? 1 :
                (int) Math.ceil(Math.sqrt(width * height / maxNumOfPixels));
        int upperBound = (minSideLength < 0) ? 128 :
                (int) Math.min(Math.floor(width / minSideLength),
                        Math.floor(height / minSideLength));

        if (upperBound < lowerBound) {
            // return the larger one when there is no overlapping zone.
            return lowerBound;
        }

        if (maxNumOfPixels < 0 && minSideLength < 0) {
            return 1;
        } else if (minSideLength < 0) {
            return lowerBound;
        } else {
            return upperBound;
        }
    }

    /**
     * Corrects the orientation of a Bitmap. Orientation, depending of the device
     * , is not correctly set in the EXIF data of the taken image when it is saved
     * into disk.
     * <p/>
     * Explanation:
     * Camera orientation is not working ok (as is when capturing an image) because
     * OEMs do not adhere to the standard. So, each company does this following their
     * own way.
     *
     * @param imagePath path to the file
     * @return
     */
    public static Bitmap media_correctImageOrientation(String imagePath) {
        Bitmap res = null;

        try {
            File f = new File(imagePath);
            ExifInterface exif = new ExifInterface(f.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            res = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                    bmp.getHeight(), mat, true);

        } catch (OutOfMemoryError e) {
            if (LOG_ENABLE)
                Log.e(TAG, "media_correctImageOrientation() [OutOfMemory!]: " + e.getMessage(), e);
        } catch (Exception e) {
            if (LOG_ENABLE)
                Log.e(TAG, "media_correctImageOrientation(): " + e.getMessage(), e);
        } catch (Throwable e) {
            if (LOG_ENABLE)
                Log.e(TAG, "media_correctImageOrientation(): " + e.getMessage(), e);
        }


        return res;
    }

    /**
     * Decodes the specified image from the storage, scales it to reduce memory consumption
     * and returning as a Bitmap object.
     *
     * @param imgFile File to get as a Bitmap object.
     * @return
     */
    public static Bitmap media_getBitmapFromFile(File imgFile) throws Exception {
        return media_getBitmapFromFile(imgFile, 70);
    }

    /**
     * Decodes the specified image from the storage, scales it to reduce memory consumption
     * and returning as a Bitmap object.
     *
     * @param imgFile File to get as a Bitmap object.
     * @return
     */
    public static Bitmap media_getBitmapFromFile(File imgFile, int requiredSize) throws Exception {
        Bitmap res = null;

        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(imgFile), null, o);

            //orienta�ao
            ExifInterface exif = new ExifInterface(imgFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = requiredSize;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            res = BitmapFactory.decodeStream(new FileInputStream(imgFile), null, o2);

            Bitmap bmRotated = rotateBitmap(res, orientation);
            return bmRotated;

        } catch (FileNotFoundException e) {
            if (LOG_ENABLE)
                Log.e(TAG, "media_generateBitmapFromFile() - FileNotFoundException: " + e.getMessage(), e);

            throw new Exception(TAG + "[media_generateBitmapFromFile() - FileNotFoundException]: " + e.getMessage(), e);
        } catch (Exception e) {
            if (LOG_ENABLE)
                Log.e(TAG, "media_generateBitmapFromFile(): " + e.getMessage(), e);

            throw new Exception(TAG + "[media_generateBitmapFromFile()]: " + e.getMessage(), e);
        }
    }

    public static Bitmap rotateBitmap(String path, Bitmap bitmap) {
        //orienta�ao
        ExifInterface exif;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            return bitmap;
        }
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

        return rotateBitmap(bitmap, orientation);
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {

        Matrix matrix = new Matrix();
        matrix = getMatrixRotate(matrix, orientation);
        if (matrix == null) {
            return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Matrix getMatrixRotate(Matrix matrix, int orientation) {
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return null;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return null;

        }
        return matrix;
    }

    /**
     * Grabs an image direct from a URL into a Drawable without saving a cache
     *
     * @param urlImage
     * @param src_name
     * @return
     * @throws Exception
     */
    public static Drawable media_getDrawableFromNet(String urlImage, String src_name) throws Exception {
        Drawable res = null;

        try {
            InputStream is = ((InputStream) new URL(urlImage).getContent());
            res = Drawable.createFromStream(is, src_name);
            is.close();
        } catch (MalformedURLException e) {
            if (LOG_ENABLE)
                Log.e(TAG, "media_generateBitmapFromFile() - MalformedURLException: " + e.getMessage(), e);

            throw new Exception(TAG + "[media_generateBitmapFromFile() - MalformedURLException]: " + e.getMessage(), e);
        } catch (IOException e) {
            if (LOG_ENABLE)
                Log.e(TAG, "media_generateBitmapFromFile() - IOException: " + e.getMessage(), e);

            throw new Exception(TAG + "[media_generateBitmapFromFile() - IOException]: " + e.getMessage(), e);
        } catch (Exception e) {
            if (LOG_ENABLE)
                Log.e(TAG, "media_generateBitmapFromFile(): " + e.getMessage(), e);

            throw new Exception(TAG + "[media_generateBitmapFromFile()]: " + e.getMessage(), e);
        }

        return res;
    }

    /**
     * Grabs an image direct from a file into a Drawable without saving a cache
     *
     * @param urlImage
     * @return
     * @throws Exception
     */
    public static Drawable media_getDrawableFromFile(String filePath) throws Exception {
        Drawable res = null;

        try {
            res = Drawable.createFromPath(filePath);
        } catch (Exception e) {
            if (LOG_ENABLE)
                Log.e(TAG, "media_getDrawableFromFile(): " + e.getMessage(), e);

            throw new Exception(TAG + "[media_getDrawableFromFile()]: " + e.getMessage(), e);
        }

        return res;
    }

    /**
     * Loads a Bitmap image form the internal storage.
     *
     * @param context
     * @param fileName
     * @return
     * @throws Exception
     */
    public static Bitmap media_loadBitmapFromInternalStorage(Context context, String fileName) throws Exception {

        try {
            FileInputStream is = context.openFileInput(fileName);
            Bitmap b = BitmapFactory.decodeStream(is);

            return b;
        } catch (Exception e) {
            throw new Exception("Error reading data '" + fileName + "' (internal storage) : " + e.getMessage(), e);
        }
    }


    /**
     * Makes rounded corners on a bitmap.
     *
     * Requires Level 17 of Android API!!
     *
     * @param bitmap
     * @return
     */
     /*public static Bitmap media_getRoundedCornerBitmap(Bitmap bitmap) {
			Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
					bitmap.getHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(output);

			final int color = 0xff424242;
			final Paint paint = new Paint();
			final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
			final RectF rectF = new RectF(rect);
			final float roundPx = 8;

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
			canvas.drawBitmap(bitmap, rect, rect, paint);

			return output;
		}*/

    /**
     * Convers a Bitmap to grayscale.
     *
     * @param bmpOriginal
     * @return
     */
    public static Bitmap media_getGrayScale(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height,
                Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }


    /**
     * Plays the specified ringtone from the application raw folder.
     *
     * @param appPackage      Application package. (you can get it by using:
     *                        AppVigilant.thiz.getApplicationContext().getPackageName())
     * @param soundResourceId Sound resource id
     */
    public static void media_soundPlayFromRawFolder(Context context, String appPackage, int soundResourceId) {
        try {
            Uri soundUri = Uri.parse("android.resource://" + appPackage + "/" + soundResourceId);
            Ringtone r = RingtoneManager.getRingtone(context, soundUri);
            r.play();

        } catch (Exception e) {
            if (LOG_ENABLE) {
                Log.e(TAG, "Error playing sound with resource id: '" + soundResourceId + "' (" + e.getMessage() + ")", e);
            }
        }
    }

    /**
     * Plays the default system notification ringtone.
     *
     * @param context
     */
    public static void media_soundPlayNotificationDefault(Context context) {
        try {
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, soundUri);
            r.play();

        } catch (Exception e) {
            if (LOG_ENABLE) {
                Log.e(TAG, "Error playing default notification sound (" + e.getMessage() + ")", e);
            }
        }
    }

    /**
     * Plays the specified sound from the application asset folder.
     *
     * @param context
     * @param assetSoundPath Path to the sound in the assets folder.
     */
    public static void media_soundPlayFromAssetFolder(Context context, String assetSoundPath) {
        try {
            AssetFileDescriptor afd = context.getAssets().openFd(assetSoundPath);

            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();

        } catch (Exception e) {
            if (LOG_ENABLE) {
                Log.e(TAG, "Error playing sound: '" + assetSoundPath + "' (" + e.getMessage() + ")", e);
            }
        }
    }

    // Device Related -----------------------------------------------------------------------------------------------------------------------------

    /**
     * Get the device Id (an Hexadecimal unique value of 64 bit)
     *
     * @param context
     * @return
     */
    public static String device_getId(Context context) {
        return Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);
    }

    /**
     * Gets the device screen size in pixels.
     *
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static Point device_screenSize(Context context) {
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        final Point size = new Point();
        try {
            display.getSize(size);
        } catch (java.lang.NoSuchMethodError ignore) { // Older device
            size.x = display.getWidth();
            size.y = display.getHeight();
        }

        return size;
    }


    public static DisplayMetrics device_screenMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        display.getMetrics(metrics);
        //display.getRealMetrics(metrics);

        return metrics;
    }

    /**
     * Get the device language (two letter code value)
     *
     * @return
     */
    public static String device_getLanguage() {
        return Locale.getDefault().getLanguage();
    }


    /**
     * Get the current android API Level.
     * <p/>
     * Android 4.2        17
     * Android 4.1        16
     * Android 4.0.3      15
     * Android 4.0        14
     * Android 3.2        13
     * Android 3.1        12
     * Android 3.0        11
     * Android 2.3.3      10
     * Android 2.3        9
     * Android 2.2        8
     * Android 2.1        7
     * Android 2.0.1      6
     * Android 2.0        5
     * Android 1.6        4
     * Android 1.5        3
     * Android 1.1        2
     * Android 1.0        1
     *
     * @return
     */
    public static int device_getAPILevel() {
        return android.os.Build.VERSION.SDK_INT;
    }

    /**
     * Gets the device number if is available.
     *
     * @param ctx
     * @return
     */
    public static String device_getDeviceMobileNumber(Context ctx) {
        TelephonyManager tm = (TelephonyManager) ctx
                .getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getLine1Number();
    }

    public static String device_getIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    public static String device_getOSVersion() {
        return String.valueOf(Build.VERSION.SDK_INT);
    }

    /**
     * Returns TRUE if a specified hardware feature is present.
     * <p/>
     * Use PackageManager.FEATURE_[feature] to specify the feature
     * to query.
     *
     * @param context
     * @param hardwareFeature Use PackageManager.FEATURE_[feature] to specify the feature to query.
     * @return
     */
    public static boolean device_isHardwareFeatureAvailable(Context context, String hardwareFeature) {
        return context.getPackageManager().hasSystemFeature(hardwareFeature);
    }

    /**
     * Return TRUE if there is a hardware keyboard present.
     *
     * @param context
     * @return
     */
    public static boolean device_isHardwareKeyboard(Context context) {
        //You can also get some of the features which are not testable by the PackageManager via the Configuration, e.g. the DPAD.
        Configuration c = context.getResources().getConfiguration();
        if (c.keyboard != Configuration.KEYBOARD_NOKEYS) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return TRUE if there is a hardware DPAD navigation button.
     *
     * @param context
     * @return
     */
    public static boolean device_isHardwareDPAD(Context context) {
        //You can also get some of the features which are not testable by the PackageManager via the Configuration, e.g. the DPAD.
        Configuration c = context.getResources().getConfiguration();
        if (c.navigation == Configuration.NAVIGATION_DPAD) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return TRUE if there is a hardware keyboard and is being made hidden.
     *
     * @param context
     * @return
     */
    public static boolean device_isHardwareKeyboardHidden(Context context) {
        //You can also get some of the features which are not testable by the PackageManager via the Configuration, e.g. the DPAD.
        Configuration c = context.getResources().getConfiguration();
        if (c.hardKeyboardHidden != Configuration.HARDKEYBOARDHIDDEN_UNDEFINED &&
                c.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return TRUE if there is a hardware keyboard and is being made visible.
     *
     * @param context
     * @return
     */
    public static boolean device_isHardwareKeyboardVisible(Context context) {
        //You can also get some of the features which are not testable by the PackageManager via the Configuration, e.g. the DPAD.
        Configuration c = context.getResources().getConfiguration();
        if (c.hardKeyboardHidden != Configuration.HARDKEYBOARDHIDDEN_UNDEFINED &&
                c.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns true if the menu hardware menu is present on the device
     * <p/>
     * This function requires API Level 14.
     *
     * @param context
     * @return
     */
    @SuppressLint("NewApi")
    public static boolean device_isHardwareMenuButtonPresent(Context context) {
        if (device_getAPILevel() >= 14) {
            //This requires API Level 14
            return ViewConfiguration.get(context).hasPermanentMenuKey();
        } else {
            return false;
        }
    }


    /**
     * This function returns a File object pointing to a public folder of the the specified
     * type in the external drive.
     * <p/>
     * User media is saved here. Be carefull.
     * <p/>
     * Returns null if the media is not mounted.
     *
     * @param folderType   The type of files directory to return. Use one of the avaliable
     *                     Environment.DIRECTORY_ values.
     * @param folder       An specific folder in the public folder. May be null to get
     *                     the root of the public folder type.
     * @param createFolder Set to TRUE to create the folder if it does not exists.
     * @return
     */
    public static File storage_getExternalPublicFolder(String folderType, String folder, boolean createFolder) {
        File res = null;
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            res = new File(android.os.Environment.getExternalStoragePublicDirectory(folderType), folder);

            if (!res.exists()) {
                //We create the folder if is desired.
                if (folder != null && folder.length() > 0 && createFolder) {
                    if (!res.mkdir()) {
                        res = null;
                    }
                } else {
                    res = null;
                }
            }
        }
        return res;
    }


    /**
     * Return a temporal empty file.
     *
     * @param filePrefix
     * @param fileSuffix
     * @param outputDirectory
     * @return
     * @throws IOException
     */
    public static File storage_createUniqueFileName(String filePrefix, String fileSuffix, File outputDirectory) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String fileName = filePrefix + timeStamp;
        File filePath = File.createTempFile(fileName, fileSuffix, outputDirectory);
        if (filePath.exists()) {
            return filePath;
        } else {
            return null;
        }
    }
}
