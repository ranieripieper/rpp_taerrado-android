package com.doisdoissete.android.util.ddsutil.view.custom.flipdigit;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.R;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;

public class FlipDigitView extends LinearLayout {
	
	
	private Context mContext;
	private TextView txt1;
	private TextView txt2;
	private int nextNr = 1;
	private int nr = 0;
	private int nrIteracoes = 0;
	private int maxIteracoes = -1;
	private FlipDigitObserver observer;
	private int velocity = 300;
	private int textStyle = -1;
	private String font = null;
	private Boolean setValue = false;
	
	public FlipDigitView(Context context) {
		super(context);
		initData(context, null);
	}

	public FlipDigitView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initData(context, attrs);
	}

	public FlipDigitView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initData(context, attrs);
	}
	private void initData(Context context, AttributeSet attrs) {
		mContext = context;
		setAttributes(attrs);

		initializeView();

	}
	
	private void initializeView() {
		LayoutInflater inflater = LayoutInflater.from(mContext);

		View view = inflater.inflate(R.layout.flip_digit, null);
		
		txt1 = (TextView) view.findViewById(R.id.txt1);
		txt2 = (TextView) view.findViewById(R.id.txt2);

		if (textStyle > 0) {
			txt1.setTextAppearance(mContext, textStyle);
			txt2.setTextAppearance(mContext, textStyle);
		}
		if (!TextUtils.isEmpty(font)) {
			FontUtilCache.setCustomFont(mContext, txt1, font);
			FontUtilCache.setCustomFont(mContext, txt2, font);
		}
		
		this.addView(view);
	       
	        
	}
	
	private void setAttributes(AttributeSet attrs) {
		if (attrs != null) {
			TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.FlipDigitView);
			velocity = a.getInteger(R.styleable.FlipDigitView_velocity, 300);
			textStyle = a.getResourceId(R.styleable.FlipDigitView_text_style, -1);
			TypedArray defaultAttr = mContext.obtainStyledAttributes(attrs, R.styleable.DDSDefaultAttributes);
			font = defaultAttr.getString(R.styleable.DDSDefaultAttributes_text_font);
			defaultAttr.recycle();
			a.recycle();
		}
	}
	
    public void scrollDigit() {
    	if(observer != null) {
    		observer.onNexScroll(nextNr);
    	}
    	
    	TranslateAnimation trans1 = new TranslateAnimation(0, 0, 0, -txt1.getHeight());
    	trans1.setDuration(velocity);
    	trans1.setFillAfter(true);
    	TranslateAnimation trans2 = new TranslateAnimation(0, 0, txt2.getHeight(), 0);
    	trans2.setDuration(velocity);
    	trans2.setAnimationListener(new Animation.AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				Log.d(FlipDigitView.class.toString(), "onAnimationEnd - setValue: " + setValue);

				if (!setValue) {
					nr = nextNr;
					nextNr++;
					nrIteracoes++;

					if (nextNr == 10) {
						nextNr = 0;
					}
					txt1.setText(""+ nextNr);
					TextView txtTmp = txt1;
					txt1 = txt2;
					txt2 = txtTmp;
					
					if (maxIteracoes == -1 || nrIteracoes < maxIteracoes) {
						scrollDigit();
					}
				} else {
					setValue(nr);
				}
				
			}
		});
    	trans2.setFillAfter(true);
    	txt1.startAnimation(trans1);
    	txt2.startAnimation(trans2);
    	
    }
    
    public void scrollToDigit(int nr) {
    	setValue = false;
    	this.maxIteracoes = nr;
    	scrollDigit();
    }

	public void setObserver(FlipDigitObserver observer) {
		this.observer = observer;
	}

	public void setMaxIteracoes(int maxIteracoes) {
		this.maxIteracoes = maxIteracoes;
	}
	
	public void setValue(int nr) {
		this.nr = nr;
		clearAllAnimation();
		setValue = true;
		Log.d(FlipDigitView.class.toString(), "SetValue: " + nr);
		txt1.setText(String.valueOf(nr));
		if (nr+1 > 9) {
			nextNr = 0;
		} else {
			nextNr = nr+1;
		}
		txt2.setText(String.valueOf(nextNr));
		
		TranslateAnimation trans1 = new TranslateAnimation(0, 0, 0, 0);
    	trans1.setDuration(0);
    	trans1.setFillAfter(true);
    	trans1.setFillAfter(true);
    	txt1.startAnimation(trans1);
    	txt2.setVisibility(View.GONE);

		
	}
	
	private void clearAllAnimation() {
		txt1.animate().cancel();
		txt2.animate().cancel();
		txt1.clearAnimation();
		txt2.clearAnimation();
	}
	
	public void stopScroll() {
		this.maxIteracoes = 0;
	}

	public void setVelocity(int velocity) {
		this.velocity = velocity;
	}
    
    
}
