package com.doisdoissete.android.util.ddsutil.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;

public class ScreenUtil {


	public static int getStatusBarHeight(Context context) {
		  int result = 0;
		  int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
		  if (resourceId > 0) {
		      result = context.getResources().getDimensionPixelSize(resourceId);
		  }
		  return result;
		}
	
	/**
	 * Retorna Display
	 * @return
	 */
	public static Display getDefaultDisplay(Context mContext) {
		 WindowManager windowManager = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
		//Returns the size of the entire window, including status bar and title.
		DisplayMetrics dm = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(dm);

		return windowManager.getDefaultDisplay();
	}

	
	/**
	 * Retorna a largura da tela
	 * @return
	 */
	public static int getDisplayWidth(Context mContext) {
		 WindowManager windowManager = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
		//Returns the size of the entire window, including status bar and title.
		DisplayMetrics dm = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(dm);

		
		DisplayMetrics displaymetrics = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(displaymetrics);
		return dm.widthPixels;//displaymetrics.heightPixels;
	}

	/**
	 * Retorna a altura da tela
	 * @return
	 */
	public static int getDisplayHeight(Context mContext) {
		 WindowManager windowManager = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
		//Returns the size of the entire window, including status bar and title.
		DisplayMetrics dm = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(dm);

		
		DisplayMetrics displaymetrics = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(displaymetrics);
		return dm.heightPixels;//displaymetrics.heightPixels;
	}
	
	/**
	 * Faz o scroll at� a view
	 * @param scrollView
	 * @param view
	 */
	public static void smoothScrollToView(final ScrollView scrollView, final View view, final View viewHeader) {
		Log.d("TAG", "**** Top: " + view.getTop() + " - " + viewHeader.getHeight());
		new CountDownTimer(2000, 20) { 

	        public void onTick(long millisUntilFinished) { 
	        	scrollView.scrollTo(0, (int) (view.getTop() -viewHeader.getHeight() - millisUntilFinished)); 
	        } 

	        public void onFinish() { 

	        } 
	     }.start();
		
	}
	
	/**
	 * Faz o scroll at� a view
	 * @param scrollView
	 * @param view
	 */
	public static void smoothScrollToView(final ListView listView, int pos) {
		final int h = getItemHeightofListView(listView, pos);
		new CountDownTimer(2000, 20) { 

	        public void onTick(long millisUntilFinished) { 
	        	listView.scrollTo(0, (int) (h - millisUntilFinished)); 
	        } 

	        public void onFinish() { 

	        } 
	     }.start();
		
	}
	
	
	@SuppressLint("NewApi") public static void smotthListView(final ListView listView, final int pos) {
		
/*		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			listView.smoothScrollBy(getItemHeightofListView(listView, pos), 3000);
		} else {*/
		
			final long totalScrollTime = 1500; //total scroll time. I think that 300 000 000 years is close enouth to infinity. if not enought you can restart timer in onFinish()
	
			final int scrollPeriod = 20; // every 20 ms scoll will happened. smaller values for smoother
	
			final int heightToScroll = 20; // will be scrolled to 20 px every time. smaller values for smoother scrolling
	
			int sumHeightView = 0;
			for (int i=0; i <= pos; i++) {
				sumHeightView += getItemHeightofListView(listView, pos);
			}
			final int totalScroll = sumHeightView;
			 
			listView.post(new Runnable() {
									int sumScroll = 0;
			                        @Override
			                        public void run() {
			                                new CountDownTimer(totalScrollTime, scrollPeriod ) {
			                                    public void onTick(long millisUntilFinished) {
			                                    	if (sumScroll < totalScroll) {
			                                    		sumScroll += heightToScroll;
			                                    		int h = heightToScroll;
			                                    		if (sumScroll > totalScroll) {
				                                    		h = sumScroll-totalScroll;
				                                    	}
				                                        listView.smoothScrollBy(h, 1);
			                                    	}
			                                    	
			                                    }
	
			                                public void onFinish() {
			                                    //you can add code for restarting timer here
			                                	listView.requestFocus();
			                                }
			                            }.start();
			                        }
			                    });
		//}
	}
	
	public static int getItemHeightofListView(ListView listView, int pos) {

		ListAdapter mAdapter = listView.getAdapter();

		View childView = mAdapter.getView(pos, null, listView);
		childView.measure(
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
		return childView.getMeasuredHeight();

	}
}
