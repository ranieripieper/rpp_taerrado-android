package com.doisdoissete.android.util.ddsutil.view.cameragallery;

import android.graphics.Bitmap;

public class BitmapFilePath {

	private Bitmap bitmap;
	private String filePath;
	
	public BitmapFilePath(Bitmap bitmap, String filePath) {
		super();
		this.bitmap = bitmap;
		this.filePath = filePath;
	}
	/**
	 * @return the bitmap
	 */
	public Bitmap getBitmap() {
		return bitmap;
	}
	/**
	 * @param bitmap the bitmap to set
	 */
	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}
	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	
}
