package com.doisdoissete.android.util.ddsutil.service.asynctask;

import android.content.Context;
import android.os.AsyncTask;

public abstract class AsyncTaskBase<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

	protected Context context;
	protected ObserverAsyncTask<Result> observer;
	protected Exception mException;
	protected boolean error = false;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (observer != null) {
			observer.onPreExecute();
		}
	}
	
	protected void onPostExecute(Result result) {
		if (error) {
			onError();
			return;
		}
		if (observer != null) {
			observer.onPostExecute(result);
		}
		
	};
	
	@Override
	protected void onCancelled() {
		super.onCancelled();
		if (observer != null) {
			observer.onCancelled();
		}
		
	}
	
	protected void onError() {
		if (observer != null) {
			observer.onError(mException);
		}
		
	};
}
