package com.doisdoissete.android.util.ddsutil.view.custom.listview;

import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;


public abstract class PagingBaseAdapter<T> extends BaseAdapter {

	protected List<T> items;

	public PagingBaseAdapter() {
		this.items = new ArrayList<T>();
	}

	public PagingBaseAdapter(List<T> items) {
		this.items = items;
	}

	public void addMoreItems(List<T> newItems) {
		this.items.addAll(newItems);
		notifyDataSetChanged();
	}
	
	public void addFirstPosition(T newItem) {
		this.items.add(0, newItem);
		notifyDataSetChanged();
	}

	public void removeAllItems() {
		if (this.items != null) {
			this.items.removeAll(this.items);
		}
		notifyDataSetChanged();
	}


}
