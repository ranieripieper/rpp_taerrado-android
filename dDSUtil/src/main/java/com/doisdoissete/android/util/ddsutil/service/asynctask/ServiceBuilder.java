package com.doisdoissete.android.util.ddsutil.service.asynctask;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.JsonDeserializer;
import com.google.gson.reflect.TypeToken;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class ServiceBuilder {

	private boolean needConnection = true;
	private String url = null;
	private TypeToken<?> targetType;
	private MultiValueMap params = new LinkedMultiValueMap();
	private ObserverAsyncTask observerAsyncTask;
	private JsonDeserializer jsonDeserializer;
	private Context mContext;
	private BaseService service = null;
	private HttpMethod httpMethod = HttpMethod.POST;
	private HttpHeaders httpHeaders = new HttpHeaders();
	private String charset;
	private boolean uploadFile = false;
	
    public ServiceBuilder setNeedConnection(boolean needConnection) {
		this.needConnection = needConnection;
		return this;
	}

	public ServiceBuilder setUrl(String url) {
		this.url = url;
		return this;
	}
	
	public ServiceBuilder setParams(MultiValueMap params) {
		this.params = params;
		return this;
	}
	
	public ServiceBuilder setCharset(String charset) {
		this.charset = charset;
		return this;
	}

	public ServiceBuilder setObserverAsyncTask(ObserverAsyncTask observerAsyncTask) {
		this.observerAsyncTask = observerAsyncTask;
		return this;
	}

	public ServiceBuilder setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
		return this;
	}

	public AsyncTaskService mappingInto(Context ctx, @NonNull Class classTarget) {
		return mappingInto(ctx, classTarget, null);
	}
	
	public AsyncTaskService mappingInto(Context ctx, @NonNull Class classTarget, BaseService customService) {
        this.targetType = TypeToken.get(classTarget);
        
        mContext = ctx;
        
        if (customService == null) {
        	customService = new BaseService(this);
        } else {
        	customService.refreshServiceBuilder(this);
        }
        
        return new AsyncTaskService(ctx, customService, observerAsyncTask);
    }
	
	public BaseService mappingIntoBaseService(Context ctx, @NonNull Class classTarget) {
		return mappingIntoBaseService(ctx, classTarget, null);
	}
	
	public BaseService mappingIntoBaseService(Context ctx, @NonNull Class classTarget, BaseService customService) {
        this.targetType = TypeToken.get(classTarget);
        
        mContext = ctx;
        
        if (customService == null) {
        	customService = new BaseService(this);
        } else {
        	customService.refreshServiceBuilder(this);
        }
        
        return customService;
    }
	

	public TypeToken<?> getTargetType() {
		return targetType;
	}

	public void setTargetType(TypeToken<?> targetType) {
		this.targetType = targetType;
	}

	public boolean isNeedConnection() {
		return needConnection;
	}

	public String getUrl() {
		return url;
	}

	public MultiValueMap getParams() {
		return params;
	}
	
	public ServiceBuilder addParams(String key, String value) {
		params.add(key, value);
		return this;
	}
	

	public ServiceBuilder addHeaders(String key, String value) {
		httpHeaders.add(key, value);
		return this;
	}
	
	public ServiceBuilder setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
		return this;
	}


	public JsonDeserializer getJsonDeserializer() {
		return jsonDeserializer;
	}

	public ServiceBuilder setJsonDeserializer(JsonDeserializer jsonDeserializer) {
		this.jsonDeserializer = jsonDeserializer;
		return this;
	}

	public ServiceBuilder setService(BaseService customService) {
		this.service = customService;
		return this;
	}
	
	public Context getmContext() {
		return mContext;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public HttpHeaders getHttpHeaders() {
		return httpHeaders;
	}

	/**
	 * @return the uploadFile
	 */
	public boolean isUploadFile() {
		return uploadFile;
	}

	/**
	 * @param uploadFile the uploadFile to set
	 */
	public ServiceBuilder setUploadFile(boolean uploadFile) {
		this.uploadFile = uploadFile;
		return this;
	}

	/**
	 * @return the charset
	 */
	public String getCharset() {
		return charset;
	}
	
}
