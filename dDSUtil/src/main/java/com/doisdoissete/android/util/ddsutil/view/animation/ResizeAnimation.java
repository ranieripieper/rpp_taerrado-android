package com.doisdoissete.android.util.ddsutil.view.animation;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ResizeAnimation  extends Animation {
	final int startWidth;
	final int targetWidth;
	final int startHeight;
	final int targetHeight;
	View view;
 
	public ResizeAnimation(View view, int targetWidth, int targetHeight) {
		this.view = view;
		this.targetWidth = targetWidth;
		this.targetHeight = targetHeight;
		startWidth = view.getLayoutParams().width;
		startHeight = view.getLayoutParams().height;
		
		System.out.println("**** ResizeAnimation: st: " + startHeight + " targ: " + targetHeight);
    	
	}
 
	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		if (targetWidth > 0) {
			int newWidth = (int) (startWidth + (targetWidth - startWidth) * interpolatedTime);
			view.getLayoutParams().width = newWidth;
		}
		if (targetHeight > 0) {
			int newHeight = (int) (startHeight + (targetHeight - startHeight) * interpolatedTime);
			view.getLayoutParams().height = newHeight;
		}
		
		view.requestLayout();
	}
 
	@Override
	public void initialize(int width, int height, int parentWidth, int parentHeight) {
		super.initialize(width, height, parentWidth, parentHeight);
	}
 
	@Override
	public boolean willChangeBounds() {
		return true;
	}
}