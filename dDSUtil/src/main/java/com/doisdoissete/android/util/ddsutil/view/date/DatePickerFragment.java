package com.doisdoissete.android.util.ddsutil.view.date;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.DateUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

	/**
    public void showTimePickerDialog(EditText edtTxt) {
        DialogFragment newFragment = new DatePickerFragment(edtTxt);
        newFragment.show(EnviarSugestaoActivity.this.getSupportFragmentManager(), "timePicker");
    }
    
	 */
	private TextView edtText;
	private Date dt;
	private Date maxDate;
	private OnSetListener onSetListener;
	
	public static void showTimePickerDialog(FragmentManager fragmentManager, EditText edtTxt) {
        DialogFragment newFragment = new DatePickerFragment(edtTxt);
        newFragment.show(fragmentManager, "timePicker");
    }
	
	public static void showTimePickerDialog(FragmentManager fragmentManager, EditText edtTxt, Date maxDate) {
        DialogFragment newFragment = new DatePickerFragment(edtTxt, null, DateUtil.dfDiaMesAno.get(), maxDate);
        newFragment.show(fragmentManager, "timePicker");
    }
	
	public DatePickerFragment(TextView edtText, OnSetListener onSetListener, final DateFormat sdf, Date maxDate) {
		super();
		this.edtText = edtText;
		this.maxDate = maxDate;
		this.onSetListener = onSetListener;
		
    	if (edtText != null && !TextUtils.isEmpty(edtText.toString())) {
    		try {
    			this.dt = sdf.parse(edtText.toString());
			} catch (ParseException e) {
			}
    	}
		
	}
	
	public DatePickerFragment(TextView edtText) {
		this(edtText, null, DateUtil.dfDiaMesAno.get(), null);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Calendar c = Calendar.getInstance();
		if (dt != null) {
			c.setTime(dt);
		}
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		DatePickerDialog dialog =  new DatePickerDialog(getActivity(), this, year, month, day);
		if (maxDate != null) {
			dialog.getDatePicker().setMaxDate(maxDate.getTime());
		}
	    
	    Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.MONTH, -18);
	    dialog.getDatePicker().setMinDate(cal.getTimeInMillis());
		return dialog;
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar dtTmp = Calendar.getInstance();
		dtTmp.set(Calendar.YEAR, year);
		dtTmp.set(Calendar.MONTH, monthOfYear);
		dtTmp.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		
		if (edtText != null) {
			edtText.setText(DateUtil.dfDiaMesAno.get().format(dtTmp.getTime()));
		}
		if (onSetListener != null) {
			onSetListener.postSet(dtTmp.getTime());
		}
	}
	
	public static abstract class OnSetListener {
		public abstract void postSet(Date dt);
	}
}
