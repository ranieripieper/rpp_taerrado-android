package com.doisdoissete.android.util.ddsutil.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.R;

import java.util.ArrayList;
import java.util.List;

public class StaticImageAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    
    private List<Integer> mThumbIds = new ArrayList<Integer>();
    
    public StaticImageAdapter(Context c, List<Integer> mThumbIds) {
        mContext = c;
        this.mThumbIds = mThumbIds;
        inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mThumbIds.size();
    }

    public Integer getItem(int position) {
        return mThumbIds.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) { 
            imageView = (ImageView)inflater.inflate(R.layout.include_image_view, null);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(getItem(position));
        return imageView;
    }

}