package com.doisdoissete.android.util.ddsutil.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class TouchBlackHoleView extends View {
	
    
	public TouchBlackHoleView(Context context) {
		super(context);
	}
	
	public TouchBlackHoleView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public TouchBlackHoleView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}
	
	private boolean touch_disabled=true;
    
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return touch_disabled;
    }
    public void disable_touch(boolean b) {
        touch_disabled=b;
    }
}