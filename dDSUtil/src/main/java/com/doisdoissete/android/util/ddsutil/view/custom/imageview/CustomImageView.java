package com.doisdoissete.android.util.ddsutil.view.custom.imageview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class CustomImageView extends ImageView {

	private GestureDetector gestureDetector;
	private DoubleClick doubleClick;
	
	public CustomImageView(Context context) {
		super(context);
		init(context);
	}
	
    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
	    init(context);       
    }
    
    private void init(Context context) {
    	//gestureDetector = new GestureDetector(context, new GestureListener());
    }

	public interface DoubleClick {
		public void doubleClick(View v);
	}
	
    // delegate the event to the gesture detector
/*    @Override
    public boolean onTouchEvent(MotionEvent e) {
        boolean res = gestureDetector.onTouchEvent(e);
        if (!res) {
        	return super.onTouchEvent(e);
        }
        return res;
    }

    *//**
	 * @param doubleClick the doubleClick to set
	 *//*
	public void setDoubleClick(DoubleClick doubleClick) {
		this.doubleClick = doubleClick;
	}*/

	private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
        
        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (doubleClick != null) {
            	doubleClick.doubleClick(CustomImageView.this);
            }
            return true;
        }
    }
}
