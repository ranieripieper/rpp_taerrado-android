package com.doisdoissete.android.util.ddsutil;

public class ValidatorUtil {

	public static boolean emailIsValid(final String email) {
		String expRegular = "^[\\w-]+(\\.[\\w-]+)*@(([\\w-]{2,63}\\.)+[A-Za-z]{2,6}" + 
		"|\\[\\d{1,3}(\\.\\d{1,3}){3}\\])$";
		
		return email.matches(expRegular);
	}
	
	public static boolean isEmpty(String txt) {
		if(txt == null || txt.trim().length() <= 0) {
			return true;
		}
		return false;
	}
	
	public static boolean isNotEmpty(String txt) {
		return !isEmpty(txt);
	}
	
	public static boolean isNumber(String value) {
		try {
			 new Double(value);
			 return true;
		} catch(Exception e) {
			return false;
		}
	
	}
	
	
	
}
