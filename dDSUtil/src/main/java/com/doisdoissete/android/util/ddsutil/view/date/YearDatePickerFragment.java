package com.doisdoissete.android.util.ddsutil.view.date;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.R;

import java.util.Calendar;

public class YearDatePickerFragment extends DialogFragment {

	private TextView edtText;
	private int maxYear;
	private int minYear;
	private static int MIN_YEAR = 1910;
	private static int MAX_YEAR = 2020;
	
	public YearDatePickerFragment(TextView edtText, int pMinYear, int pMaxYear) {
		super();
		this.edtText = edtText;	
		this.minYear = pMinYear;
		this.maxYear = pMaxYear;
	}
	
	public YearDatePickerFragment(TextView edtText, int maxYear) {
		this(edtText, MIN_YEAR, maxYear);
	}
	
	public YearDatePickerFragment(TextView edtText) {
		this(edtText, MIN_YEAR, MAX_YEAR);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		
        final NumberPicker np = new NumberPicker(getActivity());
        np.setMaxValue(maxYear);
        np.setMinValue(minYear);
        np.setWrapSelectorWheel(false);
        np.setValue(year);
        alert.setView(np);
        np.setMinimumWidth(150);
		alert.setPositiveButton(R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						if (edtText != null) {
							edtText.setText(np.getValue() + "");
						}
					}
				});
		alert.setNegativeButton(R.string.cancelar,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
					}
				});
      
		return alert.create();
	}
}
