package com.doisdoissete.android.util.ddsutil.view.custom.flipdigit;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

import com.doisdoissete.android.util.ddsutil.R;

import java.util.ArrayList;
import java.util.List;

public class FlipDigitGroupView  extends LinearLayout {
	
	private Context mContext;
	private int nrDigits = 3;
	private List<FlipDigitView> lstFlipDigitView = new ArrayList<FlipDigitView>();
	private Boolean setValue = false;
	private int velocity = -1; 
	
	public FlipDigitGroupView(Context context) {
		super(context);
		initData(context, null, 0);
	}

	public FlipDigitGroupView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initData(context, attrs, 0);
	}

	public FlipDigitGroupView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initData(context, attrs, defStyle);
	}
	private void initData(Context context, AttributeSet attrs, int defStyle) {
		mContext = context;
		setAttributes(attrs);

		initializeView(attrs, defStyle);
		
		
	}
	
	private void initializeView(AttributeSet attrs, int defStyle) {
		
		for (int i=0; i < nrDigits; i++) {
			FlipDigitView flipDigitView = new FlipDigitView(mContext, attrs, defStyle);
			this.addView(flipDigitView);
			lstFlipDigitView.add(flipDigitView);
		}
		
		for (int i=1; i < lstFlipDigitView.size(); i++) {
			FlipDigitView flipDigit = lstFlipDigitView.get(i);
			final int pos = i-1;
			flipDigit.setObserver(new FlipDigitObserver() {
				
				@Override
				public void onNexScroll(int nr) {
					if (nr == 0) {
						lstFlipDigitView.get(pos).setMaxIteracoes(1);
						lstFlipDigitView.get(pos).scrollDigit();
					}
				}
			});
		}
	}
	
	private void setAttributes(AttributeSet attrs) {
		if (attrs != null) {
			TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.FlipDigitView);
			nrDigits = a.getInteger(R.styleable.FlipDigitView_nr_digits, 3);
			a.recycle();
		}
	}
	
	public void scrollToDigit(int nr) {
		lstFlipDigitView.get(lstFlipDigitView.size()-1).scrollToDigit(nr);
	}
	
	public void setValue(int nr) {
		for (int i=lstFlipDigitView.size()-1; i >=0 ; i--) {
			lstFlipDigitView.get(i).stopScroll();
		}

		String num = String.valueOf(nr);
		if (num.length() > nrDigits) {
			num = num.substring(num.length()-nrDigits, num.length());
		}
		while (num.length() < nrDigits) {
			num = "0" + num;
		}
		
		Log.d(FlipDigitGroupView.class.toString(), " SetValue: " + num);
		for (int i=0; i < lstFlipDigitView.size(); i++) {
			FlipDigitView flipDigit = lstFlipDigitView.get(i);
			flipDigit.setValue(Integer.valueOf(String.valueOf(num.charAt(i))));
		}
	}


	public void setVelocity(int velocity) {
		this.velocity = velocity;
		for (FlipDigitView flipDigitView : lstFlipDigitView) {
			flipDigitView.setVelocity(this.velocity);
		}
	}
	
	
}
