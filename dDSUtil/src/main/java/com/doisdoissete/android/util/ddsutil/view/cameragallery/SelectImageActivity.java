package com.doisdoissete.android.util.ddsutil.view.cameragallery;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.R;
import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;

import java.io.File;

public abstract class SelectImageActivity extends AppCompatActivity {

	protected File outFile = null;
	protected String filePath = null;
	
	protected abstract void setImageSelect(Bitmap takenPictureData);
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    BitmapFilePath bitmapFilePath = null;

	    if (requestCode == NativeCameraGalleyUtil.PICTURE_TAKEN_FROM_CAMERA || requestCode == NativeCameraGalleyUtil.PICTURE_TAKEN_FROM_GALLERY) {
	    	bitmapFilePath = NativeCameraGalleyUtil.processActivityResult(SelectImageActivity.this, resultCode, requestCode, data, outFile);
	    }
	    if(bitmapFilePath != null) {
	    	filePath = bitmapFilePath.getFilePath();
	    	setImageSelect(bitmapFilePath.getBitmap());
	    }       
	}
	
	public void selectTakePicture(View v, final String appName) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_escolher_foto)
               .setPositiveButton(R.string.dialog_txt_camera, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   NativeCameraGalleyUtil.getPictureFromCamera(SelectImageActivity.this, createOutFile(appName));
                   }
               })
               .setNegativeButton(R.string.dialog_txt_galeria, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   NativeCameraGalleyUtil.getPictureFromGallery(SelectImageActivity.this);
                   }
               });
        builder.create().show();
	}
	
	private File createOutFile(String appName) {
		try {
			File outputDir = BitmapUtil.storage_getExternalPublicFolder(
					Environment.DIRECTORY_PICTURES,
					appName, true);
			outFile = BitmapUtil.storage_createUniqueFileName("cameraPic",
					".jpg", outputDir);
		} catch (Exception e) {
		}
		return outFile;
	}


}
