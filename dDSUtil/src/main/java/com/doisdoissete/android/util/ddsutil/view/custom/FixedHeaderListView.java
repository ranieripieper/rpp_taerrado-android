package com.doisdoissete.android.util.ddsutil.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.doisdoissete.android.util.ddsutil.R;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;

public class FixedHeaderListView extends LinearLayout {

	private ListView mListView;
	private ViewGroup mHeader;
	private View mHeaderLogo;
	private View mPlaceHolderView;
	private int mHeaderHeight;
	private int mMinHeaderTranslation;
	private int layoutHeader;
	private Context mContext;
	private int mBgListView = -1;
	private int mBgHeader = -1;
	private int mMinHeightSize = -1;
	private int heightHeaderLogo = 0;
	private boolean fillHeader = false;
	private boolean resizeHeader = true;
	private boolean mapIncluded = false;
	private int heightHeader = 0;
	private FixedHeaderScrollObserver mObserver;
	
	public FixedHeaderListView(Context context) {
		super(context);
		initData(context, null);
	}

	public FixedHeaderListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initData(context, attrs);
	}

	public FixedHeaderListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initData(context, attrs);
	}

	private void initData(Context context, AttributeSet attrs) {
		mContext = context;
		setAttributes(attrs);

		LayoutInflater inflater = LayoutInflater.from(context);
		if (mapIncluded) {
			inflater.inflate(R.layout.fixed_map_header_listview, this);
			mHeader = (FrameLayout)findViewById(R.id.header);
		} else {
			inflater.inflate(R.layout.fixed_header_listview, this);
			mHeader = (LinearLayout)findViewById(R.id.header);
			
		}
		
		mListView = (ListView)findViewById(R.id.listView);
        mPlaceHolderView = inflater.inflate(R.layout.view_header_placeholder, mListView, false);
        mListView.addHeaderView(mPlaceHolderView);

		if (mBgHeader > 0) {
			mHeader.setBackgroundResource(mBgHeader);
		}
		if (mBgListView > 0) {
			mListView.setBackgroundResource(mBgListView);
		}

        mHeaderHeight = Float.valueOf(getResources().getDimension(R.dimen.default_header_height)).intValue();
        if (heightHeader != 0) {
        	mHeaderHeight = heightHeader;
        }
        if (mHeaderHeight < 0) {
        	mHeaderHeight = ScreenUtil.getDisplayHeight(mContext) + mHeaderHeight;
        }
        
        //seta a altura no viewPlaceHolder e no header
        mPlaceHolderView.getLayoutParams().height = mHeaderHeight;
        mHeader.getLayoutParams().height = mHeaderHeight;

        mMinHeaderTranslation = -mHeaderHeight;
        
        if (layoutHeader > 0) {
        	mHeaderLogo = inflater.inflate(layoutHeader, null);
        	mHeader.addView(mHeaderLogo);
        	mHeaderLogo.measure( View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        	if (fillHeader) {
        		heightHeaderLogo = mHeaderHeight;
        	} else {
        		
        		heightHeaderLogo = mHeaderLogo.getMeasuredHeight();
        	}
        	
        	if (heightHeader == 0) {
        		heightHeader = mHeaderLogo.getMeasuredHeight();
        	}
        	
        }
        
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int scrollY = getListViewScrollY();
                
                int transY = Math.max(-scrollY, mMinHeaderTranslation);

                Log.d(FixedHeaderListView.class.toString(), "******** " + transY + " h: "+ (ScreenUtil.getDisplayHeight(mContext) - mMinHeightSize));

                int tam = transY;
                Log.d(FixedHeaderListView.class.toString(), "HeaderSize: " + mHeaderLogo.getHeight() + " - mMinHeightSize: " + mMinHeightSize);
                Log.d(FixedHeaderListView.class.toString(), "Visivel: " + mPlaceHolderView.getBottom() + " - top: " +mPlaceHolderView.getTop());
                Log.d(FixedHeaderListView.class.toString(), "Tamanho: " + tam + " nr:" + (ScreenUtil.getDisplayHeight(mContext) - mMinHeightSize));
                Log.d(FixedHeaderListView.class.toString(), "************************");
                
                int scrollTop = 0;
                if (Math.abs(transY) > Math.abs(oldTransy) ) {
                	scrollTop = -1;
                } else if (Math.abs(transY) < Math.abs(oldTransy) ) {
                	scrollTop = 1;
        		}
               
                Log.d(FixedHeaderListView.class.toString(), "tam: " + tam + " | " + "heightHeader: " + heightHeader + " | " + "mMinHeightSize: " + mMinHeightSize+ " | " + "ScreenUtil.getDisplayHeight(mContext): " + ScreenUtil.getDisplayHeight(mContext));
                
                if ((heightHeader > mMinHeightSize) || 
                		((Math.abs(tam) + Math.abs(heightHeader)) <= (ScreenUtil.getDisplayHeight(mContext) - mMinHeightSize))) {
                //if  {
                	if (mObserver != null) {
                		mObserver.onPreMoveHeader();
                	}
                	resizeHeaderLogo(mPlaceHolderView.getBottom(), scrollTop);
                	int topTmp = mHeader.getTop();
                	mHeader.setTranslationY(transY);
                	if (resizeHeader) {
                		mHeaderLogo.setTranslationY(-transY/2);    
                	}
                	if (mObserver != null) {
                		 Log.d(FixedHeaderListView.class.toString(), "************************" + transY + " - " + oldTransy);
                		if (Math.abs(transY) > Math.abs(oldTransy) ) {
                			mObserver.onPostMoveHeader(-1, transY);
                		} else if (Math.abs(transY) < Math.abs(oldTransy) ) {
                			mObserver.onPostMoveHeader(1, transY);
                		}                		
                	}
                	
                }
                oldTransy = transY;
                mHeader.requestLayout();
                mListView.requestLayout();
            }
        });
	}
	
	private int oldTransy = 0;
	
	private void resizeHeaderLogo(int heightView, int scroll) {
		
			int actualH = mHeaderLogo.getHeight();
			int newValue = -1;
			
			Log.d(FixedHeaderListView.class.toString(), "heightView: " + heightView + " actualH: " + actualH + " heightHeaderLogo: " + heightHeaderLogo);

			if (actualH > heightView) {
				newValue = heightView;
			} else {
				if (heightHeaderLogo > heightView ) {
					newValue = heightView;
				} else {
					newValue = heightHeaderLogo;
				}
			}
			
			if (newValue > 0) {
				if (resizeHeader) {
	
					Log.d(FixedHeaderListView.class.toString(), "NewValue Header" + newValue);
					android.view.ViewGroup.LayoutParams lp = (android.view.ViewGroup.LayoutParams)mHeaderLogo.getLayoutParams();
					lp.height = newValue;
					mHeaderLogo.setLayoutParams(lp);
					
					mHeaderLogo.requestLayout();
					
					heightHeader = newValue;
				}
				
				//findViewById(R.id.map).getLayoutParams().height = newValue;
				if (mObserver != null) {
					mObserver.onResizeHeader(scroll, actualH, newValue);
            	}
			
		}		
	}
	
	private void setAttributes(AttributeSet attrs) {
		if (attrs != null) {
			TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.FixedHeaderListView);
			layoutHeader = a.getResourceId(R.styleable.FixedHeaderListView_layout_header, -1);
			mBgListView = a.getResourceId(R.styleable.FixedHeaderListView_bg_list_view, -1);
			mBgHeader = a.getResourceId(R.styleable.FixedHeaderListView_bg_header, -1);
			mMinHeightSize = (int)a.getDimension(R.styleable.FixedHeaderListView_min_height_size, mMinHeightSize);
			resizeHeader = a.getBoolean(R.styleable.FixedHeaderListView_resize_header, resizeHeader);
			heightHeader = a.getDimensionPixelOffset(R.styleable.FixedHeaderListView_height_header, heightHeader);
			fillHeader = a.getBoolean(R.styleable.FixedHeaderListView_fill_header, fillHeader);
			mapIncluded = a.getBoolean(R.styleable.FixedHeaderListView_map_included, mapIncluded);
			a.recycle();
		}
	}
	
   public int getListViewScrollY() {
        View c = mListView.getChildAt(0);
        if (c == null) {
            return 0;
        }

        int firstVisiblePosition = mListView.getFirstVisiblePosition();
        int top = c.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mHeaderHeight;
        }

        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }
   
   public void setAdapter(ListAdapter adapter) {
	   mListView.setAdapter(adapter);
   }

	public ListView getListView() {
		return mListView;
	}

	public FixedHeaderScrollObserver getFixedHeaderScrollObserver() {
		return mObserver;
	}

	public void setFixedHeaderScrollObserver(FixedHeaderScrollObserver observer) {
		this.mObserver = observer;
	}
   
   
}
