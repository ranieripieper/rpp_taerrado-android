package com.doisdoissete.android.util.ddsutil.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.R;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ParamServiceInterface;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;

public class EditTextPlus extends EditText implements ParamServiceInterface {

	private boolean required = false;
	private String regexValidation = "";
	private String paramService = null;
	private String mask = null;
	private OnOkDoneClick mOnOkDoneClick;
	private int minLength = -1;
	
	public EditTextPlus(Context context) {
		super(context);
	}

	public EditTextPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	public EditTextPlus(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs);
	}
	
	private void init(Context context, AttributeSet attrs) {
		loadAttributes(context, attrs);
		if (!TextUtils.isEmpty(mask)) {
			TextWatcher txtWatcher = Mask.insert(mask, this);
			this.addTextChangedListener(txtWatcher);
		}
	}
	
    private void loadAttributes(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.EditTextPlus);
        
        mask = a.getString(R.styleable.EditTextPlus_mask);
        minLength = a.getInteger(R.styleable.EditTextPlus_min_length, -1);
        a.recycle();
        
        setDefaultAttrs(ctx, attrs);
    }
    
	private void setDefaultAttrs(Context ctx, AttributeSet attrs) {
		TypedArray a = ctx.obtainStyledAttributes(attrs,
				R.styleable.TextViewPlus);
		TypedArray defaultAttr = ctx.obtainStyledAttributes(attrs,
				R.styleable.DDSDefaultAttributes);
		String customFont = defaultAttr
				.getString(R.styleable.DDSDefaultAttributes_text_font);
		required = defaultAttr.getBoolean(
				R.styleable.DDSDefaultAttributes_required, false);
		regexValidation = defaultAttr
				.getString(R.styleable.DDSDefaultAttributes_regex_validation);
		paramService = defaultAttr
				.getString(R.styleable.DDSDefaultAttributes_param_service);
		defaultAttr.recycle();
		FontUtilCache.setCustomFont(ctx, this, customFont);
		a.recycle();
	}

	public boolean isValid() {
		if (required && TextUtils.isEmpty(this.getText().toString())) {
			return false;
		}
		if (!TextUtils.isEmpty(regexValidation)) {
			if (!this.getText().toString().matches(regexValidation)) {
				return false;
			}
		}
        if (minLength > 0 && (required || (getText().length() > 0))) {
            if (minLength > getText().length()) {
                return false;
            }
        }
		return true;
	}

	public boolean isNotValid() {
		return !isValid();
	}

	public String getTextStr() {
		return getText().toString();
	}

	public final void hiddenKeyboard(Context mContext) {
		EditTextPlus.hiddenKeyboard(mContext, this);
	}
	
	public final void openKeyboard(Context mContext) {
		this.requestFocus();
		InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
	}
	
	public static final void hiddenKeyboard(Context mContext, EditText edt) {
		if (edt != null) {
			InputMethodManager imm = (InputMethodManager) mContext
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
		}
	}

	/**
	 * @return the paramService
	 */
	public String getParamService() {
		return paramService;
	}

	/**
	 * @param paramService the paramService to set
	 */
	public void setParamService(String paramService) {
		this.paramService = paramService;
	}
	
	public String getValue() {
		return this.getTextStr();
	}
	
	public void setText(String str) {
		if (TextUtils.isEmpty(str)) {
			super.setText("");
		} else {
			super.setText(str);
		}
	}
	
	public void setonOkDoneClick(OnOkDoneClick onOkDoneClick) {
		this.mOnOkDoneClick = onOkDoneClick;
		this.setOnEditorActionListener(new TextView.OnEditorActionListener(){
		    public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event){
		        if(actionId == EditorInfo.IME_ACTION_DONE 
		            || actionId == EditorInfo.IME_NULL
		            || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){

		        	mOnOkDoneClick.onOkDoneClick(EditTextPlus.this);
		            return true;
		        } else {
		            return false;
		        }
		    }
		});
	}	
	
	public static abstract class OnOkDoneClick {
		public abstract void onOkDoneClick(EditTextPlus editText);
	}

}
