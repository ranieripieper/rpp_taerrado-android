package com.doisdoissete.android.util.ddsutil.exception;

public class GenericException extends Exception {

	private static final long serialVersionUID = 1L;
	private String msgErro;
	
	public GenericException() {
		
	}
	
	public GenericException(String m) {
		super();
		this.msgErro = m;
	}
	
	public GenericException(Exception e) {
		super(e);
	}

	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	
}